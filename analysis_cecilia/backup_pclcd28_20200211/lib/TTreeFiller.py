import pyLCIO
from pyLCIO import UTIL
from pyLCIO.UTIL import *
from ROOT import gStyle, gROOT
from math import sqrt, pi, atan, sin
from ROOT import TFile, TTree, TCanvas, TH1D, TF1, TLorentzVector, TVector3,kRed,kGreen,kDashed,kBlue
from datetime import datetime
from array import array
from operator import itemgetter
from collections import Counter
import json
import glob
gStyle.SetOptStat(0)
gStyle.SetLineWidth(2)
gStyle.SetTitleFont(42)
gStyle.SetLabelFont(42)
gStyle.SetPadTickX(1)
gStyle.SetPadTickY(1)
gStyle.SetLabelSize(0.04)
gROOT.ForceStyle()

#--------------------#
# ausiliar functions #
#--------------------#

def logbin(histo):
    axis = histo.GetXaxis()
    bins = axis.GetNbins()
    binzero = axis.GetXmin()
    width = (axis.GetXmax()-binzero)/bins
    new_bins = []
    for bin in range(bins):
        new_bins.append(pow(10,binzero + bin*width))
    axis.Set(bins,new_bins)
    return histo


def radius2(x,hits):
    return hits.at(x).getPositionVec().X()**2+hits.at(x).getPositionVec().Y()**2+hits.at(x).getPositionVec().Z()**2

def hit_radius2(hit):
    return hit.getPositionVec().X()**2+hit.getPositionVec().Y()**2+hit.getPositionVec().Z()**2

def length(hit1,hit2):
    return sqrt(abs(hit_radius2(hit2) - hit_radius2(hit1)))

def sort_by_radius (v_hits):
    x = []
    for l in range(len(v_hits)):   
        x.append((l,radius2(l,v_hits)))
    sorted(x,key=itemgetter(1), reverse=True)
    x = zip(*x)
    return x[0]
        
def mode(l):
    counter = Counter(l)
    max_count = max(counter.values())
    return([item for item,count in counter.items() if count == max_count])



#---------------------------------------#
#                                       #
#           Class TRACKINIT:            #
#    1.initialization class for the     #
# track useful parameters that fill the #
#     Track TTree of the .root file     # 
#    2.initialization of the TTree      #
#   branches (function TTree_init_())   #
#                                       #
#---------------------------------------#

class TrackInit:
    
    tmaxsize = 50000

    tracksperevent = array('i',[0])
    npfotot = array('i',[0])
    hits = array('i',tmaxsize*[0])
    status = array('i',tmaxsize*[0])
    npfo = array('i',tmaxsize*[0])    
    D0 = array('f',tmaxsize*[0.])
    phi = array('f',tmaxsize*[0.])
    Z0 = array('f',tmaxsize*[0.])
    theta = array('f',tmaxsize*[0.])
    curvature = array('f',tmaxsize*[0.])
    vertexr = array('f',tmaxsize*[0.])
    pt = array('f',tmaxsize*[0.])
    p = array('f',tmaxsize*[0.])
    length = array('f',tmaxsize*[0.])
    pdgid = array('f',tmaxsize*[0.])
    endpoint = array('f',tmaxsize*[0.])
    detector_layer = array('f',tmaxsize*[0.])
    dEdx_average = array('f',tmaxsize*[0.])
    dEdx_max = array('f',tmaxsize*[0.])
    t_theta = array('f',tmaxsize*[0.])
    t_phi = array('f',tmaxsize*[0.])
    t_pt = array('f',tmaxsize*[0.])
    charginosperevent = array('i',[0])
    c_theta = array('f',tmaxsize*[0.])
    c_phi = array('f',tmaxsize*[0.])
    c_dEdx_average = array('f',tmaxsize*[0.])
    c_pt = array('f',tmaxsize*[0.])
    c_pdg = array('f',tmaxsize*[0.])
    c_quality = array('f',tmaxsize*[0.])
    pfotheta = array('f',tmaxsize*[0.])
    pfophi = array('f',tmaxsize*[0.])
    
    def _init_(self):
        self.tracksperevent = 50

    def TTree_init_(self,TreeTrack):
        TreeTrack.Branch('TracksperEvent',self.tracksperevent,'TracksperEvent/I')
        TreeTrack.Branch('TrackNPFOTot',self.npfotot,'TrackNPFOTot/I')
        TreeTrack.Branch('TrackStatus',self.status,'TrackStatus[TracksperEvent]/I')
        TreeTrack.Branch('TrackNPFO',self.npfo,'TrackNPFO[TracksperEvent]/I')
        TreeTrack.Branch('TrackHits',self.hits,'TrackHits[TracksperEvent]/I')
        TreeTrack.Branch('TrackPDGId',self.pdgid,'TrackPDGId[TracksperEvent]/F')
        TreeTrack.Branch('TrackPhi',self.phi,'TrackPhi[TracksperEvent]/F')
        TreeTrack.Branch('TrackD0',self.D0,'TrackD0[TracksperEvent]/F')
        TreeTrack.Branch('TrackZ0',self.Z0,'TrackZ0[TracksperEvent]/F')
        TreeTrack.Branch('TrackTheta',self.theta,'TrackTheta[TracksperEvent]/F')
        TreeTrack.Branch('TrackVertexR',self.vertexr,'TrackVertexR[TracksperEvent]/F')
        TreeTrack.Branch('TrackCurvature',self.curvature,'TrackCurvature[TracksperEvent]/F')
        TreeTrack.Branch('TrackPt',self.pt,'TrackPt[TracksperEvent]/F')
        TreeTrack.Branch('TrackP',self.p,'TrackP[TracksperEvent]/F')
        TreeTrack.Branch('TrackL',self.length,'TrackL[TracksperEvent]/F')
        TreeTrack.Branch('TrackEndPoint',self.endpoint,'TrackEndPoint[TracksperEvent]/F')
        TreeTrack.Branch('TrackLastHitDetector.Layer',self.detector_layer,'TrackLastHitDetector.Layer[TracksperEvent]/F')
        TreeTrack.Branch('TrackdEdxAverage',self.dEdx_average,'TrackdEdxAverage[TracksperEvent]/F')
        TreeTrack.Branch('TrackdEdxMax',self.dEdx_max,'TrackdEdxMax[TracksperEvent]/F')
        TreeTrack.Branch('TrackTrueTheta',self.t_theta,'TrackTrueTheta[TracksperEvent]/F')
        TreeTrack.Branch('TrackTruePhi',self.t_phi,'TrackTruePhi[TracksperEvent]/F')
        TreeTrack.Branch('TrackTruePt',self.t_pt,'TrackTruePt[TracksperEvent]/F')
        TreeTrack.Branch('TrackPFOtheta',self.pfotheta,'TrackPFOTheta[TracksperEvent]/F')
        TreeTrack.Branch('TrackPFOPhi',self.pfophi,'TrackPFOPhi[TracksperEvent]/F')
        TreeTrack.Branch('CharginoCandidatesperEvent',self.charginosperevent,"CharginoCandidatesperEvent/I")
        TreeTrack.Branch('CharginoCandidateTheta',self.c_theta,'CharginoCandidateTheta[CharginoCandidatesperEvent]/F')
        TreeTrack.Branch('CharginoCandidatePhi',self.c_phi,'CharginoCandidatePhi[CharginoCandidatesperEvent]/F')
        TreeTrack.Branch('CharginoCandidatedEdxAverage',self.c_dEdx_average,'CharginoCandidatedEdxAverage[CharginoCandidatesperEvent]/F')
        TreeTrack.Branch('CharginoCandidatePt',self.c_pt,'CharginoCandidatePt[CharginoCandidatesperEvent]/F')
        TreeTrack.Branch('CharginoCandidatePDG',self.c_pdg,'CharginoCandidatePDG[CharginoCandidatesperEvent]/F')
        TreeTrack.Branch('CharginoCandidateFitQuality',self.c_quality,'CharginoCandidateFitQuality[CharginoCandidatesperEvent]/F')
        
        return TreeTrack


#---------------------------------------#
#                                       #
#                TRACK:                 #
# for the event "event" fills the class #
#     ptrack reading the collection     #
#           SiTracks_Refitted           #
#                                       #
#---------------------------------------#



def Track(event,ptrack,Candidates):
    tCollection = event.getCollection("SiTracks_Refitted")
    pfoCollection = event.getCollection("PandoraPFOs")#TightSelectedPandoraPFOs")
   
    relations = {}

    ptrack.tracksperevent[0] = int(len(tCollection))
    ptrack.charginosperevent[0] = ptrack.tracksperevent[0]
    ptrack.npfotot[0] = int(len(pfoCollection))

    npfotot = 0
    jt = 0
    c_jt = 0
    trackerHitCollections = ["VXDTrackerHits","VXDEndcapTrackerHits","ITrackerHits","OTrackerHits","ITrackerEndcapHits","OTrackerEndcapHits"]
    trackerHitRelationCollections = ["VXDTrackerHitRelations","VXDEndcapTrackerHitRelations","InnerTrackerBarrelHitsRelations","OuterTrackerBarrelHitsRelations","InnerTrackerEndcapHitsRelations","OuterTrackerEndcapHitsRelations"]
    for ntrackerHitCollection,trackerHitCollection in enumerate(trackerHitCollections):
        if  event.getCollection(trackerHitCollection).getNumberOfElements() == 0: continue
        relation = pyLCIO.UTIL.LCRelationNavigator(event.getCollection(trackerHitRelationCollections[ntrackerHitCollection]))
        relations[ntrackerHitCollection] = relation

    pforelation = pyLCIO.UTIL.LCRelationNavigator(event.getCollection("SiTracks_Refitted"))
        
    for nt,t in enumerate(tCollection):
        if t.getTrackerHits().size()==0:
            continue 
        npfo = 0
        dEdx_point = 0
        dEdx_max = 0
        points = 0
        for pfo in pfoCollection:
            pfotracks = pfo.getTracks()
            if len(pfotracks)==0: continue
            pfoobj = pfotracks.at(0)
            if pfoobj.id() == t.id():
                npfo +=1
                npfotot += 1
                if len(pfotracks) >1 : print('more than one', len(pfotracks))
                ptrack.pfotheta[jt] = pi/2-atan(pfoobj.getTanLambda())
                ptrack.pfophi[jt] = pfoobj.getPhi()
                break

        hits = t.getTrackerHits()
        if len(hits)>1:
            index = sort_by_radius(hits)
        else: index[0] = 0
        pdgs = []
        status = []
        truetheta = []
        truept = []
        truephi = []
        l = length(hits[index[0]], hits[index[-1]])
        for i in index:
            hit = hits[i]
            for subdetector,relation in relations.items():
                simHit = relation.getRelatedToObjects(hit)
                if len(simHit) == 0: continue
                simhit = simHit.at(0)
                quality = simhit.getQuality()
                try: 
                    particle = simhit.getMCParticle()
                    status.append(particle.getGeneratorStatus())
                    lorentzparticle = particle.getLorentzVec()
                    truetheta.append(lorentzparticle.Theta()*180/pi)
                    truept.append(lorentzparticle.Pt())
                    truephi.append(lorentzparticle.Phi())
                except: 
                    status.append(0)
                    truetheta.append(200)
                    truept.append(-200)
                    truephi.append(400)
                try: 
                    pdg = simhit.getMCParticle().getPDG()
                except: pdg = 0
                pdgs.append(pdg)
                dEdx_point += simhit.getEDep()/simhit.getPathLength()
                dEdx_max = max(dEdx_max,dEdx_point) 
                points +=1
                if hit == hits[-1]:
                    detector = subdetector
        pdg = mode(pdgs) 
        if len(pdg) != 1:
            pdg = [0]
            pdgs = [0]
        dEdx_point /= points
        layer = t.getSubdetectorHitNumbers().at(index[-1])
        ptrack.npfo[jt] = npfo
        index_true = pdgs.index(pdg[0])
        ptrack.status[jt] = status[index_true]
        ptrack.t_theta[jt] = truetheta[index_true]
        ptrack.t_phi[jt] = truephi[index_true]
        ptrack.t_pt[jt] = truept[index_true]
        ptrack.hits[jt] = len(hits)
        ptrack.phi[jt] = t.getPhi()
        ptrack.D0[jt] = t.getD0()
        ptrack.Z0[jt] = t.getZ0()
        ptrack.theta[jt] = pi/2-atan(t.getTanLambda())
        ptrack.curvature[jt] = abs(t.getOmega())
        ptrack.vertexr[jt] = sqrt(ptrack.D0[jt]**2+ptrack.Z0[jt]**2)
        if ptrack.curvature[jt] !=0: ptrack.pt[jt] = 0.3*4/(ptrack.curvature[jt]*1000)
        if sin(ptrack.theta[jt])!=0: ptrack.p[jt] = ptrack.pt[jt]/sin(ptrack.theta[jt])
        ptrack.theta[jt] *= 180/pi
        ptrack.pdgid[jt] = pdg[0]
        ptrack.length[jt] = l
        ptrack.endpoint[jt] = sqrt(hit_radius2(hits[index[-1]]))
        ptrack.detector_layer[jt] = detector + 0.1*layer
        ptrack.dEdx_average[jt] = dEdx_point
        ptrack.dEdx_max[jt] = dEdx_max
        jt += 1
        if not Candidates: continue
#        if ptrack.vertexr[jt-1] > 0.5: continue 
        if ptrack.npfo[jt-1] != 0 : continue
 #       if ptrack.pt[jt-1]<10: continue
  #      if ptrack.dEdx_average<0.0005: continue
        ptrack.c_theta[c_jt] = ptrack.theta[jt-1] 
        ptrack.c_pt[c_jt] = ptrack.pt[jt-1]
        ptrack.c_phi[c_jt] = ptrack.phi[jt-1]
        ptrack.c_pdg[c_jt] = ptrack.pdgid[jt-1]
        ptrack.c_dEdx_average[c_jt] = ptrack.dEdx_average[jt-1]
        ptrack.c_quality[c_jt] = quality
        c_jt += 1

    ptrack.charginosperevent[0] = c_jt
    ptrack.tracksperevent[0] = jt
    ptrack.npfotot[0] = npfotot
    return ptrack



#---------------------------------------#
#                                       #
#        Class KINEMATICSINIT:          #
#    1.initialization class for the     #
#  useful MC  parameters that fill the  #
#  Kinematics TTree of the .root file   # 
#    2.initialization of the TTree      #
#   branches (function TTree_init_())   #
#                                       #
#---------------------------------------#

class KinematicsInit:

    pmaxsize = 130

    particleperevent = array('i', [0])
    pt = array ('f',pmaxsize*[-200.])
    theta = array ('f',pmaxsize*[-20.])
    phi = array ('f',pmaxsize*[-200.])
    pdgid = array('i',pmaxsize*[0])

    def _init_(self):
        self.particleperevent[0] = 130

    def TTree_init_(self, TreeParticleKinematics):
        TreeParticleKinematics.Branch('SIMParticleperEvent',self.particleperevent,'SIMParticleperEvent/I')
        TreeParticleKinematics.Branch('SIMPt',self.pt,'SIMPt[SIMParticleperEvent]/F')
        TreeParticleKinematics.Branch('SIMTheta',self.theta,'SIMTheta[SIMParticleperEvent]/F')
        TreeParticleKinematics.Branch('SIMPhi',self.phi,'SIMPhi[SIMParticleperEvent]/F')
        TreeParticleKinematics.Branch('SIMPDG',self.pdgid,'SIMPDG[SIMParticleperEvent]/I')
        return TreeParticleKinematics


#---------------------------------------#
#                                       #
#             KINEMATICS:               #
# for the event "event" fills the class #
#  pkinematics reading the collection   #
#           MCPhysicsParticles          #
#                                       #
#---------------------------------------#


def Kinematics(event,pkinematics):
    pCollection = event.getCollection("MCPhysicsParticles")

    pkinematics.particleperevent[0] = len(pCollection)

    j = 0

    for np,p in enumerate(pCollection) :
        # get particle info                                                                                                                                                                                                              
        if p.getGeneratorStatus()== 0:
            continue

        if p.getSimulatorStatus() == -2147483648:
            continue
        v = p.getLorentzVec()
        pkinematics.theta[j]= v.Theta()*180/pi
        pkinematics.pt[j] = v.Pt()
        pkinematics.phi[j] = v.Phi()
        pkinematics.pdgid[j] = p.getPDG()
        j += 1
   
    pkinematics.particleperevent[0] = j

    return pkinematics
