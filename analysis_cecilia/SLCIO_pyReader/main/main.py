import sys
sys.path.append('..')
import os
from pyLCIO import IOIMPL
from pyLCIO import UTIL
from pyLCIO.UTIL import LCTOOLS
from math import sqrt, pi
from ROOT import TFile, TTree, gStyle, gROOT
import ROOT
from datetime import datetime
from array import array
import json
import glob
from lib import TTreeFiller as TF
gROOT.LoadMacro("../CLICdpStyle.C+")
ROOT.CLICdpStyle()


#-------------------------------------#
#                                     #
#           ANALYSIS CODE             #
#     to configure parameters see     #
#    ../cfg/InformationDataset.json   #
#                                     #
#-------------------------------------#


# configuring parameters
with open('../cfg/InformationDataset.json') as json_data_file:
    data = json.load(json_data_file)

# create a reader for a LCIO file
reader = IOIMPL.LCFactory.getInstance().createLCReader()

# create a json output file w/ processing info 
x = {
    'initialization_time': datetime.today().isoformat()
}

# code core: loop on the LCIO files
for ndatafile,datafile in enumerate(glob.iglob(str(data["DirectoryPath"])+'*/*.slcio')):
    # opening file "datafile"
    reader.open(str(datafile))
    print ("Opening sim file {}".format(datafile))

    # initializing output .root
    ROOTOutput = TFile("../ntuples_13899/"+data["NtupleOutputName"]+str(ndatafile)+".root","RECREATE")

    # initializing tree structure with reconstructed tracks information
    track = TF.TrackInit()
    Track = TTree("Track","Track")
    Track = track.TTree_init_(Track)

    # initializing tree structure with montecarlo particles information
    kin = TF.KinematicsInit()
    ParticleKinematics = TTree("ParticleKinematics","ParticleKinematics")
    ParticleKinematics = kin.TTree_init_(ParticleKinematics)

    # loop over all events in the file                                        
    for n,event in enumerate(reader):
        
        # filling Track TTree w/ tracks properties (Reconstructed information)
        track = TF.Track(event,track,data["CharginoCandidates"])
        Track.Fill()
        
        # filling Kinematics TTree w/ MC particles (MC information) only if "TrueInfo": 1 in .json
        if data["TrueInfo"]:
            kin = TF.Kinematics(event,kin)
            ParticleKinematics.Fill()

    # closing file "datafile"
    reader.close()

    # writing/saving .root file
    ROOTOutput.Write()
    ROOTOutput.Close()        

    # cleaning used objects
    Track = TTree()
    ParticleKinematics = TTree()
    track = 0
    kin = 0

# at the end of the loop over all the files write information in .json
x['end_of_process'] = datetime.today().isoformat()

# save .json file
with open("../infontuples_13899/"+data["NtupleOutputName"]+'.json', 'w') as outfile:
    json.dump(x, outfile)
