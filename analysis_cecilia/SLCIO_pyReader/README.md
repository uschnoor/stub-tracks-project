# SLCIO pyReader

SLCIO pyReader is a python code that translates some .slcio files information into ROOT TTrees saved in .root files.

## Installation
 
to downoload the directory:
```bash
git clone https://gitlab.cern.ch/uschnoor/stub-tracks-project/
```
to build directory environment:
```bash
cd analysis_cecilia/SLCIO_pyReader
./build.sh
```
library to run the code:
```bash
source /cvmfs/clicdp.cern.ch/iLCSoft/builds/nightly/x86_64-slc6-gcc7-opt/init_ilcsoft.sh
```
for the CLICdp Style:

<https://gitlab.cern.ch/CLICdp/Publications/Templates/Style/blob/master/CLICdpStyle/rootstyle/CLICdpStyle.C>

copy this file in the home directory.

## Usage

form this directory:
1. configure input values:
```bash
emacs cfg/InformationDataset.json
```
```json
{
	"NtupleOutputName" : "output_file_name",
	"DirectoryPath" : "directory/path/to/the/files/in/input/",
	"CharginoCandidates": 1,
	"TrueInfo": 1
}
```
- output_file_name does not require the extension .root.
- directory path to the files in input is used by glob python function to run over all the files in that directory
- CharginoCandidates flag: 
  1. if == 1 the code runs over all the tracks and saves those that survive to the cuts (cuts are specified in lib/TTreeFiller.py)
  2. if == 0 this method is skipped
- TrueInfo flag: 
  1. if == 1 all MC particles parameters are written in .root file.
  2. if == 0 this method is skipped

2. run the code:
```bash
cd main
python main.py
```
- .root files are saved in ./ntuple/ while time employed to process all .slcio files is saved in ./infontuple/ with the same name of .root files.

### Example

configuring .json file as:
```json
{
	"NtupleOutputName" : "no_overlay_negative_pol",
	"DirectoryPath" : "/eos/experiment/clicdp/grid/ilc/prod/clic/3tev/ch1ch1/CLIC_o3_v14/DST/00013899/",
	"CharginoCandidates": 1,
	"TrueInfo": 1
}
```
2001 .root files in ./ntuples are generated and a .json in ./infontuple is produced.

running an example code:
```bash
cd data_analysis
python data_analysis.py
```
the folowing result is plotted:
![](data_analysis/example_result/plot.png)

data_analysis.py is an example of reading .root files code and fournishes a method to remove tracks duplicates.

## Support

contact: c.ferrari57@campus.unimib.it
