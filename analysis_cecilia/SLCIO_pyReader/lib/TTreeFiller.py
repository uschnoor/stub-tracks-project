from __future__ import division
import pyLCIO
from pyLCIO import UTIL
from pyLCIO.UTIL import *
from math import sqrt, pi, atan, sin
from ROOT import TFile, TTree, TCanvas, TH1D, TF1, TLorentzVector, TVector3,kRed,kGreen,kDashed,kBlue
from array import array
from operator import itemgetter
from collections import Counter

#--------------------#
# ausiliar functions #
#--------------------#

def radius2(x,hits):
    return hits.at(x).getPositionVec().X()**2+hits.at(x).getPositionVec().Y()**2+hits.at(x).getPositionVec().Z()**2

def hit_radius2(hit):
    return hit.getPositionVec().X()**2+hit.getPositionVec().Y()**2+hit.getPositionVec().Z()**2

def length(hit1,hit2):
    return sqrt(abs(hit_radius2(hit2) - hit_radius2(hit1)))

def sort_by_radius (v_hits):
    x = []
    for l in range(len(v_hits)):   
        x.append((l,radius2(l,v_hits)))
    sorted(x,key=itemgetter(1), reverse=True)
    x = zip(*x)
    return x[0]
        
def mode(l):
    # MODE finds the mode value in a list
    counter = Counter(l)
    max_count = max(counter.values())
    return([item for item,count in counter.items() if count == max_count])

#---------------------------------------#
#                                       #
#           Class TRACKINIT:            #
#    1.initialization class for the     #
# track useful parameters that fill the #
#     Track TTree of the .root file     # 
#    2.initialization of the TTree      #
#   branches (function TTree_init_())   #
#                                       #
#---------------------------------------#

class TrackInit:
    
    # attributes initialization --> only these 
    # parameters will be saved in .root output file
    tmaxsize = 1000

    # tracks reconstructed info
    tracksperevent = array('i',[0])          # number of tracks per event
    hits = array('i',tmaxsize*[0])           # number of hits per track
    status = array('i',tmaxsize*[0])         # getGeneratorStatus value per track
    D0 = array('f',tmaxsize*[0.])            # D0 reconstructed parameter 
    Z0 = array('f',tmaxsize*[0.])            # Z0 reconstructed parameter  
    phi = array('f',tmaxsize*[0.])           # phi reconstructed parameter 
    theta = array('f',tmaxsize*[0.])         # theta reconstructed parameter 
    curvature = array('f',tmaxsize*[0.])     # curvature reconstructed parameter (from Track -> getOmega())
    vertexr = array('f',tmaxsize*[0.])       # vertexr = sqrt(recoD0^2 + recoZ0^2)
    pt = array('f',tmaxsize*[0.])            # pt parameter calculated from the curvature
    p = array('f',tmaxsize*[0.])             # p parameter calculated from pt and theta
    length = array('f',tmaxsize*[0.])        # length parameter calculated from hits position
    endpoint = array('f',tmaxsize*[0.])      # endpoint = last hit position
    detector_layer = array('f',tmaxsize*[0.])# detector_layer = (last hit detector number) + (last hit layer number)*0.1 -> if some length analysis is needed
    # tracks associated MC info
    pdgid = array('f',tmaxsize*[0.])         # PDG id of the MC particle associated to the track: only if more than 50% of the track hits belong to that MC particle
    dEdx_average = array('f',tmaxsize*[0.])  # <dEdx> (average on the whole set of hits). MC information!
    dEdx_max = array('f',tmaxsize*[0.])      # max(dEdx) (on the whole set of hits). MC information!
    t_theta = array('f',tmaxsize*[0.])       # track true theta parameter
    t_phi = array('f',tmaxsize*[0.])         # track true phi parameter
    t_pt = array('f',tmaxsize*[0.])          # track true pt parameter
    t_purity = array('f',tmaxsize*[0.])      # track purity
    # chargino candidate search
    charginosperevent = array('i',[0])       # number of chargino candidate per event (selected on the cuts reported in Track function)
    c_theta = array('f',tmaxsize*[0.])       # chargino track candidate theta
    c_phi = array('f',tmaxsize*[0.])         # chargino track candidate phi
    c_dEdx_average = array('f',tmaxsize*[0.])# chargino track candidate <dEdx>
    c_pt = array('f',tmaxsize*[0.])          # chargino track candidate pt
    c_pdg = array('f',tmaxsize*[0.])         # chargino track candidate PDG id
    # tracks-pfos association
    npfotot = array('i',[0])                 # number of pfos per event
    npfo = array('i',tmaxsize*[0])           # number of pfos associated per track
    pfotheta = array('f',tmaxsize*[0.])      # pfo theta 
    pfophi = array('f',tmaxsize*[0.])        # phi theta

    
    def _init_(self):
        self.tracksperevent = 50

    # initialization of the TTree: creating branches associated to the class attributes
    def TTree_init_(self,TreeTrack):
        # tracks reconstructed info
        TreeTrack.Branch('TracksperEvent',self.tracksperevent,'TracksperEvent/I')
        TreeTrack.Branch('TrackHits',self.hits,'TrackHits[TracksperEvent]/I')
        TreeTrack.Branch('TrackStatus',self.status,'TrackStatus[TracksperEvent]/I')
        TreeTrack.Branch('TrackD0',self.D0,'TrackD0[TracksperEvent]/F')
        TreeTrack.Branch('TrackZ0',self.Z0,'TrackZ0[TracksperEvent]/F')
        TreeTrack.Branch('TrackPhi',self.phi,'TrackPhi[TracksperEvent]/F')
        TreeTrack.Branch('TrackTheta',self.theta,'TrackTheta[TracksperEvent]/F')
        TreeTrack.Branch('TrackCurvature',self.curvature,'TrackCurvature[TracksperEvent]/F')
        TreeTrack.Branch('TrackVertexR',self.vertexr,'TrackVertexR[TracksperEvent]/F')
        TreeTrack.Branch('TrackPt',self.pt,'TrackPt[TracksperEvent]/F')
        TreeTrack.Branch('TrackP',self.p,'TrackP[TracksperEvent]/F')
        TreeTrack.Branch('TrackL',self.length,'TrackL[TracksperEvent]/F')
        TreeTrack.Branch('TrackEndPoint',self.endpoint,'TrackEndPoint[TracksperEvent]/F')
        TreeTrack.Branch('TrackLastHitDetector.Layer',self.detector_layer,'TrackLastHitDetector.Layer[TracksperEvent]/F')
        # tracks associated MC info
        TreeTrack.Branch('TrackPDGId',self.pdgid,'TrackPDGId[TracksperEvent]/F')
        TreeTrack.Branch('TrackdEdxAverage',self.dEdx_average,'TrackdEdxAverage[TracksperEvent]/F')
        TreeTrack.Branch('TrackdEdxMax',self.dEdx_max,'TrackdEdxMax[TracksperEvent]/F')
        TreeTrack.Branch('TrackTrueTheta',self.t_theta,'TrackTrueTheta[TracksperEvent]/F')
        TreeTrack.Branch('TrackTruePhi',self.t_phi,'TrackTruePhi[TracksperEvent]/F')
        TreeTrack.Branch('TrackTruePt',self.t_pt,'TrackTruePt[TracksperEvent]/F')
        TreeTrack.Branch('TrackPurity',self.t_purity,'TrackPurity[TracksperEvent]/F')
        # chargino candidate search
        TreeTrack.Branch('CharginoCandidatesperEvent',self.charginosperevent,"CharginoCandidatesperEvent/I")
        TreeTrack.Branch('CharginoCandidateTheta',self.c_theta,'CharginoCandidateTheta[CharginoCandidatesperEvent]/F')
        TreeTrack.Branch('CharginoCandidatePhi',self.c_phi,'CharginoCandidatePhi[CharginoCandidatesperEvent]/F')
        TreeTrack.Branch('CharginoCandidatedEdxAverage',self.c_dEdx_average,'CharginoCandidatedEdxAverage[CharginoCandidatesperEvent]/F')
        TreeTrack.Branch('CharginoCandidatePt',self.c_pt,'CharginoCandidatePt[CharginoCandidatesperEvent]/F')
        TreeTrack.Branch('CharginoCandidatePDG',self.c_pdg,'CharginoCandidatePDG[CharginoCandidatesperEvent]/F')
        # tracks-pfo association
        TreeTrack.Branch('TrackNPFOTot',self.npfotot,'TrackNPFOTot/I')
        TreeTrack.Branch('TrackNPFO',self.npfo,'TrackNPFO[TracksperEvent]/I')
        TreeTrack.Branch('TrackPFOtheta',self.pfotheta,'TrackPFOTheta[TracksperEvent]/F')        
        TreeTrack.Branch('TrackPFOPhi',self.pfophi,'TrackPFOPhi[TracksperEvent]/F')

        return TreeTrack


#---------------------------------------#
#                                       #
#                TRACK:                 #
# for the event "event" fills the class #
#     ptrack reading the collection     #
#           SiTracks_Refitted           #
#                                       #
#---------------------------------------#

def Track(event,ptrack,Candidates):

    # loading useful collections 
    tCollection = event.getCollection("SiTracks_Refitted")     # Tracks collection
    pfoCollection = event.getCollection("PandoraPFOs")         # TightSelectedPandoraPFOs is not enough for this study: background particles and emitted soft pions have very low pT.
   
    # initializing event "lenght" (it will be at the end modified discarding useless information)
    ptrack.tracksperevent[0] = int(len(tCollection))
    ptrack.charginosperevent[0] = ptrack.tracksperevent[0]
    ptrack.npfotot[0] = int(len(pfoCollection))

    # pyLCIO LCRelationNavigator is used to link tracks simhits to MC particle in MCPhysicsParticles collection
    relations = {} 
    # hits collections
    trackerHitCollections = ["VXDTrackerHits","VXDEndcapTrackerHits","ITrackerHits","OTrackerHits","ITrackerEndcapHits","OTrackerEndcapHits"]
    trackerHitRelationCollections = ["VXDTrackerHitRelations","VXDEndcapTrackerHitRelations","InnerTrackerBarrelHitsRelations","OuterTrackerBarrelHitsRelations","InnerTrackerEndcapHitsRelations","OuterTrackerEndcapHitsRelations"]
    for ntrackerHitCollection,trackerHitCollection in enumerate(trackerHitCollections):
        if  event.getCollection(trackerHitCollection).getNumberOfElements() == 0: continue  # if the collection is empty go to the next one
        relation = pyLCIO.UTIL.LCRelationNavigator(event.getCollection(trackerHitRelationCollections[ntrackerHitCollection]))
        relations[ntrackerHitCollection] = relation  # append the relation to relations initialized outside the loop

    # pyLCIO LCRelationNavigator is used to link tracks to pfo in PandoraPFOs collection
    pforelation = pyLCIO.UTIL.LCRelationNavigator(event.getCollection("SiTracks_Refitted"))

    # loop initialization (information per event)
    npfotot = 0      # number of pfo per event
    ntracks = 0      # number of tracks per event
    c_ntracks = 0    # number of chargino candidates per event

    # loop over all tracks in event "event" (SiTracks_Refitted collection)
    for nt,t in enumerate(tCollection):
        # check on track hits number
        if t.getTrackerHits().size()==0:
            continue 
        # loops initialization (information per track)
        npfo = 0            # number of track associated pfo

        # loop on the pfo in PandoraPFOs
        for pfo in pfoCollection:
            pfotracks = pfo.getTracks()
            if len(pfotracks)==0: continue     # skip empty track information pfo
            pfoobj = pfotracks.at(0)
            if pfoobj.id() == t.id():          # track-pfo track association 
                npfo +=1       # per track
                npfotot += 1   # per event
                if len(pfotracks) >1 : print('more than one', len(pfotracks))  # check if there is something wrong
                # filling some pfo information
                ptrack.pfotheta[ntracks] = pi/2-atan(pfoobj.getTanLambda())
                ptrack.pfophi[ntracks] = pfoobj.getPhi()
                break

        # get the tracker hits
        hits = t.getTrackerHits()
        # sorting hits per distance from axis origin
        if len(hits)>1:
            index = sort_by_radius(hits)
        else: index[0] = 0
        l = length(hits[index[0]], hits[index[-1]])   # length definition
        
        # loop initialization (information per track)
        mcpertrack = []    # list of MC particles whose hits are on the track
        pdgs = []          # list of pdg ids of the MC particles associated to the track hits
        status = []        # list of GeneratorStatus of the MC particles associated to the track hits
        truetheta = []     # list of true thetas of the MC particles associated to the track hits
        truept = []        # list of true pts of the MC particles associated to the track hits
        truephi = []       # list of true phis of the MC particles associated to the track hits
        dEdx_point = 0     # dEdx of the hit
        dEdx_max = 0       # max(dEdx) between the already-read data
        points = 0         # number of hits to average on
        purity = 0         # track purity
        mc_maxhits = 0     # counter of number of hits for MC particle with max hits 

        # loop over all the track hits
        for i in index:
            hit = hits[i]
            # get the sim hit
            for subdetector,relation in relations.items():
                simHit = relation.getRelatedToObjects(hit)
                if len(simHit) == 0: continue
                simhit = simHit.at(0)
                # get MC Particle information if the particle exists (no overlay)
                try: 
                    particle = simhit.getMCParticle()
                    mcpertrack.append(str(particle))
                    #if( len(Counter(mcpertrack).keys()) > 1):
                        #print("****More than 1 mc: "+str(mcpertrack))
                        #print("++++Highest frequency occurrence: "+str(max(Counter(mcpertrack).values())))
                    status.append(particle.getGeneratorStatus())
                    lorentzparticle = particle.getLorentzVec()
                    truetheta.append(lorentzparticle.Theta()*180/pi)
                    truept.append(lorentzparticle.Pt())
                    truephi.append(lorentzparticle.Phi())
                # if not possible save standard values
                except: 
                    mcpertrack.append(0)
                    status.append(0)
                    truetheta.append(200)
                    truept.append(-200)
                    truephi.append(400)

                # try to get PDG id of the simhit
                try: 
                    pdg = simhit.getMCParticle().getPDG()
                # if not assign a standard value
                except: 
                    pdg = 0
                # collect information
                pdgs.append(pdg)
                dEdx_point += simhit.getEDep()/simhit.getPathLength()
                dEdx_max = max(dEdx_max,dEdx_point) 
                points +=1
                # if it is the last hit, collect information on its position in the detector
                if hit == hits[-1]:
                    detector = subdetector

        # collect information that will fill the TTree
        
        # association MC particle-track occurs only when the track hits
        # mostly belong to the same particle --> mode in the pdg list
        pdg = mode(pdgs)    # pdg is now a list
        if len(pdg) != 1:   # if pdg has more than one entry, it should be counted as overlay
            pdg = [0]
            pdgs = [0]

        dEdx_point /= points                # get the average of dEdx
        layer = t.getSubdetectorHitNumbers().at(index[-1])

        index_true = pdgs.index(pdg[0])     # true information position in the lists 
        
        purity = max(Counter(mcpertrack).values())/len(hits)

        # filling ptrack attributes
        # tracks reconstructed info
        ptrack.hits[ntracks] = len(hits)
        ptrack.status[ntracks] = status[index_true]
        ptrack.D0[ntracks] = t.getD0()
        ptrack.Z0[ntracks] = t.getZ0()
        ptrack.phi[ntracks] = t.getPhi()
        ptrack.theta[ntracks] = (pi/2-atan(t.getTanLambda()))*180/pi
        ptrack.curvature[ntracks] = abs(t.getOmega())
        ptrack.vertexr[ntracks] = sqrt(ptrack.D0[ntracks]**2+ptrack.Z0[ntracks]**2)
        if ptrack.curvature[ntracks] !=0: ptrack.pt[ntracks] = 0.3*4/(ptrack.curvature[ntracks]*1000)
        if sin(ptrack.theta[ntracks])!=0: ptrack.p[ntracks] = ptrack.pt[ntracks]/sin(ptrack.theta[ntracks])
        ptrack.length[ntracks] = l
        ptrack.endpoint[ntracks] = sqrt(hit_radius2(hits[index[-1]]))
        ptrack.detector_layer[ntracks] = detector + 0.1*layer
        # tracs associated MC info
        ptrack.pdgid[ntracks] = pdg[0]
        ptrack.dEdx_average[ntracks] = dEdx_point
        ptrack.dEdx_max[ntracks] = dEdx_max
        ptrack.t_theta[ntracks] = truetheta[index_true]
        ptrack.t_phi[ntracks] = truephi[index_true]
        ptrack.t_pt[ntracks] = truept[index_true]
        ptrack.t_purity[ntracks] = purity
        # filling some pfo-association
        ptrack.npfo[ntracks] = npfo
        ntracks += 1
        
        # chargino candidate analysis
        if not Candidates: continue
        # cuts that can be applied 
        #if ptrack.vertexr[ntracks-1] > 0.5: continue 
        if ptrack.npfo[ntracks-1] != 0 : continue
        #if ptrack.pt[ntracks-1]<10: continue
        #if ptrack.dEdx_average[ntracks]<0.0005: continue
        # chargino candidate tracks filling
        ptrack.c_theta[c_ntracks] = ptrack.theta[ntracks-1] 
        ptrack.c_phi[c_ntracks] = ptrack.phi[ntracks-1]
        ptrack.c_dEdx_average[c_ntracks] = ptrack.dEdx_average[ntracks-1]
        ptrack.c_pt[c_ntracks] = ptrack.pt[ntracks-1]
        ptrack.c_pdg[c_ntracks] = ptrack.pdgid[ntracks-1]
        c_ntracks += 1

    # filling information per event
    ptrack.charginosperevent[0] = c_ntracks
    ptrack.tracksperevent[0] = ntracks
    ptrack.npfotot[0] = npfotot

    return ptrack



#---------------------------------------#
#                                       #
#        Class KINEMATICSINIT:          #
#    1.initialization class for the     #
#  useful MC  parameters that fill the  #
#  Kinematics TTree of the .root file   # 
#    2.initialization of the TTree      #
#   branches (function TTree_init_())   #
#                                       #
#---------------------------------------#

class KinematicsInit:

    pmaxsize = 130

    particleperevent = array('i', [0])    # number of particle per event
    pt = array ('f',pmaxsize*[-200.])     # pt parameter
    theta = array ('f',pmaxsize*[-20.])   # theta parameter
    phi = array ('f',pmaxsize*[-200.])    # phi parameter
    pdgid = array('i',pmaxsize*[0])       # pdgid

    def _init_(self):
        self.particleperevent[0] = 130

    def TTree_init_(self, TreeParticleKinematics):
        # tree branches initialization
        TreeParticleKinematics.Branch('SIMParticleperEvent',self.particleperevent,'SIMParticleperEvent/I')
        TreeParticleKinematics.Branch('SIMPt',self.pt,'SIMPt[SIMParticleperEvent]/F')
        TreeParticleKinematics.Branch('SIMTheta',self.theta,'SIMTheta[SIMParticleperEvent]/F')
        TreeParticleKinematics.Branch('SIMPhi',self.phi,'SIMPhi[SIMParticleperEvent]/F')
        TreeParticleKinematics.Branch('SIMPDG',self.pdgid,'SIMPDG[SIMParticleperEvent]/I')

        return TreeParticleKinematics


#---------------------------------------#
#                                       #
#             KINEMATICS:               #
# for the event "event" fills the class #
#  pkinematics reading the collection   #
#           MCPhysicsParticles          #
#                                       #
#---------------------------------------#

def Kinematics(event,pkinematics):
    # loading the collection
    pCollection = event.getCollection("MCPhysicsParticles")
    # get event MC particle "length"
    pkinematics.particleperevent[0] = len(pCollection)

    nparticles = 0

    for np,p in enumerate(pCollection) :
        # get particle info                                                                                                                                                                                                              
        if p.getGeneratorStatus()== 0:      # if GenStatus == 0 discard the particle
            continue

        if p.getSimulatorStatus() == -2147483648:   # if SimStatus == -2147483648, the particle belongs to the beam -> discard it
            continue
        # fill pkinematics w/ some MC information
        v = p.getLorentzVec()
        pkinematics.theta[nparticles]= v.Theta()*180/pi
        pkinematics.pt[nparticles] = v.Pt()
        pkinematics.phi[nparticles] = v.Phi()
        pkinematics.pdgid[nparticles] = p.getPDG()
        nparticles += 1

    # filling information per event
    pkinematics.particleperevent[0] = nparticles

    return pkinematics
