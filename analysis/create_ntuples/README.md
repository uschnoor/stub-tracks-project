# Documentation of the branches in the ntuples

## Track
filled in `lib/TrackTF.py`
### tracks reconstructed info
+ `TracksperEvent`: total number of tracks (SiTracks_Refitted) in the event (filled per event)	
+ `TrackHits`: total number of hits obtained from getTrackerHits() on the track (SiTracks_Refitted), i.e. number of hits used to build this track (filled as array of n_tracks values per event)
+ `TrackStatus`: GeneratorStatus of each track in the event
+ `TrackD0`				
+ `TrackZ0`				
+ `TrackPhi`			
+ `TrackTheta`			
+ `TrackCurvature`			
+ `TrackVertexR`			
+ `TrackPt`				
+ `TrackP`				
+ `TrackL`				
+ `TrackEndPoint`			
+ `TrackLastHitDetector.Layer`

### tracks associated MC info

+ `TrackPDGId`
+ `TrackdEdxAverage`
+ `TrackdEdxMax`	
+ `TrackTrueTheta`			
+ `TrackTruePhi`		
+ `TrackTruePt`		
+ `TrackPurity`

### chargino candidate search
CharginoCandidatesperEvent	
CharginoCandidateTheta		
CharginoCandidatePhi		
CharginoCandidatedEdxAverage	
CharginoCandidatePt		
CharginoCandidatePDG		

### tracks-pfoassociation
TrackNPFOTot			
TrackNPFO			
TrackPFOtheta			
TrackPFOPhi			


## MCParticle
MC_ParticleperEvent	
MC_Pt			
MC_Theta		
MC_Phi			
MC_PDG			


## MCEventInfo
n_event