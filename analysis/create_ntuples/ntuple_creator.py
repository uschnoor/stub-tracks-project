#!/usr/bin/env python
import sys
sys.path.append('..')
#import objgraph
import os
from pyLCIO import IOIMPL
from pyLCIO import UTIL
from pyLCIO.UTIL import LCTOOLS
from ROOT import gStyle, gROOT
from math import sqrt, pi
from ROOT import TFile, TTree, TCanvas, TH1D, TF1, TLorentzVector, TVector3,kRed,kGreen,kDashed,kBlue
from datetime import datetime
from array import array
import json
import glob
from lib import MCParticlesTF, MCEventInfoTF
from lib import TrackTF, EventTF
gStyle.SetOptStat(0)
gStyle.SetLineWidth(2)
gStyle.SetTitleFont(42)
gStyle.SetLabelFont(42)
gStyle.SetPadTickX(1)
gStyle.SetPadTickY(1)
gStyle.SetLabelSize(0.04)
gROOT.ForceStyle()


#-------------------------------------#
#                                     #
#           ANALYSIS CODE             #
#     to configure parameters see     #
#    ../cfg/InformationDataset.json   #
#                                     #
#-------------------------------------#

def configure_parameters(run_config_file):
    with open(run_config_file) as json_run_file:
	run_conf = json.load(json_run_file)
    dataset_config_file = run_conf["DatasetInformationFile"]
    try: 
        with open(dataset_config_file) as json_datasets_file:
	    all_datasets_conf = json.load(json_datasets_file)
    except IOError:
        print("could not find {}".format(dataset_config_file))
        dataset_config_file = dataset_config_file.replace("../cfg/","cfg/")
        print("trying instead with this one: {}".format(dataset_config_file))
        with open(dataset_config_file) as json_datasets_file:
	    all_datasets_conf = json.load(json_datasets_file)
        
    for i in run_conf["ntuplecreator_prodids"]:
	#for now, build it like this, but this needs to be modified once using more than one prodid!!
	prod_id_str = str(i)
	dataset_conf = all_datasets_conf[prod_id_str]
	input_conf = {"NtupleOutputName" : "{}_{}mm_{}_".format(
            run_conf["NtupleOutputPrefix"],
            dataset_conf["ctau_in_mm"],
            dataset_conf["polarization"]),
	              "dataset_conf" : dataset_conf,
	              "DirectoryPath" : dataset_conf["path_to_DSTs"],
		      "CharginoCandidates": 1,
		      "TrueInfo": 1
	}
        input_conf.update(run_conf) #run_conf takes precendence just in case one of these things should be overwritten
        
    return input_conf

def get_list_of_input_files(input_conf):
    '''
    creates the list of input files from the config

    it currently assumes that they are always within a subdirectory and that there is one prodID per sample
    '''
    return glob.iglob(str(input_conf["DirectoryPath"])+'/*/*.slcio')



def main(argv):
    if len(argv) == 0:
        print("Please always give the run_config file as argument, e.g. \n python main.py ../cfg/run_info_local.json")
        sys.exit()

        
    # configuring parameters
    #input_conf = configure_parameters('../cfg/run_info_local.json','../cfg/info_on_all_datasets.json')
    input_conf = configure_parameters(argv[0])
    	
    run_meta_info = {
	'initialization_time': datetime.today().isoformat(),
	'configuration' : input_conf
    }
	

    list_of_filenames = get_list_of_input_files(input_conf)
    
    #for testing purposes
    if input_conf["TestMode"] ==1:
        list_of_filenames = list(list_of_filenames)[0:2]

    #print(argv)

    #this is used for the grid submission, but can also be used locally now.
    if len(argv) == 2:
        #list_of_filenames = [i.split('/')[-1] for i in  argv[1]]
        list_of_filenames = argv[1].split(",")
        if input_conf["GridMode"]:
            #in that case, the files are transfered into the local directory.
            #therefore, the preceding path must be removed
            list_of_filenames_tmp = []
            for fn in list_of_filenames:
                fn_new = fn.split('/')[-1]
                list_of_filenames_tmp.append(fn_new)
            list_of_filenames = list_of_filenames_tmp
    #print list_of_filenames

    ntuple_filling_event_loop(list_of_filenames, input_conf, run_meta_info)


def ntuple_filling_event_loop(list_of_filenames, input_conf, run_meta_info):
    # create a reader for a LCIO file
    reader = IOIMPL.LCFactory.getInstance().createLCReader()



    # code core: loop on the LCIO files
    for ndatafile,datafile in enumerate(list_of_filenames):
        
        print("trying to open {}".format(str(datafile)))
	reader.open(str(datafile))
	#if ndatafile % 100 == 0:
	print ("Opening SLCIO file {}".format(datafile))
	
        #outputfilename must be based on inputfile name
        file_number = str(datafile).split('/')[-1].replace(".slcio","")
        
	# initializing output .root
        outputfilename = input_conf["NtupleOutputDir"]+"/"+input_conf["NtupleOutputPrefix"]+file_number+".root"

        
        if input_conf["GridMode"]:
            outputfilename = input_conf["NtupleOutputPrefix"]+str(datafile).split('/')[-1].replace(".slcio",".root")
            #outputfilename = input_conf["NtupleOutputName"]+str(list_of_filenames[0]).split('/')[-1].replace(".slcio",".root")


	ROOTOutput = TFile(outputfilename,"RECREATE")
        print("Saving the ntuple to {}".format(outputfilename))
	
	# initializing tree structure for track tree
	track_tree_filler = TrackTF.TrackTreeFiller()
	TrackTree = TTree("Track","Track")
	TrackTree = track_tree_filler.initialize_branches(TrackTree)


        # initializing tree structure for event tree
        event_tree_filler = EventTF.EventTreeFiller()
        EventTree = TTree("Event","Event")
        EventTree = event_tree_filler.initialize_branches(EventTree)

	# filling MC Particles TTree w/ MC particles (MC information) only if "TrueInfo": 1 in .json
	if input_conf["TrueInfo"]:
	  # initializing tree structure with montecarlo particles information
	  mcparticles_tree_filler = MCParticlesTF.MCParticlesTreeFiller()
	  MCParticlesTree = TTree("MCParticles","MCParticles")
	  MCParticlesTree = mcparticles_tree_filler.initialize_branches(MCParticlesTree)

	# loop over all events in the file                                        
	for n,event in enumerate(reader):
	    #if n % 1000 == 0:
	    print("running on event {}".format(n))
	    
	    # filling MC Particles TTree w/ MC particles (MC information) only if "TrueInfo": 1 in .json
	    if input_conf["TrueInfo"]:
	        mcptf = mcparticles_tree_filler.fill_tree(event)
	        MCParticlesTree.Fill()

	    # filling Track TTree w/ tracks properties (Reconstructed information)
	    track = track_tree_filler.fill_tree(event,input_conf["CharginoCandidatesInfo"])
	    TrackTree.Fill()
	    # number PFO analysis: if generated by the signal
	    eventinfo = event_tree_filler.fill_tree(event, input_conf["PreselectCharginoCandidates"])
	    EventTree.Fill()
	    print("\n")
	
	# closing the current file
	reader.close()
	
	# write and save the .root output file
        print("Writing and saving the tree ntuple to {}".format(ROOTOutput.GetName()))
	ROOTOutput.Write()
	ROOTOutput.Close()        
	
	# debug memory parameters
	tot_m, used_m, free_m = map(int, os.popen('free -t -m').readlines()[-1].split()[1:])
	print('tot_m = ',tot_m, 'used_m = ', used_m, 'free_m = ',free_m)

        EventTree             = TTree()
	TrackTree             = TTree()
	MCParticlesTree       = TTree()
        TrackTreeFiller       = 0
	MCParticlesTreeFiller = 0
	
	
    run_meta_info['end_of_process'] = datetime.today().isoformat()
	
    if not input_conf["GridMode"]:
        with open(input_conf["RunInfoOutputDir"]+input_conf["NtupleOutputPrefix"]+'.json', 'w') as outfile:
	    json.dump(run_meta_info, outfile)
	
    #raw_input()
	
if __name__ == "__main__":
    main(sys.argv[1:])
