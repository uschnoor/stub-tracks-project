from array import array
from math import sqrt, pi, atan, sin



class MCParticlesTreeFiller:
    """
    fills the "MCParticles" Tree

    This tree contains arrays of the different particles information in one event.
    For particle-wise information, they are filled for each of the (not-discarded) particles.
    """
    pmaxsize = 130

    # initialize the particle-wise arrays with a length of pmaxsize. Initialization value according to what is most useful
    
    particleperevent = array('i', [0]) #event-wise -> need only one entry
    pt               = array ('f',pmaxsize*[-200.])
    p                = array ('f',pmaxsize*[-200.])
    m                = array ('f',pmaxsize*[-200.])
    theta            = array ('f',pmaxsize*[-20.])
    phi              = array ('f',pmaxsize*[-200.])
    pdgid            = array ('i',pmaxsize*[0])

    endpoint_x         = array('f',pmaxsize*[-9999])
    endpoint_y         = array('f',pmaxsize*[-9999])
    endpoint_z         = array('f',pmaxsize*[-9999])
    endpoint_len       = array('f',pmaxsize*[-9999])
    endpoint_R         = array('f',pmaxsize*[-9999])
    displacement_boost = array('f',pmaxsize*[-9999])
    
    def _init_(self):
        self.particleperevent[0] = 130


        
    def initialize_branches(self, MCParticleTree): #initialize_branches (self, MCParticleTree)
        """ initializes the branches of the TTree MCParticleTree that is handed as argument 
        (created before)
        """
        MCParticleTree.Branch('MC_ParticleperEvent' ,self.particleperevent,'MC_ParticleperEvent/I')
        MCParticleTree.Branch('MC_Pt'               ,self.pt, 'MC_Pt[MC_ParticleperEvent]/F')
        MCParticleTree.Branch('MC_P'                ,self.p,  'MC_P[MC_ParticleperEvent]/F')
        MCParticleTree.Branch('MC_M'                ,self.m,  'MC_M[MC_ParticleperEvent]/F')
        MCParticleTree.Branch('MC_Theta'            ,self.theta, 'MC_Theta[MC_ParticleperEvent]/F')
        MCParticleTree.Branch('MC_Phi'              ,self.phi, 'MC_Phi[MC_ParticleperEvent]/F')
        MCParticleTree.Branch('MC_PDG'              ,self.pdgid, 'MC_PDG[MC_ParticleperEvent]/I')

        # branches needed for the life-time reweighting
        MCParticleTree.Branch('MC_endpoint_x'       ,self.endpoint_x,   'MC_endpoint_x[MC_ParticleperEvent]/F'     )
        MCParticleTree.Branch('MC_endpoint_y'       ,self.endpoint_y,   'MC_endpoint_y[MC_ParticleperEvent]/F'     )
        MCParticleTree.Branch('MC_endpoint_z'       ,self.endpoint_z,   'MC_endpoint_z[MC_ParticleperEvent]/F'     )
        MCParticleTree.Branch('MC_endpoint_len'     ,self.endpoint_len, 'MC_endpoint_len[MC_ParticleperEvent]/F'   )
        MCParticleTree.Branch('MC_endpoint_R'       ,self.endpoint_R,   'MC_endpoint_R[MC_ParticleperEvent]/F'     )

        MCParticleTree.Branch('MC_displacement_boost',self.displacement_boost,   'MC_displacement_boost[MC_ParticleperEvent]/F'     )

        return MCParticleTree


    
    def is_selected_status(self,particle):
        """
        returns True in the cases where the particle should be selected
        particle: an entry of a particle collection
        """
        if particle.getGeneratorStatus() == 0: # if GenStatus == 0 discard the particle
            return False
        if particle.getSimulatorStatus() == -2147483648:
            # if SimStatus == -2147483648, the particle belongs to the beam -> discard it
            return False
        else:
            return True
                

    def fill_tree(self, event):
        pCollection = event.getCollection("MCPhysicsParticles")

        self.particleperevent[0] = len(pCollection)

        nparticles = 0

        # particle loop
        for np,particle in enumerate(pCollection) :

            # discard particles not fulfilling the criteria on the status
            if not self.is_selected_status(particle):
                continue
           
            # fill self w/ some MC information
            v = particle.getLorentzVec()
            self.theta[nparticles] = v.Theta()*180/pi #in degree
            self.pt[nparticles]    = v.Pt()
            self.phi[nparticles]   = v.Phi()
            self.pdgid[nparticles] = particle.getPDG()
            self.p[nparticles]     = v.P()
            self.m[nparticles]     = v.M()
            epv = particle.getEndpointVec()

            self.endpoint_x[nparticles]    =  epv.X()
            self.endpoint_y[nparticles]    =  epv.Y()
            self.endpoint_z[nparticles]    =  epv.Z()
            self.endpoint_len[nparticles]  =  epv.Mag()
            self.endpoint_R[nparticles]    =  epv.Perp()

            mass = v.M()

            #if particle.getPDG()==1000024:
            #    print mass
            if v.P() !=0:
                self.displacement_boost[nparticles] = mass * epv.Mag()/v.P()
            # for charginos, this is 1050.* epv.Mag()/pchi
            
            nparticles += 1
   
        self.particleperevent[0] = nparticles

        return self

