from array import array
from math import sqrt, atan, pi, sin
import pyLCIO
from ROOT import TVector3
from pyLCIO import UTIL
from pyLCIO.UTIL import *
from collections import Counter 
from operator import itemgetter

class Candidate:

    true_pdgid = 0
    pt         = -999.
    p          = -99999.
    theta      = -999.
    phi        = -999.
    vertexR    = -999.
    n_assoc_pfo= -9e-2
    d0         = - 9
    z0         = -9
    track_pTfraction_cone01 = -9.

    #charginos only 
    lasthitR   = -9e3
    firsthitR   = -9e3
    dEdx_avrg  = -9e-3
    n_conepions = -9

    #pions only 
    id_ass_ch  = 0
    dist_ch    = 0

    def _init_(self):
        return
    
    

class EventTreeFiller:

    #pt (GeV)
    #phi (rad)   = [ -pi, pi ] 
    #theta (rad) = [ 0, pi ] 
    true_chm_pt        = array('f',[-999])
    true_chm_p         = array('f',[-999]) 
    true_chm_theta     = array('f',[-999])
    true_chm_phi       = array('f',[-999])
    true_chm_vtxR      = array('f',[-999])
    true_chm_e         = array('f',[-999])
    true_chm_trajlen   = array('f',[-999])
    true_chm_endpointR = array('f',[-999])
    true_chm_m         = array('f',[-999])
    true_chm_pdgid     = array('i',[0])

    true_chp_pt        = array('f',[-999])
    true_chp_p         = array('f',[-999]) 
    true_chp_theta     = array('f',[-999])
    true_chp_phi       = array('f',[-999])
    true_chp_vtxR      = array('f',[-999])
    true_chm_e         = array('f',[-999])
    true_chp_e         = array('f',[-999])
    true_chp_trajlen   = array('f',[-999])
    true_chp_endpointR = array('f',[-999])
    true_chp_m         = array('f',[-999])
    true_chp_pdgid     = array('i',[0])
    
    truepionm_pt      = array('f',[0])
    truepionm_p       = array('f',[0])
    truepionm_theta   = array('f',[0])
    truepionm_phi     = array('f',[0])
    truepionm_vtxR    = array('f',[-999])
    true_chm_e        = array('f',[-999])
    truepionm_e       = array('f',[0])
    truepionm_m       = array('f',[0])
    truepionm_pdgid   = array('i',[0])
    
    truepionp_pt      = array('f',[0])
    truepionp_p       = array('f',[0])
    truepionp_theta   = array('f',[0])
    truepionp_phi     = array('f',[0])
    truepionp_vtxR    = array('f',[-999])
    truepionp_e       = array('f',[0])
    truepionp_m       = array('f',[0])
    truepionp_pdgid   = array('i',[0])
    
    n_ch_cand     = array('i',[0])
    
    ch1_true_pdgid = array('i',[0])
    ch1_pt         = array('f',[0])
    ch1_p          = array('f',[0])
    ch1_theta      = array('f',[0])
    ch1_phi        = array('f',[0])
    ch1_lasthitR   = array('f',[-999])
    ch1_firsthitR  = array('f',[-999])
    ch1_vertexR    = array('f',[-999])   # the R of the vertex where it originates
    ch1_dEdx_avrg  = array('f',[0])
    ch1_n_assocpfo = array('f',[-999])
    ch1_n_conepions= array('f',[0])
    ch1_d0         = array('f',[-999])
    ch1_z0         = array('f',[-999])
    ch1_pTfraction_cone01 = array('f', [999])

    ch2_true_pdgid = array('i',[0])
    ch2_pt         = array('f',[0])
    ch2_p          = array('f',[0])
    ch2_theta      = array('f',[0])
    ch2_phi        = array('f',[0])
    ch2_firsthitR  = array('f',[-999])
    ch2_lasthitR   = array('f',[-999])
    ch2_vertexR    = array('f',[-999])   # the R of the vertex where it originates
    ch2_dEdx_avrg  = array('f',[0])
    ch2_n_assocpfo = array('f',[-999])
    ch2_n_conepions= array('f',[0])
    ch2_d0         = array('f',[-999])
    ch2_z0         = array('f',[-999])
    ch2_pTfraction_cone01 = array('f', [999])

    n_pi_cand     = array('i',[0])

    pi1_true_pdgid = array('i',[0])
    pi1_pt         = array('f',[0])
    pi1_p          = array('f',[0])
    pi1_theta      = array('f',[0])
    pi1_phi        = array('f',[0])
    pi1_firsthitR  = array('f',[-999])
    pi1_lasthitR   = array('f',[-999])
    pi1_vertexR    = array('f',[-999])   # the R of the vertex where it originates
    pi1_n_assocpfo = array('f',[-999])
    pi1_d0         = array('f',[-999])
    pi1_z0         = array('f',[-999])
    pi1_pTfraction_cone01 = array('f', [999])

    pi2_true_pdgid = array('i',[0])
    pi2_pt         = array('f',[0])
    pi2_p          = array('f',[0])
    pi2_theta      = array('f',[0])
    pi2_phi        = array('f',[0])
    pi2_firsthitR  = array('f',[-999])
    pi2_lasthitR   = array('f',[-999])
    pi2_vertexR    = array('f',[-999])   # the R of the vertex where it originates
    pi2_n_assocpfo = array('f',[-999])
    pi2_d0         = array('f',[-999])
    pi2_z0         = array('f',[-999])
    pi2_pTfraction_cone01 = array('f', [999])

    _magnField     = 4.0

    def _init_(self):
        return
   
    

    def initialize_branches(self,EventTree):
        # all true charginos 
        EventTree.Branch('TrueChm_PT'        , self.true_chm_pt        , 'TrueChm_PT/F'      )
        EventTree.Branch('TrueChm_P'         , self.true_chm_p         , 'TrueChm_P/F'       )
        EventTree.Branch('TrueChm_Theta'     , self.true_chm_theta     , 'TrueChm_Theta/F'   )
        EventTree.Branch('TrueChm_Phi'       , self.true_chm_phi       , 'TrueChm_Phi/F'     )
        EventTree.Branch('TrueChm_VertexR'   , self.true_chm_vtxR      , 'TrueChm_VertexR/F' )
        EventTree.Branch('TrueChm_E'         , self.true_chm_e         , 'TrueChm_E/F'       )
        EventTree.Branch('TrueChm_TrajLen'   , self.true_chm_trajlen   , 'TrueChm_TrajLen/F' )
        EventTree.Branch('TrueChm_M'         , self.true_chm_m         , 'TrueChm_M/F'       )
        EventTree.Branch('TrueChm_PDGID'     , self.true_chm_pdgid     , 'TrueChm_PDGID/I'   )

        EventTree.Branch('TrueChp_PT'        , self.true_chp_pt        , 'TrueChp_PT/F'      )
        EventTree.Branch('TrueChp_P'         , self.true_chp_p         , 'TrueChp_P/F'       )
        EventTree.Branch('TrueChp_Theta'     , self.true_chp_theta     , 'TrueChp_Theta/F'   )
        EventTree.Branch('TrueChp_Phi'       , self.true_chp_phi       , 'TrueChp_Phi/F'     )
        EventTree.Branch('TrueChp_VertexR'   , self.true_chp_vtxR      , 'TrueChp_VertexR/F' )
        EventTree.Branch('TrueChp_E'         , self.true_chp_e         , 'TrueChp_E/F'       )
        EventTree.Branch('TrueChp_TrajLen'   , self.true_chp_trajlen   , 'TrueChp_TrajLen/F' )
        EventTree.Branch('TrueChp_M'         , self.true_chp_m         , 'TrueChp_M/F'       )
        EventTree.Branch('TrueChp_PDGID'     , self.true_chp_pdgid     , 'TrueChp_PDGID/I'   )
        # true pions
        EventTree.Branch('TruePionm_PT'      , self.truepionm_pt      , 'TruePionm_PT/F'        )
        EventTree.Branch('TruePionm_P'       , self.truepionm_p       , 'TruePionm_P/F'         )
        EventTree.Branch('TruePionm_Theta'   , self.truepionm_theta   , 'TruePionm_Theta/F'     )
        EventTree.Branch('TruePionm_Phi'     , self.truepionm_phi     , 'TruePionm_Phi/F'       )
        EventTree.Branch('TruePionm_VertexR' , self.truepionm_vtxR    , 'TruePionm_VertexR/F'   )
        EventTree.Branch('TruePionm_E'       , self.truepionm_e       , 'TruePionm_E/F'         )
        EventTree.Branch('TruePionm_M'       , self.truepionm_m       , 'TruePionm_M/F'         )
        EventTree.Branch('TruePionm_PDGID'   , self.truepionm_pdgid   , 'TruePionm_PDGID/I'     )
        
        EventTree.Branch('TruePionp_PT'      , self.truepionp_pt      , 'TruePionp_PT/F'        )
        EventTree.Branch('TruePionp_P'       , self.truepionp_p       , 'TruePionp_P/F'         )
        EventTree.Branch('TruePionp_Theta'   , self.truepionp_theta   , 'TruePionp_Theta/F'     )
        EventTree.Branch('TruePionp_Phi'     , self.truepionp_phi     , 'TruePionp_Phi/F'       )
        EventTree.Branch('TruePionp_VertexR' , self.truepionp_vtxR    , 'TruePionp_VertexR/F'   )
        EventTree.Branch('TruePionp_E'       , self.truepionp_e       , 'TruePionp_E/F'         )
        EventTree.Branch('TruePionp_M'       , self.truepionp_m       , 'TruePionp_M/F'         )
        EventTree.Branch('TruePionp_PDGID'   , self.truepionp_pdgid   , 'TruePionp_PDGID/I'     )

        # preselected chargino candidates
        EventTree.Branch('Ch_N', self.n_ch_cand , 'Ch_N/I')
        # the 2 highest pT chargino candidates
        EventTree.Branch('Ch1_truePDGID'   , self.ch1_true_pdgid , 'Ch1_truePDGID/I'         ) 
        EventTree.Branch('Ch1_PT'          , self.ch1_pt         , 'Ch1_PT/F'                )
        EventTree.Branch('Ch1_P'           , self.ch1_p          , 'Ch1_P/F'                 )
        EventTree.Branch('Ch1_Theta'       , self.ch1_theta      , 'Ch1_Theta/F'             )
        EventTree.Branch('Ch1_Phi'         , self.ch1_phi        , 'Ch1_Phi/F'               )
        EventTree.Branch('Ch1_FirstHitR'   , self.ch1_firsthitR  , 'Ch1_FirstHitR/F'         )
        EventTree.Branch('Ch1_LastHitR'    , self.ch1_lasthitR   , 'Ch1_LastHitR/F'          )
        EventTree.Branch('Ch1_VertexR'     , self.ch1_vertexR    , 'Ch1_VertexR/F'           )
        EventTree.Branch('Ch1_dEdx_Avrg'   , self.ch1_dEdx_avrg  , 'Ch1_dEdx_Avrg/F'         )
        EventTree.Branch('Ch1_N_AssocPFO'    , self.ch1_n_assocpfo   , 'Ch1_N_AssocPFO'      )
        EventTree.Branch('Ch1_N_ConePions' , self.ch1_n_conepions, 'Ch1_N_ConePions/F'       )
        EventTree.Branch('Ch1_D0'           , self.ch1_d0          , 'Ch1_D0/F'              )
        EventTree.Branch('Ch1_Z0'           , self.ch1_z0          , 'Ch1_Z0/F'              )
        EventTree.Branch('Ch1_PTFraction_cone01', self.ch1_pTfraction_cone01, 'Ch1_PTFraction_cone01/F')

        EventTree.Branch('Ch2_truePDGID'   , self.ch2_true_pdgid , 'Ch2_truePDGID/I'         ) 
        EventTree.Branch('Ch2_PT'          , self.ch2_pt         , 'Ch2_PT/F'                )
        EventTree.Branch('Ch2_P'           , self.ch2_p          , 'Ch2_P/F'                 )
        EventTree.Branch('Ch2_Theta'       , self.ch2_theta      , 'Ch2_Theta/F'             )
        EventTree.Branch('Ch2_Phi'         , self.ch2_phi        , 'Ch2_Phi/F'               )
        EventTree.Branch('Ch2_FirstHitR'   , self.ch2_firsthitR  , 'Ch2_FirstHitR/F'         )
        EventTree.Branch('Ch2_LastHitR'    , self.ch2_lasthitR   , 'Ch2_LastHitR/F'          )
        EventTree.Branch('Ch2_VertexR'     , self.ch2_vertexR    , 'Ch2_VertexR/F'           )
        EventTree.Branch('Ch2_dEdx_Avrg'   , self.ch2_dEdx_avrg  , 'Ch2_dEdx_Avrg/F'         )
        EventTree.Branch('Ch2_N_AssocPFO'    , self.ch2_n_assocpfo   , 'Ch2_N_AssocPFO'      )
        EventTree.Branch('Ch2_N_ConePions' , self.ch2_n_conepions, 'Ch2_N_ConePions/F'       )
        EventTree.Branch('Ch2_D0'           , self.ch2_d0          , 'Ch2_D0/F'              )
        EventTree.Branch('Ch2_Z0'           , self.ch2_z0          , 'Ch2_Z0/F'              )
        EventTree.Branch('Ch2_PTFraction_cone01', self.ch2_pTfraction_cone01, 'Ch2_PTFraction_cone01/F')

        # preselected pion candidates
        EventTree.Branch('Pi_N', self.n_pi_cand , 'Pi_N/I')
        # the 2 closest pion candidates to the associated charginos
        EventTree.Branch('Pi1_truePDGID'   , self.pi1_true_pdgid , 'Pi1_truePDGID/I'         )
        EventTree.Branch('Pi1_PT'          , self.pi1_pt         , 'Pi1_PT/F'                )
        EventTree.Branch('Pi1_P'           , self.pi1_p          , 'Pi1_P/F'                 )
        EventTree.Branch('Pi1_Theta'       , self.pi1_theta      , 'Pi1_Theta/F'             )
        EventTree.Branch('Pi1_Phi'         , self.pi1_phi        , 'Pi1_Phi/F'               )
        EventTree.Branch('Pi1_FirstHitR'   , self.pi1_firsthitR  , 'Pi1_FirstHitR/F'         )
        EventTree.Branch('Pi1_LastHitR'    , self.pi1_lasthitR   , 'Pi1_LastHitR/F'          )
        EventTree.Branch('Pi1_VertexR'     , self.pi1_vertexR    , 'Pi1_VertexR/F'           )
        EventTree.Branch('Pi1_N_AssocPFO'  , self.pi1_n_assocpfo , 'Pi1_N_AssocPFO'          )
        EventTree.Branch('Pi1_D0'          , self.pi1_d0         , 'Pi1_D0/F'                )
        EventTree.Branch('Pi1_Z0'          , self.pi1_z0         , 'Pi1_Z0/F'                )
        EventTree.Branch('Pi1_PTFraction_cone01', self.pi1_pTfraction_cone01, 'Pi1_PTFraction_cone01/F')

        EventTree.Branch('Pi2_truePDGID'   , self.pi2_true_pdgid , 'Pi2_truePDGID/I'         )
        EventTree.Branch('Pi2_PT'          , self.pi2_pt         , 'Pi2_PT/F'                )
        EventTree.Branch('Pi2_P'           , self.pi2_p          , 'Pi2_P/F'                 )
        EventTree.Branch('Pi2_Theta'       , self.pi2_theta      , 'Pi2_Theta/F'             )
        EventTree.Branch('Pi2_Phi'         , self.pi2_phi        , 'Pi2_Phi/F'               )
        EventTree.Branch('Pi2_FirstHitR'   , self.pi2_firsthitR  , 'Pi2_FirstHitR/F'         )
        EventTree.Branch('Pi2_LastHitR'    , self.pi2_lasthitR   , 'Pi2_LastHitR/F'          )
        EventTree.Branch('Pi2_VertexR'     , self.pi2_vertexR    , 'Pi2_VertexR/F'           )
        EventTree.Branch('Pi2_N_AssocPFO'  , self.pi2_n_assocpfo , 'Pi2_N_AssocPFO'          )
        EventTree.Branch('Pi2_D0'          , self.pi2_d0         , 'Pi2_D0/F'                )
        EventTree.Branch('Pi2_Z0'          , self.pi2_z0         , 'Pi2_Z0/F'                )
        EventTree.Branch('Pi2_PTFraction_cone01', self.pi2_pTfraction_cone01, 'Pi2_PTFraction_cone01/F')

        # event properties (MET etc)

        
        return EventTree

    def is_selected_status_mctruth(self,particle):
        """
        returns True in the cases where the particle should be selected
        particle: an entry of a particle collection
        """
        if particle.getGeneratorStatus() == 0: # if GenStatus == 0 discard the particle
            return False
        if particle.getSimulatorStatus() == -2147483648:
            # if SimStatus == -2147483648, the particle belongs to the beam -> discard it
            return False
        else:
            return True

        
    def createTrackerHitRelationDict(self,event):
        """
        creates a dictionary containing for this event the LCRelationNavigator objects that indicate the relation between a track and a Hit entry
        """
        # pyLCIO LCRelationNavigator is used to link tracks simhits to MC particle in MCPhysicsParticles collection (first it allows to get the SimTrackerHit related to the TrackerHit belonging to the track collection, and then based on the SimTrackerHit, one can get the MCParticle)
        relations = {} 
        # hits collections. The order has to be the same for the subdetectors in trackerHitCollections and trackerHitRelationCollections
        trackerHitCollections = ["VXDTrackerHits","VXDEndcapTrackerHits","ITrackerHits","OTrackerHits","ITrackerEndcapHits","OTrackerEndcapHits"]
        trackerHitRelationCollections = ["VXDTrackerHitRelations","VXDEndcapTrackerHitRelations","InnerTrackerBarrelHitsRelations","OuterTrackerBarrelHitsRelations","InnerTrackerEndcapHitsRelations","OuterTrackerEndcapHitsRelations"]
        
        for ntrackerHitCollection,trackerHitCollection in enumerate(trackerHitCollections):
            if  event.getCollection(trackerHitCollection).getNumberOfElements() == 0: continue  # if the collection is empty go to the next trackerHitCollection
            relation = pyLCIO.UTIL.LCRelationNavigator(event.getCollection(trackerHitRelationCollections[ntrackerHitCollection]))
            relations[ntrackerHitCollection] = relation  # append the relation to relations initialized outside the loop


        return relations

    def true_chargino_pion_properties(self,event):
        # fills the properties of the class that belong to the charginos and pions

        mctruth_collection = event.getCollection("MCPhysicsParticles")
        n_pionm = 0   # to be able to use only the first two pions: one pi+ one pi-
        n_pionp = 0   # to be able to use only the first two pions: one pi+ one pi-
        for np,particle in enumerate(mctruth_collection):
            if not self.is_selected_status_mctruth(particle):
                continue
            epv, ch_4mom = 0, 0
            
            if particle.getPDG() == 1000024:
                #filling the variables for the chargino+
                self.true_chp_pdgid[0]      = particle.getPDG()       

                epv = particle.getEndpointVec()  # the vector to the endpoint (decay vertex) of the simulated particle
                self.true_chp_trajlen[0]       = epv.Mag()
                self.true_chp_endpointR[0]     = epv.Perp()
                
                ch_4mom = particle.getLorentzVec()  # 4-momentum of the chargino+
                self.true_chp_pt[0]        = ch_4mom.Pt()
                self.true_chp_p[0]          = ch_4mom.P()
                self.true_chp_theta[0]      = ch_4mom.Theta()
                self.true_chp_phi[0]        = ch_4mom.Phi()
                self.true_chp_vtxR[0]       = sqrt(pow(particle.getVertex()[0], 2) + pow(particle.getVertex()[1], 2)) #production vertex of the particle in [mm]
                self.true_chp_e[0]          = ch_4mom.E()
                self.true_chp_m[0]          = ch_4mom.M()
                print("True chargino+: pt = %f, theta %f, phi %f"%(self.true_chp_pt[0], self.true_chp_theta[0], self.true_chp_phi[0] ))
                #print("                endpointR = %f"%( self.true_chp_endpointR[0] ))
            elif particle.getPDG() == -1000024:
                #filling the variables for the chargino-
                self.true_chm_pdgid[0]      = particle.getPDG()       


                epv = particle.getEndpointVec()   # the vector to the endpoint (decay vertex) of the simulated particle
                self.true_chm_trajlen[0]       = epv.Mag()
                self.true_chm_endpointR[0]     = epv.Perp()
                
                ch_4mom = particle.getLorentzVec()  # 4-momentum of the chargino-
                self.true_chm_pt[0]         = ch_4mom.Pt()
                self.true_chm_p [0]         = ch_4mom.P()
                self.true_chm_theta[0]      = ch_4mom.Theta()
                self.true_chm_phi[0]        = ch_4mom.Phi()
                self.true_chm_vtxR[0]       = sqrt(pow(particle.getVertex()[0], 2) + pow(particle.getVertex()[1], 2))
                self.true_chm_e[0]          = ch_4mom.E()
                self.true_chm_m[0]          = ch_4mom.M()
                print("True chargino-: pt = %f, theta %f, phi %f"%(self.true_chm_pt[0], self.true_chm_theta[0], self.true_chm_phi[0] ))
                #print("                endpointR = %f"%( self.true_chm_endpointR[0] ))
            elif particle.getPDG() == -211 and n_pionm < 1: #pi-, only using the first one in the event record
                n_pionm += 1
                pi_4mom = particle.getLorentzVec()
                self.truepionm_pt[0]      = pi_4mom.Pt()
                self.truepionm_p[0]       = pi_4mom.P()
                self.truepionm_theta[0]   = pi_4mom.Theta()
                self.truepionm_phi[0]     = pi_4mom.Phi()
                self.truepionm_vtxR[0]    = sqrt(pow(particle.getVertex()[0], 2) + pow(particle.getVertex()[1], 2))
                self.truepionm_e[0]       = pi_4mom.E()
                self.truepionm_m[0]       = pi_4mom.M()    
                self.truepionm_pdgid[0]   = particle.getPDG()
                print("True pion-: pt = %f, theta %f, phi %f"%(self.truepionm_pt[0], self.truepionm_theta[0], self.truepionm_phi[0]))
                #print("            vertexR = %f"%( self.truepionm_vtxR[0] ))
                
            elif particle.getPDG() == 211 and n_pionp < 1: #pi+, only using the first one in the event record
                n_pionp += 1
                pi_4mom = particle.getLorentzVec()
                self.truepionp_pt[0]      = pi_4mom.Pt()   
                self.truepionp_p[0]       = pi_4mom.P()    
                self.truepionp_theta[0]   = pi_4mom.Theta()
                self.truepionp_phi[0]     = pi_4mom.Phi()
                self.truepionp_vtxR[0]    = sqrt(pow(particle.getVertex()[0], 2) + pow(particle.getVertex()[1], 2))
                self.truepionp_e[0]       = pi_4mom.E()    
                self.truepionp_m[0]       = pi_4mom.M()    
                self.truepionp_pdgid[0]   = particle.getPDG()
                print("True pion+: pt = %f, theta %f, phi %f"%(self.truepionp_pt[0], self.truepionp_theta[0], self.truepionp_phi[0]))
                #print("            vertexR = %f"%( self.truepionp_vtxR[0] ))


            
        return self

    def sort_chargino_list(self, list_of_charginocandidates):

        l = []
        for n_c,c in enumerate(list_of_charginocandidates):
            l.append((n_c,c.pt))
        l = sorted(l,key=itemgetter(1), reverse=True)   # start with the highest pT
        l = zip(*l)
        indices = l[0]
        return [ list_of_charginocandidates[i] for i in indices ]

    def sort_pion_list(self, list_of_pioncandidates):

        l = []
        for n_c,c in enumerate(list_of_pioncandidates):
            l.append((n_c,c.dist_ch))
        l = sorted(l,key=itemgetter(1), reverse=False)   # start with the highest pT
        l = zip(*l)
        indices = l[0]
        return [ list_of_pioncandidates[i] for i in indices ]

    
    def get_hits_info(self, hits, ch_properties, trackrelations ):

        pdgids     = []
        dEdx_point = 0
        lasthitR   = 0
        firsthitR   = 9999
        lasthit_detector = ""
        points     = 0
        for hit in hits:
            hitR = sqrt(pow(hit.getPosition()[0], 2) + pow(hit.getPosition()[1], 2)) # in mm
            lasthitR = max(hitR, lasthitR)
            firsthitR = min(hitR, firsthitR)
            # get the sim hit (simhit is a SimTrackerHit object which is related to the hit from the tCollection.getTrackerHits() hit)
            for subdetector,relation in trackrelations.items():
                simHit = relation.getRelatedToObjects(hit)
                if len(simHit) == 0: continue
                simhit = simHit.at(0)   # use the first hit: it is the one with highest probability to be connected
                                        # to the MC particle (in cases of one hit produced by 2 or more MCParticles)
                #MCParticles coming from Overlay are currently not included, so their PDG is set to 0
                try: 
                    mc_pdgid = simhit.getMCParticle().getPDG()
                except:
                    mc_pdgid = 0
                pdgids.append(mc_pdgid)
                dEdx_point += simhit.getEDep()/simhit.getPathLength()
                points += 1


  
        ch_properties.firsthitR = firsthitR
        ch_properties.lasthitR = lasthitR
        ch_properties.dEdx_avrg = dEdx_point / points
        ch_properties.true_pdgid = self.most_frequent(pdgids)
        
        return ch_properties
    
    def most_frequent(self, List): 
        occurence_count = Counter(List) 
        return occurence_count.most_common(1)[0][0] 

    def get_track_pt(self, track):
        _track_curvature = abs(track.getOmega())
        if _track_curvature !=0:
            return 0.3*self._magnField/(_track_curvature*1000) # omega in 1/mm -> transformed in 1/m
        return 0

    def get_delta_r_tracks(self, track1, track2):
        _track1_theta = (pi/2-atan(track1.getTanLambda())) # in rad
        _track2_theta = (pi/2-atan(track2.getTanLambda())) # in rad
        return self.get_delta_r_tracks_expl(_track1_theta, track1.getPhi(), _track2_theta, track2.getPhi())

    def get_delta_r_tracks_expl(self, track1_theta, track1_phi, track2_theta, track2_phi):
        t1 = TVector3()
        t1.SetPtThetaPhi(1, track1_theta, track1_phi)
        t2 = TVector3()
        t2.SetPtThetaPhi(1, track2_theta, track2_phi)
        # the pts are just dummies as DeltaR does not use them
        return t1.DeltaR(t2)
    
    def get_track_pT_cone_fraction(self, event, central_track, cone_radius=0.1):
        
        pTsum = 0
        track_collection = event.getCollection("SiTracks_Refitted")
        #loop through all tracks of the event        
        for it in track_collection:
            if it.getTrackerHits().size() == 0:
                continue
            #if they are identical to the central track whose isolation is determined, ignore
            if it.id() == central_track.id():
                continue
            #if they are further away than cone_radius, ignore
            if self.get_delta_r_tracks(it, central_track) > cone_radius:
                continue
            pTsum += self.get_track_pt(it)
        # sum up the tracks pT
        central_track_pt = self.get_track_pt(central_track)
        if central_track_pt == 0:
            return 0
        return pTsum/central_track_pt

    def get_distance_cone(self, theta1, phi1, theta2, phi2):
        dist = 999.
        # check that are not the same track
        if abs( theta1 - theta2 ) < 0.001 and abs( phi1 - phi2 ) < 0.001 :
            return dist

        dist = self.get_delta_r_tracks_expl( theta1, phi1, theta2, phi2)
        return dist
    
    def chargino_candidate_properties(self, event, particle_type, apply_preselection):

        track_collection = event.getCollection("SiTracks_Refitted")   # Tracks collection
        pfo_collection   = event.getCollection("PandoraPFOs")         # TightSelectedPandoraPFOs is not enough for this study:
                                                                      # background particles and emitted soft pions have very low pT.
        
                                                              
        # create a dictionary containing the pyLCIO.UTIL::LCRelationNavigator objects which help to connect the tracks to the simhits
        # (and those to the MC particles)
        trackrelations = self.createTrackerHitRelationDict(event)

        # pyLCIO LCRelationNavigator is used to link tracks to pfo in PandoraPFOs collection
        pforelation = pyLCIO.UTIL.LCRelationNavigator(event.getCollection("SiTracks_Refitted"))
     

        list_of_candidates = []
        list_of_charginos  = []
        list_of_pions_ch1  = []
        list_of_pions_ch2  = []
        cand_dummy = Candidate()
 
         
        # loop over all tracks in the event ("SiTracks_Refitted") and identify the chargino candidates (TODO and pion candidates)
        if particle_type is "chargino": print('Total n tracks in event = %i'%len(track_collection))
        for nt,t in enumerate(track_collection):
            # check on track hits number, but it never happens that there is an empty track
            if t.getTrackerHits().size()==0:
                print('Track without hits??')
                continue 
            
            _track_pt = 0
            _track_p  = 0
            # variables needed for the preselection but also for being filled
            _track_z0 = t.getZ0()
            _track_d0 = t.getD0()
            _track_vertexR = sqrt(_track_z0**2 + _track_d0**2)
            _track_theta = pi / 2 - atan(t.getTanLambda())
            _track_theta_deg = _track_theta * 180. / pi
            _track_phi = t.getPhi()
            _track_phi_deg = t.getPhi() * 180. / pi
            _track_curvature = abs(t.getOmega())
            if _track_curvature !=0:
                _track_pt = 0.3*4/(_track_curvature*1000)
            if sin(_track_theta)!=0: _track_p = _track_pt/sin(_track_theta)
            #print(" .. Reco track: pt = %f, theta %f, phi %f"%(_track_pt, _track_theta, _track_phi))

            # get the pT fraction from the tracks in a cone of radius 0.1 surrounding the track
            _track_pTfraction_cone01 = self.get_track_pT_cone_fraction(event, t, cone_radius=0.1)                        


            # apply track-based preselection cuts for chargino candidates before filling the variables
            # veto non-prompt, low pt tracks (they are not suitable chargino candidates)
            if apply_preselection and _track_vertexR > 0.5: continue
            if apply_preselection and _track_pt < 1: continue
            if apply_preselection and _track_pTfraction_cone01 > 0.1: continue  # keep only the tracks whose surrounding R=0.1 cone has
                                                                                  # less than 10% of their pT, i.e. they are isolated from other tracks

            npfo = 0
            # get the associated pfos for the track
            for pfo in pfo_collection:
                pfotracks = pfo.getTracks()
                if len(pfotracks) == 0: continue     # skip empty track information pfo # we care only about pfos associated to tracks
                pfoobj = pfotracks.at(0) 
                if pfoobj.id() == t.id() and _track_pt > 1.5:          # track-pfo track association (for track_pt < 1.5,
                                                                       # there will be a pfo for a track only (no CAL deposit)
                    npfo +=1       # per track
                    break          # one is enough

            # apply pfo-of-track-based preselection cuts for chargino candidates before filling the variables
            if apply_preselection and npfo > 0: continue   # veto tracks that have an associated pfo if their pt is > 1.5 GeV

            # start filling the properties of this particular chargino candidate
            cand_properties = Candidate()

            # get the info about the track for which a loop over the hits is needed
            # get the tracker hits
            hits = t.getTrackerHits()
            cand_properties = self.get_hits_info(hits, cand_properties , trackrelations)
            if abs(cand_properties.true_pdgid) == 1000024:
                print('This track is associated to a chargino%s'%("+" if cand_properties.true_pdgid == 1000024 else "-"))

            # in the case of pion, search for tracks in cone around charginos 
            if particle_type is "pion" : 
                if self.n_ch_cand[0] >= 1: 
                    #print(" .. Reco track: pt = %f, theta %f, phi %f"%(_track_pt, _track_theta, _track_phi))
                    #print("                firsthitR = %f"%(cand_properties.firsthitR))
                    #print("                vertexR = %f"%(_track_vertexR))
            
                    #print("In this event were present %d chargino(s).. searching for associated pions"%(self.n_ch_cand[0]))
                    #print("Reco chargino1: pt = %f, theta %f, phi %f"%( self.ch1_pt[0], self.ch1_theta[0], self.ch1_phi[0]))
                    #print("                lasthitR %f"%(self.ch1_lasthitR[0]))
                    #print("Reco chargino2: pt = %f, theta %f, phi %f"%( self.ch2_pt[0], self.ch2_theta[0], self.ch2_phi[0]))
                    #print("                lasthitR %f"%(self.ch2_lasthitR[0]))

                    # not consider the chargino candidates with PDG null
                    _distance_cone_to_ch1 = 999.
                    _distance_cone_to_ch2 = 999.
                    _distance_cone_to_ch1 = self.get_distance_cone( _track_theta, _track_phi, self.ch1_theta[0], self.ch1_phi[0])
                    if self.n_ch_cand[0] >= 2: 
                        _distance_cone_to_ch2 = self.get_distance_cone( _track_theta, _track_phi, self.ch2_theta[0], self.ch2_phi[0])
                    # Just for debug purposes
                    if abs(cand_properties.true_pdgid) == 211: 
                        print('This pion is associated to pion %s'%("+" if cand_properties.true_pdgid == 211 else "-"))
                        if _distance_cone_to_ch1 < 999. : print('Distance to ch1 = %f'%_distance_cone_to_ch1)
                        if _distance_cone_to_ch2 < 999. : print('Distance to ch2 = %f'%_distance_cone_to_ch2)

                    #decay: pion associated to only the closest chargino
                    if _distance_cone_to_ch1 <= _distance_cone_to_ch2 :
                        cand_properties.dist_ch = _distance_cone_to_ch1
                        cand_properties.id_ass_ch = 1
                    else:
                        cand_properties.dist_ch = _distance_cone_to_ch2
                        cand_properties.id_ass_ch  = 2
                    #decay: pion kept only if in a certain size cone
                    if cand_properties.dist_ch > 1.0 :
                        continue

                    #decay: r first hit pion > r last hit ass chargino
                    if cand_properties.id_ass_ch is 1:
                        if self.ch1_lasthitR[0] > cand_properties.firsthitR :
                            continue
                    if cand_properties.id_ass_ch is 2:
                        if self.ch2_lasthitR[0] > cand_properties.firsthitR :
                            continue

                else:
                    continue

            cand_properties.n_assoc_pfo = npfo
            # get the info from the track that is just track based!
            cand_properties.pt = _track_pt
            cand_properties.p  = _track_p
            cand_properties.theta = _track_theta
            cand_properties.phi = _track_phi
            cand_properties.z0 = _track_z0
            cand_properties.d0 = _track_d0
            cand_properties.vertexR = _track_vertexR
            cand_properties.track_pTfraction_cone01 = _track_pTfraction_cone01

            list_of_candidates.append(cand_properties)

        if particle_type is "chargino":
            self.n_ch_cand[0] = len(list_of_candidates)
            print('Total n %s candidates = %i'%(particle_type,len(list_of_candidates)))
        elif particle_type is "pion":
            self.n_pi_cand[0] = len(list_of_candidates)
            print('Total n %s candidates = %i'%(particle_type,len(list_of_candidates)))
            for cand in list_of_candidates:
                if cand.id_ass_ch is 1 : list_of_pions_ch1.append(cand)
                if cand.id_ass_ch is 2 : list_of_pions_ch2.append(cand)
            self.ch1_n_conepions[0] = len(list_of_pions_ch1)
            self.ch2_n_conepions[0] = len(list_of_pions_ch2)
            print('Total n %s candidates associated to Ch1 = %i'%(particle_type,len(list_of_pions_ch1)))
            print('Total n %s candidates associated to Ch2 = %i'%(particle_type,len(list_of_pions_ch2)))
 
        while len(list_of_candidates) < 2:
            list_of_candidates.append(cand_dummy)

        # ERICA: THIS NEVER HAPPENS, BECAUSE OF THE DUMMIES!
        # stop filling anything if there is no candidate anyway
        if len(list_of_candidates) == 0:
            return self
        
        sorted_list_of_pions_ch1  = []
        sorted_list_of_pions_ch2  = []
        # sort the charginos by pT and use the first 2 ones
        if particle_type is "chargino":
            list_of_charginos = self.sort_chargino_list(list_of_candidates)
        # sort the pions using their distance to the chargino
        elif particle_type is "pion":
            if len(list_of_pions_ch1) >= 1:
                sorted_list_of_pions_ch1 = self.sort_pion_list(list_of_pions_ch1)
            if len(list_of_pions_ch2) >= 1:
                sorted_list_of_pions_ch2 = self.sort_pion_list(list_of_pions_ch2)
            if len(list_of_pions_ch1) == 0:
                sorted_list_of_pions_ch1.append(cand_dummy)
            if len(list_of_pions_ch2) == 0:
                sorted_list_of_pions_ch2.append(cand_dummy)
        
        # then, fill the variables   
        if particle_type is "chargino":
            if len(list_of_charginos) >= 1: 
                self.ch1_true_pdgid[0] = list_of_charginos[0].true_pdgid
                self.ch1_pt[0]         = list_of_charginos[0].pt
                self.ch1_p[0]          = list_of_charginos[0].p
                self.ch1_theta[0]      = list_of_charginos[0].theta
                self.ch1_phi[0]        = list_of_charginos[0].phi
                self.ch1_firsthitR[0]  = list_of_charginos[0].firsthitR
                self.ch1_lasthitR[0]   = list_of_charginos[0].lasthitR
                self.ch1_vertexR[0]    = list_of_charginos[0].vertexR
                self.ch1_dEdx_avrg[0]  = list_of_charginos[0].dEdx_avrg
                self.ch1_n_assocpfo[0] = list_of_charginos[0].n_assoc_pfo
                self.ch1_d0[0]    = list_of_charginos[0].d0
                self.ch1_z0[0]    = list_of_charginos[0].z0
                self.ch1_pTfraction_cone01[0] = list_of_charginos[0].track_pTfraction_cone01
                print("** Best ch1: PDG id = %i"%(self.ch1_true_pdgid[0]))
                print("   Best ch1: pt = %f, theta %f, phi %f"%(self.ch1_pt[0], self.ch1_theta[0], self.ch1_phi[0]))
            if len(list_of_charginos) >=2:
                self.ch2_true_pdgid[0] = list_of_charginos[1].true_pdgid
                self.ch2_pt[0]         = list_of_charginos[1].pt
                self.ch2_p[0]          = list_of_charginos[1].p
                self.ch2_theta[0]      = list_of_charginos[1].theta
                self.ch2_phi[0]        = list_of_charginos[1].phi
                self.ch2_firsthitR[0]  = list_of_charginos[1].firsthitR
                self.ch2_lasthitR [0]  = list_of_charginos[1].lasthitR
                self.ch2_vertexR[0]    = list_of_charginos[1].vertexR
                self.ch2_dEdx_avrg[0]  = list_of_charginos[1].dEdx_avrg
                self.ch2_n_assocpfo[0] = list_of_charginos[1].n_assoc_pfo
                self.ch2_d0[0]    = list_of_charginos[1].d0
                self.ch2_z0[0]    = list_of_charginos[1].z0
                self.ch2_pTfraction_cone01[0] = list_of_charginos[1].track_pTfraction_cone01
                print("** Best ch2: PDG id = %i"%(self.ch2_true_pdgid[0]))
                print("   Best ch2: pt = %f, theta %f, phi %f"%(self.ch2_pt[0], self.ch2_theta[0], self.ch2_phi[0]))

        if particle_type is "pion":
            if len(sorted_list_of_pions_ch1) >= 1:
                self.pi1_true_pdgid[0] = sorted_list_of_pions_ch1[0].true_pdgid
                self.pi1_pt[0]         = sorted_list_of_pions_ch1[0].pt
                self.pi1_p[0]          = sorted_list_of_pions_ch1[0].p
                self.pi1_theta[0]      = sorted_list_of_pions_ch1[0].theta
                self.pi1_phi[0]        = sorted_list_of_pions_ch1[0].phi
                self.pi1_firsthitR[0]  = sorted_list_of_pions_ch1[0].firsthitR
                self.pi1_lasthitR [0]  = sorted_list_of_pions_ch1[0].lasthitR
                self.pi1_vertexR[0]    = sorted_list_of_pions_ch1[0].vertexR
                self.pi1_n_assocpfo[0] = sorted_list_of_pions_ch1[0].n_assoc_pfo
                self.pi1_d0[0]         = sorted_list_of_pions_ch1[0].d0
                self.pi1_z0[0]         = sorted_list_of_pions_ch1[0].z0
                self.pi1_pTfraction_cone01[0] = sorted_list_of_pions_ch1[0].track_pTfraction_cone01
                print("** Best pi1: PDG id = %i, distance = %f"%(self.pi1_true_pdgid[0], sorted_list_of_pions_ch1[0].dist_ch))
                print("   Best pi1: pt = %f, theta %f, phi %f"%(self.pi1_pt[0], self.pi1_theta[0], self.pi1_phi[0]))
            if len(sorted_list_of_pions_ch2) >= 1:
                self.pi2_true_pdgid[0] = sorted_list_of_pions_ch2[0].true_pdgid
                self.pi2_pt[0]         = sorted_list_of_pions_ch2[0].pt
                self.pi2_p[0]          = sorted_list_of_pions_ch2[0].p
                self.pi2_theta[0]      = sorted_list_of_pions_ch2[0].theta
                self.pi2_phi[0]        = sorted_list_of_pions_ch2[0].phi
                self.pi2_firsthitR[0]  = sorted_list_of_pions_ch2[0].firsthitR
                self.pi2_lasthitR [0]  = sorted_list_of_pions_ch2[0].lasthitR
                self.pi2_vertexR[0]    = sorted_list_of_pions_ch2[0].vertexR
                self.pi2_n_assocpfo[0] = sorted_list_of_pions_ch2[0].n_assoc_pfo
                self.pi2_d0[0]         = sorted_list_of_pions_ch2[0].d0
                self.pi2_z0[0]         = sorted_list_of_pions_ch2[0].z0
                self.pi2_pTfraction_cone01[0] = sorted_list_of_pions_ch2[0].track_pTfraction_cone01
                print("** Best pi2: PDG id = %i, distance = %f"%(self.pi2_true_pdgid[0], sorted_list_of_pions_ch2[0].dist_ch))
                print("   Best pi2: pt = %f, theta %f, phi %f"%(self.pi2_pt[0], self.pi2_theta[0], self.pi2_phi[0]))

        #print( self.ch2_p[0])
        
        return self
    
    def fill_tree(self,event, apply_preselection):

        self.true_chargino_pion_properties(event)
        self.chargino_candidate_properties(event, "chargino", apply_preselection)
        self.chargino_candidate_properties(event, "pion", False)

        

        return self
