from array import array
import pyLCIO
from pyLCIO import UTIL
from pyLCIO.UTIL import *
from operator import itemgetter
from math import sqrt, pi, atan, sin
from collections import Counter


class TrackTreeFiller:
    """
    class for initializing and filling the "Track" TTree
    """
    # attributes initialization --> only these 
    # parameters will be saved in .root output file
    tmaxsize = 1000

    # tracks reconstructed info
    tracksperevent = array('i',[0])          # number of tracks per event
    hits = array('i',tmaxsize*[0])           # number of hits per track
    status = array('i',tmaxsize*[0])         # getGeneratorStatus value per track
    D0 = array('f',tmaxsize*[0.])            # D0 reconstructed parameter 
    Z0 = array('f',tmaxsize*[0.])            # Z0 reconstructed parameter  
    phi = array('f',tmaxsize*[0.])           # phi reconstructed parameter 
    theta = array('f',tmaxsize*[0.])         # theta reconstructed parameter 
    curvature = array('f',tmaxsize*[0.])     # curvature reconstructed parameter (from Track -> getOmega())
    vertexr = array('f',tmaxsize*[0.])       # vertexr = sqrt(recoD0^2 + recoZ0^2)
    pt = array('f',tmaxsize*[0.])            # pt parameter calculated from the curvature
    p = array('f',tmaxsize*[0.])             # p parameter calculated from pt and theta
    length = array('f',tmaxsize*[0.])        # length parameter calculated from hits position
    endpoint = array('f',tmaxsize*[0.])      # endpoint = last hit position
    detector_layer = array('f',tmaxsize*[0.])# detector_layer = (last hit detector number) + (last hit layer number)*0.1 -> if some length analysis is needed
    # tracks associated MC info
    pdgid = array('f',tmaxsize*[0.])         # PDG id of the MC particle associated to the track: only if more than 50% of the track hits belong to that MC particle
    dEdx_average = array('f',tmaxsize*[0.])  # <dEdx> (average on the whole set of hits). MC information!
    dEdx_max = array('f',tmaxsize*[0.])      # max(dEdx) (on the whole set of hits). MC information!
    t_theta = array('f',tmaxsize*[0.])       # track true theta parameter
    t_phi = array('f',tmaxsize*[0.])         # track true phi parameter
    t_pt = array('f',tmaxsize*[0.])          # track true pt parameter
    t_purity = array('f',tmaxsize*[0.])      # track purity
    # chargino candidate search
    charginosperevent = array('i',[0])       # number of chargino candidate per event (selected on the cuts reported in Track function)
    c_theta = array('f',tmaxsize*[0.])       # chargino track candidate theta
    c_phi = array('f',tmaxsize*[0.])         # chargino track candidate phi
    c_dEdx_average = array('f',tmaxsize*[0.])# chargino track candidate <dEdx>
    c_pt = array('f',tmaxsize*[0.])          # chargino track candidate pt
    c_pdg = array('f',tmaxsize*[0.])         # chargino track candidate PDG id
    # tracks-pfos association
    npfotot = array('i',[0])                 # number of pfos per event
    npfo = array('i',tmaxsize*[0])           # number of pfos associated per track
    pfotheta = array('f',tmaxsize*[0.])      # pfo theta 
    pfophi = array('f',tmaxsize*[0.])        # phi theta


    def _init_(self):
        self.tracksperevent = 50

    # initialization of the TTree: creating branches associated to the class attributes

    def radius2(self,x,hits):
        return hits.at(x).getPositionVec().X()**2+hits.at(x).getPositionVec().Y()**2+hits.at(x).getPositionVec().Z()**2
    
    def hit_radius2(self,hit):
        return hit.getPositionVec().X()**2+hit.getPositionVec().Y()**2+hit.getPositionVec().Z()**2

    def calculate_length(self,hit1,hit2):
        return sqrt(abs(self.hit_radius2(hit2) - self.hit_radius2(hit1)))

    def sort_by_radius (self,v_hits):
        """
        returns the indeces of the hits in the v_hits list from the hit with smallest radius to largest
        """
        x = []
        for l in range(len(v_hits)):   
            x.append((l,self.radius2(l,v_hits)))
        #original Cecilia:
        #sorted(x,key=itemgetter(1), reverse=True) #this does not change x, and it sorts reversed - why?
        x = sorted(x,key=itemgetter(1), reverse=False)
        # zip allows to just return the indices of the hits
        x = zip(*x)
        return x[0]
        


    def mode(self,l):
        # MODE finds the mode value in a list
        counter = Counter(l)
        max_count = max(counter.values())
        return([item for item,count in counter.items() if count == max_count])



    def initialize_branches(self,TreeTrack):
        # tracks reconstructed info
        TreeTrack.Branch('TracksperEvent',self.tracksperevent,'TracksperEvent/I')
        TreeTrack.Branch('TrackHits',self.hits,'TrackHits[TracksperEvent]/I')
        TreeTrack.Branch('TrackStatus',self.status,'TrackStatus[TracksperEvent]/I')
        TreeTrack.Branch('TrackD0',self.D0,'TrackD0[TracksperEvent]/F')
        TreeTrack.Branch('TrackZ0',self.Z0,'TrackZ0[TracksperEvent]/F')
        TreeTrack.Branch('TrackPhi',self.phi,'TrackPhi[TracksperEvent]/F')
        TreeTrack.Branch('TrackTheta',self.theta,'TrackTheta[TracksperEvent]/F')
        TreeTrack.Branch('TrackCurvature',self.curvature,'TrackCurvature[TracksperEvent]/F')
        TreeTrack.Branch('TrackVertexR',self.vertexr,'TrackVertexR[TracksperEvent]/F')
        TreeTrack.Branch('TrackPt',self.pt,'TrackPt[TracksperEvent]/F')
        TreeTrack.Branch('TrackP',self.p,'TrackP[TracksperEvent]/F')
        TreeTrack.Branch('TrackL',self.length,'TrackL[TracksperEvent]/F')
        TreeTrack.Branch('TrackEndPoint',self.endpoint,'TrackEndPoint[TracksperEvent]/F')
        TreeTrack.Branch('TrackLastHitDetector.Layer',self.detector_layer,'TrackLastHitDetector.Layer[TracksperEvent]/F')
        # tracks associated MC info
        TreeTrack.Branch('TrackPDGId',self.pdgid,'TrackPDGId[TracksperEvent]/F')
        TreeTrack.Branch('TrackdEdxAverage',self.dEdx_average,'TrackdEdxAverage[TracksperEvent]/F')
        TreeTrack.Branch('TrackdEdxMax',self.dEdx_max,'TrackdEdxMax[TracksperEvent]/F')
        TreeTrack.Branch('TrackTrueTheta',self.t_theta,'TrackTrueTheta[TracksperEvent]/F')
        TreeTrack.Branch('TrackTruePhi',self.t_phi,'TrackTruePhi[TracksperEvent]/F')
        TreeTrack.Branch('TrackTruePt',self.t_pt,'TrackTruePt[TracksperEvent]/F')
        TreeTrack.Branch('TrackPurity',self.t_purity,'TrackPurity[TracksperEvent]/F')
        # chargino candidate search
        TreeTrack.Branch('CharginoCandidatesperEvent',self.charginosperevent,"CharginoCandidatesperEvent/I")
        TreeTrack.Branch('CharginoCandidateTheta',self.c_theta,'CharginoCandidateTheta[CharginoCandidatesperEvent]/F')
        TreeTrack.Branch('CharginoCandidatePhi',self.c_phi,'CharginoCandidatePhi[CharginoCandidatesperEvent]/F')
        TreeTrack.Branch('CharginoCandidatedEdxAverage',self.c_dEdx_average,'CharginoCandidatedEdxAverage[CharginoCandidatesperEvent]/F')
        TreeTrack.Branch('CharginoCandidatePt',self.c_pt,'CharginoCandidatePt[CharginoCandidatesperEvent]/F')
        TreeTrack.Branch('CharginoCandidatePDG',self.c_pdg,'CharginoCandidatePDG[CharginoCandidatesperEvent]/F')
        # tracks-pfo association
        TreeTrack.Branch('TrackNPFOTot',self.npfotot,'TrackNPFOTot/I')
        TreeTrack.Branch('TrackNPFO',self.npfo,'TrackNPFO[TracksperEvent]/I')
        TreeTrack.Branch('TrackPFOtheta',self.pfotheta,'TrackPFOTheta[TracksperEvent]/F')        
        TreeTrack.Branch('TrackPFOPhi',self.pfophi,'TrackPFOPhi[TracksperEvent]/F')

        return TreeTrack

    def createTrackerHitRelationDict(self,event):
        """
        creates a dictionary containing for this event the LCRelationNavigator objects that indicate the relation between a track and a Hit entry
        """
        # pyLCIO LCRelationNavigator is used to link tracks simhits to MC particle in MCPhysicsParticles collection (first it allows to get the SimTrackerHit related to the TrackerHit belonging to the track collection, and then based on the SimTrackerHit, one can get the MCParticle)
        relations = {} 
        # hits collections. The order has to be the same for the subdetectors in trackerHitCollections and trackerHitRelationCollections
        trackerHitCollections = ["VXDTrackerHits","VXDEndcapTrackerHits","ITrackerHits","OTrackerHits","ITrackerEndcapHits","OTrackerEndcapHits"]
        trackerHitRelationCollections = ["VXDTrackerHitRelations","VXDEndcapTrackerHitRelations","InnerTrackerBarrelHitsRelations","OuterTrackerBarrelHitsRelations","InnerTrackerEndcapHitsRelations","OuterTrackerEndcapHitsRelations"]
        
        for ntrackerHitCollection,trackerHitCollection in enumerate(trackerHitCollections):
            if  event.getCollection(trackerHitCollection).getNumberOfElements() == 0: continue  # if the collection is empty go to the next trackerHitCollection
            relation = pyLCIO.UTIL.LCRelationNavigator(event.getCollection(trackerHitRelationCollections[ntrackerHitCollection]))
            relations[ntrackerHitCollection] = relation  # append the relation to relations initialized outside the loop


        return relations

    def fill_tree(self,event,fill_candidates_info):
        """
        fills the Track Tree
        
        arguments:
        event: the event obtained from the reader in ntuple_creator
        fill_candidates_info: a boolean whether or not chargino candidates information should be filled
        """

        
        # loading useful collections 
        tCollection = event.getCollection("SiTracks_Refitted")     # Tracks collection
        pfoCollection = event.getCollection("PandoraPFOs")         # TightSelectedPandoraPFOs is not enough for this study: background particles and emitted soft pions have very low pT.



        # initializing event "length" (it will be at the end modified discarding useless information)
        self.tracksperevent[0] = int(len(tCollection))
        self.charginosperevent[0] = self.tracksperevent[0]
        self.npfotot[0] = int(len(pfoCollection))

        #create a dictionary containing the pyLCIO.UTIL::LCRelationNavigator objects which help to connect the tracks to the simhits
        #(and those to the MC particles)
        trackrelations = self.createTrackerHitRelationDict(event)

        # pyLCIO LCRelationNavigator is used to link tracks to pfo in PandoraPFOs collection
        pforelation = pyLCIO.UTIL.LCRelationNavigator(event.getCollection("SiTracks_Refitted"))
    
        # loop initialization (information per event)
        npfotot = 0      # number of pfo per event
        ntracks = 0      # number of tracks per event
        c_ntracks = 0    # number of chargino candidates per event
    
        # loop over all tracks in event "event" (SiTracks_Refitted collection)
        for nt,t in enumerate(tCollection):
            # check on track hits number
            if t.getTrackerHits().size()==0:
                continue 
            # loops initialization (information per track)
            npfo = 0            # number of track associated pfo
    
            # loop on the pfo in PandoraPFOs
            for pfo in pfoCollection:
                
                pfotracks = pfo.getTracks()
                if len(pfotracks)==0: continue     # skip empty track information pfo # we care only about pfos associated to tracks
                pfoobj = pfotracks.at(0) 
                if pfoobj.id() == t.id():          # track-pfo track association 
                    npfo +=1       # per track
                    npfotot += 1   # per event
                    if len(pfotracks) >1 : print('more than one', len(pfotracks))  # check if there is something wrong
                    # filling some pfo information
                    self.pfotheta[ntracks] = pi/2-atan(pfoobj.getTanLambda())
                    self.pfophi[ntracks] = pfoobj.getPhi()
                    break
    
            # get the tracker hits
            hits = t.getTrackerHits()
            # sorting hits per distance from axis origin
            if len(hits)>1:
                index = self.sort_by_radius(hits)
            else: index[0] = 0
            l = self.calculate_length(hits[index[0]], hits[index[-1]])   # length definition
            
            # loop initialization (information per track)
            mcpertrack = []    # list of MC particles whose hits are on the track
            pdgs = []          # list of pdg ids of the MC particles associated to the track hits
            status = []        # list of GeneratorStatus of the MC particles associated to the track hits
            truetheta = []     # list of true thetas of the MC particles associated to the track hits
            truept = []        # list of true pts of the MC particles associated to the track hits
            truephi = []       # list of true phis of the MC particles associated to the track hits
            dEdx_point = 0     # dEdx of the hit
            dEdx_max = 0       # max(dEdx) between the already-read data
            points = 0         # number of hits to average on
            purity = 0         # track purity
            mc_maxhits = 0     # counter of number of hits for MC particle with max hits 


            # loop over all the track hits
            for i in index:
                hit = hits[i]
                # get the sim hit (simhit is a SimTrackerHit object which is related to the hit from the tCollection.getTrackerHits() hit)
                for subdetector,relation in trackrelations.items():
                    simHit = relation.getRelatedToObjects(hit)
                    if len(simHit) == 0: continue
                    simhit = simHit.at(0)
                    # get MC Particle information if the particle exists (no overlay)
                    try: 
                        particle = simhit.getMCParticle()
                        mcpertrack.append(str(particle))
                        #if( len(Counter(mcpertrack).keys()) > 1):
                            #print("****More than 1 mc: "+str(mcpertrack))
                            #print("++++Highest frequency occurrence: "+str(max(Counter(mcpertrack).values())))
                        status.append(particle.getGeneratorStatus())
                        lorentzparticle = particle.getLorentzVec()
                        truetheta.append(lorentzparticle.Theta()*180/pi)
                        truept.append(lorentzparticle.Pt())
                        truephi.append(lorentzparticle.Phi())
                    # if not possible save standard values
                    except: 
                        mcpertrack.append(0)
                        status.append(0)
                        truetheta.append(200)
                        truept.append(-200)
                        truephi.append(400)
    
                    # try to get PDG id of the simhit
                    try: 
                        pdg = simhit.getMCParticle().getPDG()
                    # if not assign a standard value
                    except: 
                        pdg = 0
                    # collect information
                    pdgs.append(pdg)
                    dEdx_point += simhit.getEDep()/simhit.getPathLength()
                    dEdx_max = max(dEdx_max,dEdx_point) 
                    points +=1
                    # if it is the last hit, collect information on its position in the detector
                    if hit == hits[-1]:
                        detector = subdetector
    
            # collect information that will fill the TTree
            
            # association MC particle-track occurs only when the track hits
            # mostly belong to the same particle --> mode in the pdg list
            pdg = self.mode(pdgs)    # pdg is now a list
            if len(pdg) != 1:   # if pdg has more than one entry, it should be counted as overlay
                pdg = [0]
                pdgs = [0]
    
            dEdx_point /= points                # get the average of dEdx
            layer = t.getSubdetectorHitNumbers().at(index[-1])
    
            index_true = pdgs.index(pdg[0])     # true information position in the lists 
            
            purity = max(Counter(mcpertrack).values())/len(hits)
    
            # filling self attributes
            # tracks reconstructed info
            self.hits[ntracks] = len(hits)
            self.status[ntracks] = status[index_true]
            self.D0[ntracks] = t.getD0()
            self.Z0[ntracks] = t.getZ0()
            self.phi[ntracks] = t.getPhi()
            self.theta[ntracks] = (pi/2-atan(t.getTanLambda()))*180/pi
            self.curvature[ntracks] = abs(t.getOmega())
            self.vertexr[ntracks] = sqrt(self.D0[ntracks]**2+self.Z0[ntracks]**2)
            if self.curvature[ntracks] !=0: self.pt[ntracks] = 0.3*4/(self.curvature[ntracks]*1000)
            if sin(self.theta[ntracks])!=0: self.p[ntracks] = self.pt[ntracks]/sin(self.theta[ntracks])
            self.length[ntracks] = l
            self.endpoint[ntracks] = sqrt(self.hit_radius2(hits[index[-1]]))
            self.detector_layer[ntracks] = detector + 0.1*layer
            # tracs associated MC info
            self.pdgid[ntracks] = pdg[0]
            self.dEdx_average[ntracks] = dEdx_point
            self.dEdx_max[ntracks] = dEdx_max
            self.t_theta[ntracks] = truetheta[index_true]
            self.t_phi[ntracks] = truephi[index_true]
            self.t_pt[ntracks] = truept[index_true]
            self.t_purity[ntracks] = purity
            # filling some pfo-association
            self.npfo[ntracks] = npfo
            ntracks += 1
            
            # chargino candidate analysis
            if not fill_candidates_info: continue
            # cuts that can be applied 
            if self.vertexr[ntracks-1] > 0.5: continue 
            if self.npfo[ntracks-1] != 0 : continue
            if self.pt[ntracks-1]<1: continue
            #if self.dEdx_average[ntracks]<0.0005: continue
            # chargino candidate tracks filling
            self.c_theta[c_ntracks] = self.theta[ntracks-1] 
            self.c_phi[c_ntracks] = self.phi[ntracks-1]
            self.c_dEdx_average[c_ntracks] = self.dEdx_average[ntracks-1]
            self.c_pt[c_ntracks] = self.pt[ntracks-1]
            self.c_pdg[c_ntracks] = self.pdgid[ntracks-1]
            c_ntracks += 1


        # filling information per event
        self.charginosperevent[0] = c_ntracks
        self.tracksperevent[0] = ntracks
        self.npfotot[0] = npfotot
    
        return self
