from array import array
from math import sqrt, pi, atan, sin

class MCEventInfoTreeFiller:
    """
    this class deals with the MCEventInfo Tree
    this is a tree about the MC truth event information filled per event, not per particle
    """

    n_event = array('i', [0])

    def _init_(self):
        return
    def initialize_branches(self, MCEventInfoTree):
        """
        creates the branches of the TTree MCEventInfoTree
        """
        #MCEventInfoTree.Branch()
        MCEventInfoTree.Branch('n_event', self.n_event, 'n_event/I')
        return MCEventInfoTree


    def fill_tree(self,n,event):
        self.n_event[0] = n

