#!/usr/bin/env python
import sys
sys.path.append('..')
#import objgraph
import os
from pyLCIO import IOIMPL
from pyLCIO import UTIL
from pyLCIO.UTIL import LCTOOLS
from ROOT import gStyle, gROOT
from math import sqrt, pi
from ROOT import TFile, TTree, TCanvas, TH1D, TF1, TLorentzVector, TVector3,kRed,kGreen,kDashed,kBlue
from datetime import datetime
from array import array
import json
import glob
from lib import MCParticlesTF, MCEventInfoTF
from lib import TrackTF
import ntuple_creator
gStyle.SetOptStat(0)
gStyle.SetLineWidth(2)
gStyle.SetTitleFont(42)
gStyle.SetLabelFont(42)
gStyle.SetPadTickX(1)
gStyle.SetPadTickY(1)
gStyle.SetLabelSize(0.04)
gROOT.ForceStyle()


def configure_parameters(run_config_file):
    with open(run_config_file) as json_run_file:
	run_conf = json.load(json_run_file)
    dataset_config_file = run_conf["DatasetInformationFile"]
    try: 
        with open(dataset_config_file) as json_datasets_file:
	    all_datasets_conf = json.load(json_datasets_file)
    except IOError:
        print("could not find {}".format(dataset_config_file))
        dataset_config_file = dataset_config_file.replace("../cfg/","cfg/")
        print("trying instead with this one: {}".format(dataset_config_file))
        with open(dataset_config_file) as json_datasets_file:
	    all_datasets_conf = json.load(json_datasets_file)
        
    for i in run_conf["ntuplecreator_prodids"]:
	#for now, build it like this, but this needs to be modified once using more than one prodid!!
	prod_id_str = str(i)
	dataset_conf = all_datasets_conf[prod_id_str]
	input_conf = {"NtupleOutputName" : "{}_{}mm_{}_".format(
            run_conf["NtupleOutputPrefix"],
            dataset_conf["ctau_in_mm"],
            dataset_conf["polarization"]),
	              "dataset_conf" : dataset_conf,
	              "DirectoryPath" : dataset_conf["path_to_DSTs"],
		      "CharginoCandidates": 1,
		      "TrueInfo": 1
	}
        input_conf.update(run_conf) #run_conf takes precendence just in case one of these things should be overwritten
        
    return input_conf


def main(argv):
    if len(argv) == 0:
        print("Please always give the run_config file as argument, e.g. \n python run_locally.py ../cfg/run_info_local.json")
        sys.exit()

    # configuring parameters
    ### this should be used: input_conf = configure_parameters(argv[0])
    ### ntuple_creator.main(input_conf)


    
    ntuple_creator.main(argv)
    
if __name__ == "__main__":
    main(sys.argv[1:])
