import sys
import json
import glob
import os
# from lib import MCParticlesTF, MCEventInfoTF
# from lib import TrackTF
# import ntuple_creator
from datetime import datetime

from DIRAC.Core.Base import Script
Script.parseCommandLine()
from ILCDIRAC.Interfaces.API.DiracILC import  DiracILC
from ILCDIRAC.Interfaces.API.NewInterface.UserJob import *
from ILCDIRAC.Interfaces.API.NewInterface.Applications import *



def configure_parameters(run_config_file):
    with open(run_config_file) as json_run_file:
	run_conf = json.load(json_run_file)
    dataset_config_file = run_conf["DatasetInformationFile"]
    #print dataset_config_file
    with open(dataset_config_file) as json_datasets_file:
	all_datasets_conf = json.load(json_datasets_file)
	
    for i in run_conf["ntuplecreator_prodids"]:
	#for now, build it like this, but this needs to be modified once using more than one prodid!!
	prod_id_str = str(i)
	dataset_conf = all_datasets_conf[prod_id_str]
	input_conf = {"NtupleOutputName" : "{}_{}mm_{}_".format(
            run_conf["NtupleOutputPrefix"],
            dataset_conf["ctau_in_mm"],
            dataset_conf["polarization"]),
	              "dataset_conf" : dataset_conf,
	              "DirectoryPath" : dataset_conf["path_to_DSTs"],
		      "CharginoCandidates": 1,
		      "TrueInfo": 1
	}
        input_conf.update(run_conf) #run_conf takes precendence just in case one of these things should be overwritten
        
    return input_conf

def get_list_of_input_files_prodid(input_conf, prodid):
    # create the list of filenames for the given prodid based on input_conf information
   
    print(input_conf)
    return glob.iglob(str(input_conf["DirectoryPath"])+'/*/*.slcio')


def main(argv):
    """script to submit the ntuple creation to the grid according to the configuration
    """
    if len(argv) == 0:
        print("Please always give the run_config file as argument, e.g. \n python submit_grid.py ../cfg/run_info_grid.json")
        sys.exit()

    input_conf = configure_parameters(argv[0])
    for prodid in input_conf["ntuplecreator_prodids"]:
        print (prodid)
        run_on_one_prodid(prodid,argv, input_conf)

    return

def run_on_one_prodid(prodid, argv, input_conf):
    #this runs the ntuple_filling_event_loop function of ntuple_creator.py but only for Grid_Files_Per_Job files at the same time, and  submits those to the grid

    timestamp      = "{}-{}-{}".format(datetime.today().year,datetime.today().month,datetime.today().day)
    repositoryfile = "repository-{}.rep".format(timestamp)
    dirac          = DiracILC(True,repositoryfile)
    jobgroup       = "chargino_ntuples"
    #jobgroup       = "chargino_ntuples_test"

    outputPath = input_conf["GridOutputPath"]
    	
    run_meta_info = {
	'initialization_time': datetime.today().isoformat(),
	'configuration' : input_conf
    }
	

    lol = lambda lst, sz: [lst[i:i+sz] for i in range(0, len(lst), sz)]

    n_files_per_job = input_conf["Grid_Files_Per_Job"]
    list_of_all_filenames = list(get_list_of_input_files_prodid(input_conf, prodid))

    j=0
    for job_file_list in lol(list_of_all_filenames,  n_files_per_job ):
        #print job_file_list
        
        #removing "/eos/experiment/clicdp/grid" from the filenames
        job_file_list_input = []
        for fname in job_file_list:
            fname_input = fname.replace("/eos/experiment/clicdp/grid","")
            job_file_list_input.append(fname_input)
        if input_conf["TestMode"]:
          print('Files input:',job_file_list_input)
        
        j+=1
        if input_conf["TestMode"] and j> 3:
            break
        job = UserJob()
        job.setInputSandbox(input_conf["grid_tarball"])


        job.setInputData(job_file_list_input)

        # outputFileName = input_conf["NtupleOutputName"]+str(job_file_list_input[0]).split('/')[-1].replace(".slcio",".root")
        # print outputFileName
        outputFileNames = [input_conf["NtupleOutputPrefix"]+str(i).split('/')[-1].replace(".slcio",".root") for i in job_file_list_input]
        if input_conf["TestMode"]:
            print('Files output:',outputFileNames)

        script = RootScript()
        script.setVersion("ILCSoft-2020-02-07_gcc62")
        script.setScript ("ntuple_creator.py")

        #in tarring the tarball, the leading ../ is removed:
        run_info_file = argv[0].replace("../","")
        script.setArguments("{} {}".format(run_info_file, (",").join(job_file_list_input) )   )
        script.setOutputFile(outputFileNames[0])
        job.setOutputSandbox(["*.log","*.sh"])
        job.setOutputData(outputFileNames, outputPath, OutputSE="CERN-DST-EOS")
        res =  job.append(script)
        if not res['OK']:
            print res['Message']
            exit()
        job.setJobGroup(jobgroup)
        job.dontPromptMe()
        job.submit(dirac)

        print("Submitting job number {}".format(j))
    print ("Repository file: {}".format(repositoryfile))
    print("Job group {}".format(jobgroup))


if __name__ == "__main__":
    main(sys.argv[1:])
