from ROOT import *

path = "../"#/eos/experiment/clicdp/data/user/e/ericabro/stubs/"
# without preselections
fileName = "output_file_nopresel_ch1ch1_dst_13901.root" 
# with preselections
#fileName = "output_file_presel_ch1ch1_dst_13901.root" 

# without preselections
path2 = "/eos/experiment/clicdp/data/user/s/schnooru/stubs/histograms/without_preselection_LLP_2020-07-23/"
# with preselections
#path2 = "/eos/experiment/clicdp/data/user/s/schnooru/stubs/histograms/with_preselection_LLP_2020-07-22/"
fileName2 = "histos_sig.root" 

print("Input file names:")
print(path+fileName)
print(path2+fileName2)

# save histo in ttree
TREENAME = "Event"
pdgid = 1000024

gROOT.Macro("CLICdpStyle.C") 

FILE = TFile(path+fileName)
TREE = FILE.Get(TREENAME)
print(TREE)

TCut_reconstructable = ""
TCut_reconstructable_true = "abs(Ch1_truePDGID) == "+str(pdgid)+" || abs(Ch2_truePDGID) == "+str(pdgid)+""

HISTO     = TH1F("h_dEdx","h_dEdx",100,0,0.01)
HISTO_CH2 = TH1F("h_dEdx_ch2","h_dEdx_ch2",100,0,0.01)
HISTO_ONLYTRUE     = TH1F("h_dEdx_true","h_dEdx_true",100,0,0.01)
HISTO_ONLYTRUE_CH2 = TH1F("h_dEdx_true_ch2","h_dEdx_true_ch2",100,0,0.01)

TREE.Draw("Ch1_dEdx_Avrg>>h_dEdx",TCut_reconstructable)
TREE.Draw("Ch2_dEdx_Avrg>>h_dEdx_ch2",TCut_reconstructable)
TREE.Draw("Ch1_dEdx_Avrg>>h_dEdx_true",TCut_reconstructable_true)
TREE.Draw("Ch2_dEdx_Avrg>>h_dEdx_true_ch2",TCut_reconstructable_true)


# save histo in histograms
FILE2 = TFile(path2+fileName2)
HISTO_ANA     = FILE2.Get("ch1_dEdx_avrg")
HISTO_ANA_CH2 = FILE2.Get("ch2_dEdx_avrg")
print(HISTO_ANA)

# print on canvas
c = TCanvas("c")
c.SetRightMargin(0.14)
HISTO.Add(HISTO_CH2)
TGaxis.SetMaxDigits(3)
HISTO.SetTitle(";<dE/dx> [GeV/mm];Events")
HISTO.SetLineWidth(2)
HISTO.SetLineColor(kBlack)

HISTO_ONLYTRUE.Add(HISTO_ONLYTRUE_CH2)
HISTO_ONLYTRUE.SetLineWidth(2)
HISTO_ONLYTRUE.SetLineColor(kRed)
HISTO_ONLYTRUE.SetLineStyle(2)

HISTO_ANA.Add(HISTO_ANA_CH2)
HISTO_ANA.SetLineWidth(2)
HISTO_ANA.SetLineColor(kBlue)
HISTO_ANA.SetLineStyle(1)

HISTO_ANA.Draw("")
HISTO_ANA.GetXaxis().SetRangeUser(0.0, 0.005)
HISTO_ONLYTRUE.Draw("same")
HISTO.Draw("same")

gPad.Update()

leg01 = TLegend(0.30,0.63,0.80,0.90)
if(int(pdgid) == 1000024):
    leg01.SetHeader("charginos, c#tau_{#chi^{#pm}} = 600mm")

leg01.AddEntry(HISTO,"non-reweighted sample, all chargino candidates")
leg01.AddEntry(HISTO_ONLYTRUE,"non-reweighted sample, only ch candidates associated w/true")
leg01.AddEntry(HISTO_ANA,"reweighted sample, all chargino candidates")
leg01.Draw()

text = TLatex();
text.SetTextSize(0.035);
text.DrawTextNDC(0.530, 0.939349, "CLICdp work in progress");

c.Draw()
#if(int(pdgid) == 211):
#    c.SaveAs("plots/pion_dEdx.eps","eps")    
if(int(pdgid) == 1000024):
    c.SaveAs("chargino_dEdx.eps","eps")

INPUT = raw_input('Press enter to exit...')
