from ROOT import *

# sig with preselections
path_sig = "/eos/experiment/clicdp/data/user/s/schnooru/stubs/histograms/with_preselection_LLP_2020-07-22/"
fileName_sig = "histos_sig.root" 

# bkg with preselections
path_bkg = "/eos/experiment/clicdp/data/user/s/schnooru/stubs/histograms/with_preselection_LLP_2020-07-22/"
fileName_bkg = "histos_bg.root" 

print("Input file names:")
print(path_sig+fileName_sig)
print(path_bkg+fileName_bkg)

gROOT.Macro("CLICdpStyle.C") 

# save histo in histograms
FILE_SIG = TFile(path_sig+fileName_sig)
HISTO_ANA_SIG     = FILE_SIG.Get("ch1_dEdx_avrg")
HISTO_ANA_SIG_CH2 = FILE_SIG.Get("ch2_dEdx_avrg")
print(HISTO_ANA_SIG)

FILE_BKG = TFile(path_bkg+fileName_bkg)
HISTO_ANA_BKG     = FILE_BKG.Get("ch1_dEdx_avrg")
HISTO_ANA_BKG_CH2 = FILE_BKG.Get("ch2_dEdx_avrg")
print(HISTO_ANA_BKG)

# print on canvas
c = TCanvas("c")
c.SetRightMargin(0.14)
HISTO_ANA_SIG.Add(HISTO_ANA_SIG_CH2)
TGaxis.SetMaxDigits(4)
HISTO_ANA_SIG.SetLineWidth(2)
HISTO_ANA_SIG.SetLineColor(kBlue)

HISTO_ANA_BKG.Add(HISTO_ANA_BKG_CH2)
HISTO_ANA_BKG.SetLineWidth(2)
HISTO_ANA_BKG.SetLineColor(kBlue+2)
HISTO_ANA_BKG.SetLineStyle(2)


HISTO_ANA_BKG.Scale(1./HISTO_ANA_BKG.Integral())
HISTO_ANA_SIG.Scale(1./HISTO_ANA_SIG.Integral())


HISTO_ANA_BKG.Draw()
HISTO_ANA_BKG.SetTitle(";<dE/dx> [GeV/mm];Events")
HISTO_ANA_BKG.GetXaxis().SetRangeUser(0.0, 0.0015)
HISTO_ANA_SIG.Draw("same")

gPad.Update()

leg01 = TLegend(0.30,0.63,0.80,0.90)
leg01.SetHeader("charginos, c#tau_{#chi^{#pm}} = 600mm")

leg01.AddEntry(HISTO_ANA_SIG,"reweighted sample, sig")
leg01.AddEntry(HISTO_ANA_BKG,"bkg")
leg01.Draw()

text = TLatex();
text.SetTextSize(0.035);
text.DrawTextNDC(0.530, 0.939349, "CLICdp work in progress");

c.Draw()
c.SaveAs("chargino_dEdx_comp.eps","eps")

INPUT = raw_input('Press enter to exit...')
