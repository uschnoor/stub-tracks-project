from ROOT import gStyle, gROOT
from ROOT import THStack, TFile, TTree, TChain,  TCanvas, TH1D, TF1, TH2F, TLorentzVector, TVector3,kRed,kGreen,kBlack,kDashed,kBlue, TH1F, TLegend, TRatioPlot
import ROOT
gStyle.SetOptStat(1)
gStyle.SetLineWidth(2)
gStyle.SetTitleFont(42)
gStyle.SetLabelFont(42)
gStyle.SetPadTickX(1)
gStyle.SetPadTickY(1)
gStyle.SetLabelSize(0.04)
gROOT.ForceStyle()

import json
import sys

def configure_parameters(run_config_file):
    with open(run_config_file) as json_run_file:
	run_conf = json.load(json_run_file)
    dataset_config_file = run_conf["DatasetInformationFile"]
    with open(dataset_config_file) as json_datasets_file:
	all_datasets_conf = json.load(json_datasets_file)
	
    for i in run_conf["prodids"]:
	#for now, build it like this, but this needs to be modified, but this needs to be modified once using more than one prodid!!
	prod_id_str = str(i)
	dataset_conf = all_datasets_conf[prod_id_str]
	input_conf = {"NtupleOutputName" : "{}_{}mm_{}_".format(
            run_conf["NtupleOutputPrefix"],
            dataset_conf["ctau_in_mm"],
            dataset_conf["polarization"]),
	              "dataset_conf" : dataset_conf,
	              "DirectoryPath" : dataset_conf["path_to_DSTs"],
		      "CharginoCandidates": 1,
		      "TrueInfo": 1
	}
        input_conf.update(run_conf) #run_conf takes precendence just in case one of these things should be overwritten
    return input_conf

def main(argv):
    if len(argv) == 0:
        print("Please always give the run_config file as argument, e.g. \n python plot.py ../cfg/run_info_local.json")
        sys.exit()
    input_conf = configure_parameters(argv[0])
    print input_conf

    histofile = TFile.Open("{}/{}.root".format(
        input_conf["HistoFileDir"],
        input_conf["HistoFileName"]))

    print("Opening the histograms from this file: {}".format( histofile.GetName()))
    histothetacorr           =histofile.Get("histothetacorr")

    
    histoep                  =histofile.Get("histoep")
    hs                       =histofile.Get("hs")
    h1                       =histofile.Get("h1")
    h2                       =histofile.Get("h2")
    histopfo                 =histofile.Get("histopfo")
    histopfo2                =histofile.Get("histopfo2")
    histo                    =histofile.Get("histo")
    histo2                   =histofile.Get("histo2")
    histopt                  =histofile.Get("histopt")
    histopt2                 =histofile.Get("histopt2")
    histopt3                 =histofile.Get("histopt3")
    histopto                 =histofile.Get("histopto")

    histo_theta_ch_all       =histofile.Get("histo_theta_ch_all")
    histo_theta_ch_ge4       =histofile.Get("histo_theta_ch_ge4")
    histo_theta_ch_ge6       =histofile.Get("histo_theta_ch_ge6")
    
    histopromp               =histofile.Get("histopromp")
    histopromp_ch_all = histofile.Get("histopromp_ch_all")
    histopromp_ch_ge4 = histofile.Get("histopromp_ch_ge4")
    histopromp_ch_ge6 = histofile.Get("histopromp_ch_ge6")
    histodedxaverage_ch_all = histofile.Get("histodedxaverage_ch_all")
    histodedxaverage_ch_ge4 = histofile.Get("histodedxaverage_ch_ge4")
    histodedxaverage_ch_ge6 = histofile.Get("histodedxaverage_ch_ge6")

    
    histopromp2              =histofile.Get("histopromp2")
    histodedxaverage         =histofile.Get("histodedxaverage")
    histodedxaverage2        =histofile.Get("histodedxaverage2")
    histodelta               =histofile.Get("histodelta")
    histodelta2              =histofile.Get("histodelta2")
    histotheta               =histofile.Get("histotheta")
    histotheta2              =histofile.Get("histotheta2")
    histophi                 =histofile.Get("histophi")
    histophi2                =histofile.Get("histophi2")
    histothetascatt          =histofile.Get("histothetascatt")
    histohits                =histofile.Get("histohits")
    histoendpoint            =histofile.Get("histoendpoint")
    histodedxmax             =histofile.Get("histodedxmax")
    histodedxavtot           =histofile.Get("histodedxavtot")
    histo_eventType          =histofile.Get("histo_eventType")
    histo_eventType_chargino =histofile.Get("histo_eventType_chargino")
    histo_eventClassCh       =histofile.Get("histo_eventClassCh")
    histo_eventClassPi       =histofile.Get("histo_eventClassPi")
    histo_eventClassOther    =histofile.Get("histo_eventClassOther")
    histothetach_2chi        =histofile.Get("histothetach_2chi")
    histotruethetach_2chi    =histofile.Get("histotruethetach_2chi")
    histothetapi_2chi        =histofile.Get("histothetapi_2chi")
    histotruethetapi_2chi    =histofile.Get("histotruethetapi_2chi")
    histochpurity_2chi       =histofile.Get("histochpurity_2chi")
    histopipurity_2chi       =histofile.Get("histopipurity_2chi")
    


    
    #### canvases for the results of charginos and pions based on the event classification
    
    correl = TCanvas("correl")
    histothetacorr.Draw("colz")
    histothetacorr.SetTitle("")
    histothetacorr.GetXaxis().SetTitle("true #theta[#circ]")
    histothetacorr.GetYaxis().SetTitle("reco #theta[#circ]")
    
    event_class = TCanvas("event_class")
    event_class.SetLogz()
    event_class.Divide(2,2)
    event_class.cd(1)
    histo_eventClassCh.Draw("colz")
    histo_eventClassCh.SetTitle("")
    histo_eventClassCh.GetXaxis().SetTitle("nr tracks per event")
    histo_eventClassCh.GetYaxis().SetTitle("nr reco charginos per event")
    event_class.cd(2)
    histo_eventClassPi.Draw("colz")
    histo_eventClassPi.SetTitle("")
    histo_eventClassPi.GetXaxis().SetTitle("nr tracks per event")
    histo_eventClassPi.GetYaxis().SetTitle("nr reco pions per event")
    event_class.cd(3)
    histo_eventClassOther.Draw("colz")
    histo_eventClassOther.SetTitle("")
    histo_eventClassOther.GetXaxis().SetTitle("nr tracks per event")
    histo_eventClassOther.GetYaxis().SetTitle("nr reco others per event")
    
    theta_distrib = TCanvas("theta_distrib")
    histotheta2.Draw()
    histotheta2.GetXaxis().SetTitle("#theta [#circ]")
    histotheta2.SetLineColor(kBlue)
    histotheta.Draw("same")
    histotheta.SetLineColor(kRed)
    leg00 = TLegend(0.48,0.68,0.85,0.88)
    leg00.AddEntry(histotheta2,"true")
    leg00.AddEntry(histotheta,"reco")
    leg00.Draw()
    
    nrTracks = TCanvas("nrTracks")
    histo_eventType.Draw()
    histo_eventType.GetXaxis().SetTitle("nr tracks per event")
    histo_eventType.SetLineWidth(2)
    
    nrCharginos = TCanvas("nrCharginos")
    histo_eventType_chargino.Draw()
    histo_eventType_chargino.GetXaxis().SetTitle("nr reco charginos")
    histo_eventType_chargino.SetLineWidth(2)
    
    theta_pions = TCanvas("theta_pions")
    histotruethetapi_2chi.Draw()
    histotruethetapi_2chi.GetXaxis().SetTitle("pion #theta [#circ]")
    histotruethetapi_2chi.SetLineWidth(2)
    histotruethetapi_2chi.SetLineColor(kBlue)
    histothetapi_2chi.Draw("same")
    histothetapi_2chi.SetLineWidth(2)
    histothetapi_2chi.SetLineColor(kRed)
    histothetapi_2chi.SetMaximum(50)
    leg1 = TLegend(0.48,0.68,0.85,0.88)
    leg1.AddEntry(histotruethetapi_2chi,"true")
    leg1.AddEntry(histothetapi_2chi,"reco")
    leg1.Draw()
    
    theta_charginos = TCanvas("theta_charginos")
    histotruethetach_2chi.Draw()
    histotruethetach_2chi.GetXaxis().SetTitle("chargino #theta [#circ]")
    histotruethetach_2chi.SetLineWidth(2)
    histotruethetach_2chi.SetLineColor(kBlue)
    histothetach_2chi.Draw("same")
    histothetach_2chi.SetLineWidth(2)
    histothetach_2chi.SetLineColor(kRed)
    leg2 = TLegend(0.48,0.68,0.85,0.88)
    leg2.AddEntry(histotruethetach_2chi,"true")
    leg2.AddEntry(histothetach_2chi,"reco")
    leg2.Draw()
    
    purity_charginos = TCanvas("purity_charginos")
    histochpurity_2chi.Draw()
    histochpurity_2chi.GetXaxis().SetTitle("chargino purity")
    
    purity_pions = TCanvas("purity_pions")
    histopipurity_2chi.Draw()
    histopipurity_2chi.GetXaxis().SetTitle("pion purity")
    
    #### canvases for the results of the analysis cuts
    
    c = TCanvas("c")
    histophi2.Draw()
    histophi.Draw("same")
    histophi2.Sumw2()
    rp = TRatioPlot(histophi2, histophi)
    rp.Draw()
    rp.GetLowYaxis().SetNdivisions(505)
    c.Update()
    histo.Draw("colz")

    
    histopt.Draw()
    histopt.SetLineColor(kBlack)
    histopt2.Draw("same")
    histopt2.SetLineColor(kBlue)
    histopt3.Draw("same")
    histopt3.SetLineColor(kGreen)
    histopto.Draw("same")
    histopto.SetLineColor(kRed)
    c.Update()
    raw_input()
    
    histo_theta_ch_all.SetLineColor(kBlack)
    histo_theta_ch_all.Draw()
    histo_theta_ch_ge4.SetLineColor(kBlue)
    histo_theta_ch_ge4.Draw("same")
    histo_theta_ch_ge6.SetLineColor(kGreen)
    histo_theta_ch_ge6.Draw("same")
    c.Update()
    raw_input()
    
    hs.Draw("")
    #histopfo.Draw()
    #histopfo2.Draw("same")
    histo.SetFillColor(2)
    histo2.SetFillColor(4)
    histo.Draw("SURF")
    histo2.Draw("SURF same")
    
    cpt = TCanvas("cpt")
    cpt.cd()
    histopt2.Draw()
    histopt.Draw("same")
    ctheta = TCanvas("ctheta")
    ctheta.cd()
    histotheta2.Draw()
    histotheta.Draw("same")
    cdedxa = TCanvas("cdedxa")
    cdedxa.cd()
    histodedxaverage2.SetLineColor(kBlack)
    histodedxaverage.SetLineColor(kRed)
    histodedxaverage2.Draw("")
    histodedxaverage.Draw("same")
    cdelta = TCanvas("cdelta")
    cdelta.cd()
    histodelta2.Draw("")
    histodelta.Draw("same")
    cpromp = TCanvas("cpromp")
    histopromp2.SetLineColor(kRed) #other
    histopromp_ch_all.SetLineColor(kBlack) #all charginos
    histopromp_ch_ge4.SetLineColor(kBlue) #4hits
    histopromp_ch_ge6.SetLineColor(kGreen) #6hits

    histopromp_ch_all.Draw("")    
    histopromp2.Draw("same")
    histopromp_ch_ge4.Draw("same")
    histopromp_ch_ge6.Draw("same")
    
    
    # histopromp2.SetLineColor(kBlack) 
    # histopromp.Draw()
    # histopromp2.Draw("same")
    c.Update()
    histodedxaverage_ch_all.SetLineColor(kBlack) #all charginos
    histodedxaverage_ch_ge4.SetLineColor(kBlue) #4hits 
    histodedxaverage_ch_ge6.SetLineColor(kGreen) #6hits

    histodedxaverage_ch_all.Draw("")    
    histodedxaverage_ch_ge4.Draw("same")
    histodedxaverage_ch_ge6.Draw("same")
    
    
    raw_input()
    

    histofile.Close()
    

if __name__ == "__main__":
    main(sys.argv[1:])
