import sys
from create_ntuples.lib import TrackTF
from pyLCIO import IOIMPL
from pyLCIO import UTIL
from pyLCIO.UTIL import LCTOOLS

import unittest


class TestTrackTreeFiller(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        reader = IOIMPL.LCFactory.getInstance().createLCReader()
        print("Opening test file ch1ch1_dst_14712_111.slcio")
        reader.open("/eos/experiment/clicdp/grid/ilc//prod/clic/3tev/ch1ch1/CLIC_o3_v14/DST/00014712/000/ch1ch1_dst_14712_111.slcio")
        print ("This file contains {} events".format(reader.getNumberOfEvents()))
        cls.reader = reader

    @classmethod
    def tearDownClass(cls):
        cls.reader.close()
        

    def test_sort_by_radius(self):
        trackTF_test = TrackTF.TrackTreeFiller()
        event = self.reader.readEvent(0,0)

        #reader = self.reader
        #for n,event in enumerate(reader):
        #    print ('hi')
        tracker_hits = event.getCollection("SiTracks_Refitted")[0].getTrackerHits()

        if len(tracker_hits)>1:
            
            index = trackTF_test.sort_by_radius(tracker_hits)
            
        self.assertEqual(index , (0,1,2,3,4,5,6))
                                

    def test_hit_radius2(self):
        reader = self.reader
        trackTF_test = TrackTF.TrackTreeFiller()
        list_of_radii = [4182.419296178431, 2049.2965180037263, 10733.155405951775, 54064.99789086356, 2049.08224338199, 3749.2579547929568, 18464.0029832031, 3749.147141247444, 3689.118772170722, 3749.2437027621645, 3689.3691117511753, 5470.555843729578, 4181.9064959054485, 10732.995316898234, 3749.3325361505304, 4182.082948414165, 41872.95556621865, 45630.838614792694, 18369.68920021258, 3748.8961478126466, 10733.41467660649, 2049.289305934139, 3748.84430413278, 4182.373421532457]
        for n, event in enumerate(reader):
            tracker_hits = event.getCollection("SiTracks_Refitted")[0].getTrackerHits()
            firsthit = tracker_hits[0]
            #list_of_radii.append( trackTF_test.hit_radius2(firsthit))
            self.assertEqual(trackTF_test.hit_radius2(firsthit), list_of_radii[n])
                   

if __name__ == "__main__":
    unittest.main()
