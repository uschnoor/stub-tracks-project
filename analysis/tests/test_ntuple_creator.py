import sys
print sys.path
from create_ntuples import ntuple_creator

import unittest


class TestNtuple_Creator(unittest.TestCase):
    def test_get_list_of_input_files(self):
        pathlist = ntuple_creator.get_list_of_input_files({"DirectoryPath": "/eos/experiment/clicdp/grid/ilc/prod/clic/3tev/ch1ch1/CLIC_o3_v14/DST/00013899/"})

        _first_filename_should_be = "/eos/experiment/clicdp/grid/ilc/prod/clic/3tev/ch1ch1/CLIC_o3_v14/DST/00013899/000/ch1ch1_dst_13899_1.slcio"
        self.assertEqual(list(pathlist)[0],_first_filename_should_be)

        self.assertIsNotNone(ntuple_creator.get_list_of_input_files({"DirectoryPath": ""}))
        return

if __name__ == "__main__":
    unittest.main()
