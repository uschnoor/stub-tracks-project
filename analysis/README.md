The new analysis directory containing the full chain

# Structure

`create_ntuples`: from slcio to ntuples

`analyze_data`: running on ntuples to produce histograms and tables

`plot_histograms`: make nice plots

`cfg`: configuration files, ideally usable by all of the three steps of the full chain

# Setting up
to build directory environment (once):
```bash
cd analysis
./build.sh
```

# Prepare running:
```bash
source /cvmfs/clicdp.cern.ch/iLCSoft/builds/2020-02-07/x86_64-slc6-gcc62-opt/init_ilcsoft.sh
source setup.sh
```
the setup.sh sets the correct python path so that all dependencies are found.

# Configure for your setup:
* run configuration is by default `cfg/run_info_local.json`
* there, set which prodids you want to use -> the dataset information is then read
  from the corresponding configuration file (default `cfg/info_on_all_datasets.json`)
* NB. set the correct `NtupleOutputDir` etc. !
* Description of special parameters:
  * `CharginoCandidatesInfo`: if set to `1` will fill the `Track` TTree with chargino candidates as well (short selection list applied)
  * `TrueInfo`: if set to `1` will create and fill a TTree dedicated to MC Particles 
  * `TestMode`: if set to `1` will run only two files from the input dataset
  * `GridMode`: if set to `1` will set up the configuration to run on the grid

# Running to create the ntuples from the SLCIO files:
```bash
cd create_ntuples
python run_locally.py <run config file>
```
This script imports and runs the `ntuple_creator.py` module which is also equally used by the `submit_grid.py` script. The only difference is how the configuration and the paths are passed on, because that must be different on the grid.


# Running ntuple creation on the grid:
* First, set up dirac environment:
```
source /cvmfs/clicdp.cern.ch/DIRAC/bashrc
dirac-proxy-init
```
* upload the needed files in a tarball to the grid, e.g. like this:
```
cd create_ntuples
tar -cvzf lib.tar.gz ntuple_creator.py lib/*py ../cfg/*json
TIMESTAMP=2020-07-22
dirac-dms-add-file /ilc/user/s/schnooru/LLP/LLP_${TIMESTAMP}/lib.tar.gz lib.tar.gz CERN-DST-EOS
cd ..
dirac-dms-replicate-lfn /ilc/user/s/schnooru/LLP/LLP_${TIMESTAMP}/lib.tar.gz DESY-SRM
dirac-dms-replicate-lfn /ilc/user/s/schnooru/LLP/LLP_${TIMESTAMP}/lib.tar.gz RAL-SRM

```
* Additional special parameters:
  * `TestMode`: if set to `1` will run only three jobs
  * `grid_tarball`: point to the LFN files created above. To find the ones you have already uploaded: `dirac-dms-user-lfns -b /ilc/user/s/schnooru/LLP`
  * `GridOutputPath`: output Path on your personal grid folder, NB: no leading or trailing `/` !
  * `Grid_Files_Per_Job`: set how many files per job you want to run
* Then, submit the job:
```bash
cd create_ntuples
python submit_grid.py <run config file>
```

# Analyse the ntuples and put them in histograms:
```bash
cd analyze_data
python analyzer.py <run config file>
```
# Read the histograms from file and plot them:
```bash
cd plot_histograms
python plot.py <run config file>
```

for the CLICdp Style:

<https://gitlab.cern.ch/CLICdp/Publications/Templates/Style/blob/master/CLICdpStyle/rootstyle/CLICdpStyle.C> 

copy this file in the home directory.

# Unit tests
Unit tests are in the folder `analysis/tests`. They are ready to be run by doing `python test_ntuple_creator.py` (for example).


# Documentation of the TBranches in the `Event` TTree

## Preselection
The chargino candidates ch1 and ch2 are preselected: they are those tracks from "SiTracks_Refitted" which also have a pT > 1 GeV, a sqrt(d0^2+z0^2) < 0.5, and which are not associated to a pfo if they have pT > 1.5 GeV, and which are isolated (not more than 10% of their pT should be the sum of the pTs of the tracks surrounding it in a cone of R=0.1).

## Nomenclature

Names of TBranches in the `Event` TTree are usually named as `<particle/object>_<property/observable>`.

Particle/Object names:
+ `Ch1`, `Ch2`: the reco-level (preselected) chargino candidates, sorted by a sorting criterion (CURRENTLY pT). If the property is  a property of the truth-matched chargino (particle), this is mentioned in the name of the observable (e.g. `truePDGID`).
+ `TrueChm`, `TrueChp`: The `MCParticles` with PDGID -1000024 (`TrueChm`) and +1000024 (`TrueChp`)
+ Same for Pions: `TruePionm`: pi-, `TruePionp`: pi+ 

Default values are chosen in such a way that they are not physical. They have to be removed in the analysis because the tree is filled even if there is no Ch2 or Ch1 present. For this purpose, the number of chargino candidates can be accessed as `Ch_N`.

## TBranches
### True Charginos (MCParticles with |PDGID| = 1000024)

+ `TrueChm_PT`, `TrueChp_PT`: Transverse momentum of the true charginos (`Chm`: chargino-, `Chp`: chargino+)
+ `TrueChm_P`, `TrueChp_P`: Momentum of the true charginos
+ `TrueChm_Theta`, `TrueChp_Theta`: Theta of the true charginos
+ `TrueChm_Phi`, `TrueChp_Phi`: Phi of the true charginos
+ `TrueChm_VertexR`, `TrueChp_VertexR`: VertexR of the true charginos
+ `TrueChm_E`, `TrueChp_E`: Energy of the true charginos
+ `TrueChm_TrajLen`, `TrueChp_TrajLen`: Length between (0,0,0) and the decay vertex of the true charginos (=trajectory length for straight trajectories, which they are to good approximation
+ `TrueChm_M`, `TrueChp_M`: Mass of the true charginos (should be 1050 GeV)
+ `TrueChm_PDGID`, `TrueChp_PDGID`: PDGID of the particle (to double-check)

### True Pions  (MCParticles with |PDGID| = 211)

+ `TruePionm_PT`, `TruePionp_PT`: Transverse momentum of the true pions (`Pionm`: pion-, `Pionp`: pion+)
+ `TruePionm_P`, `TruePionp_P`: Momentum of the true pions
+ `TruePionm_Theta`, `TruePionp_Theta`: Theta of the true pions
+ `TruePionm_Phi`, `TruePionp_Phi`: Phi of the true pions
+ `TruePionm_VertexR`, `TruePionp_VertexR`: VertexR of the true pions
+ `TruePionm_E`, `TruePionp_E`: Energy of the true pions
+ `TruePionm_M`, `TruePionp_M`: Mass of the true pions
+ `TruePionm_PDGID`, `TruePionp_PDGID`: PDGID of the particle (to double-check)


### Chargino candidates (tracks selected according to the preselection)
ordering by Pt (currently - could be changed to dEdx possibly)

+ `Ch_N`: number of chargino candidates

+ `Ch1_truePDGID`: true PDGID obtained from the PDGID of the majority of the sim hits belonging to the  first chargino
+ `Ch1_PT`      
+ `Ch1_P`       
+ `Ch1_Theta`   
+ `Ch1_Phi`     
+ `Ch1_FirsthitR` 
+ `Ch1_LasthitR` 
+ `Ch1_VertexR` 
+ `Ch1_PTFraction_cone01` 
+ `Ch1_dEdx_Avrg`
+ `Ch1_N_AssocPFO`
+ `Ch1_N_ConePions` : number of pion candidates associated to this chargino
+ `Ch1_D0`
+ `Ch1_Z0`

		
+ `Ch2_truePDGID` 
+ `Ch2_PT`        
+ `Ch2_P`         
+ `Ch2_Theta`     
+ `Ch2_Phi`       
+ `Ch2_FirsthitR` 
+ `Ch2_LastHitR`  
+ `Ch2_VertexR` 
+ `Ch2_PTFraction_cone01` 
+ `Ch2_dEdx_Avrg` 
+ `Ch2_N_AssocPFO`  
+ `Ch2_N_ConePions` : number of pion candidates associated to this chargino
+ `Ch2_D0`
+ `Ch2_Z0`


### Pion candidates
ordering by Distance with closer chargino

+ `Pi_N`: number of pion candidates

+ `Pi1_truePDGID`
+ `Pi1_PT`       
+ `Pi1_P`        
+ `Pi1_Theta`    
+ `Pi1_Phi`      
+ `Pi1_FirsthitR` 
+ `Pi1_LastHitR`  
+ `Pi1_VertexR`  
+ `Pi1_N_AssocPFO`
+ `Pi1_D0`
+ `Pi1_Z0`
+ `Pi1_PTFraction_cone01`

+ `Pi2_truePDGID`
+ `Pi2_PT`
+ `Pi2_P`
+ `Pi2_Theta`
+ `Pi2_Phi`
+ `Pi2_FirsthitR` 
+ `Pi2_LastHitR`  
+ `Pi2_VertexR`
+ `Pi2_N_AssocPFO`
+ `Pi2_D0`
+ `Pi2_Z0`
+ `Pi2_PTFraction_cone01`
