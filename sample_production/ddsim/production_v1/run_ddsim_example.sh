ddsim \
    --inputFiles ../whizard/charginopair.stdhep \
    -N 10000 \
    --compactFile /cvmfs/clicdp.cern.ch/iLCSoft/builds/nightly/x86_64-slc6-gcc62-opt/lcgeo/HEAD/CLIC/compact/CLIC_o3_v14/CLIC_o3_v14.xml \
    --physics.pdgfile particle_m_1050_dm_355_3ct_180.tbl \
    --outputFile production_v1/ch1ch1_m_1050_dm_355_nopol_3ct_180_1.slcio
