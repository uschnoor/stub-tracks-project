from pyLCIO import IOIMPL
from ROOT import gStyle, gROOT
from math import sqrt
from ROOT import TCanvas, TH1D, TF1, TLorentzVector, TVector3,kRed,kGreen,kDashed,kBlue
gStyle.SetOptStat(0)
gStyle.SetLineWidth(2)
gStyle.SetTitleFont(42)
gStyle.SetLabelFont(42)
gStyle.SetPadTickX(1)
gStyle.SetPadTickY(1)
gStyle.SetLabelSize(0.04)
gROOT.ForceStyle()

# create a reader and open an LCIO file
reader = IOIMPL.LCFactory.getInstance().createLCReader()

simfile = "/eos/experiment/clicdp/data/user/s/schnooru/stubs/production_v0/ch1ch1_m1050_dm_160_355_nopol_ct_60_SIM.slcio"

reader.open(simfile)
print ("Opening sim file {}".format(simfile))


draw_cumulative = False

# create a canvas for the histogram
canvas = TCanvas( 'aCanvas', 'my Canvas', 800, 700 )
# create a histogram for the hit energies
histogram = TH1D( 'displacement', ';decay displacement [mm];Entries', 100, 0., 200 )


nch, nch_46, nch_58 = 0, 0, 0

# loop over all events in the file
for n,event in enumerate(reader):
    if n % 1000 == 0:
        print("running on event {}".format(n))
    # get the collection from the event


    pCollection = event.getCollection("MCParticle")
    for p in pCollection:
        if abs(p.getPDG() ) == 1000024:
            nch +=1
            epv = p.getEndpointVec()

            chargino_mom = p.getLorentzVec()
            pchi = p.getLorentzVec().P()
            displacement_boost = 1050.* epv.Mag()/pchi
            histogram.Fill( displacement_boost )
            
            vertex_R = epv.Perp()

            if vertex_R >=46:
                nch_46+=1
                if vertex_R >= 58:
                    nch_58+=1

reader.close()


print("Total number of charginos: {}".format(nch))
print("Number of charginos with R of decay vertex at least 46mm (4 hits): {}".format(nch_46))
print("Number of charginos with R of decay vertex at least 58mm (6 hits): {}".format(nch_58))



histogram.Draw()
if draw_cumulative:
    #this is not necessary because fitting the exponential gives the same
    #result for the exponent as fitting the cumulative of the exponential
    hcumu = TH1D( 'cumulative', 'cumulative;x [mm];Entries', 100, 0., 200 )
    hcumu.SetLineColor(kBlue)
    for b in range(0,histogram.GetXaxis().GetNbins()+1):
        cumulative_b = 0
        for bi in range(b,histogram.GetXaxis().GetNbins()+2):
            cumulative_b += histogram.GetBinContent(bi)
        hcumu.SetBinContent(b, cumulative_b)
    hcumu.SetLineColor(kGreen)
    hcumu.Draw("same")
    histogram.GetYaxis().SetRangeUser(0,22000)
canvas.Modified()
canvas.Update()

raw_input("Now, normalize to 1? (Enter)")
histogram.Scale(1./histogram.Integral())
histogram.GetYaxis().SetTitle("a.u.")
histogram.Draw()
canvas.Modified()
canvas.Update()

raw_input("Now, fit? (Enter)")


fitFunction = TF1( 'aFunction', '[0]*exp(-[1]*x)', 0, 200 )
print("Form of the fit function: p0*exp(-p1*x)")
fitFunction.SetLineColor( kRed )
fitFunction.SetLineStyle( kDashed )
#
if draw_cumulative:
    hcumu.Fit( fitFunction, 'R' )
    hcumu.Draw()
else:
    histogram.Fit( fitFunction, 'R' )
    histogram.Draw()
canvas.Modified()
canvas.Update()

fitFunction.Draw("l same")
raw_input()
