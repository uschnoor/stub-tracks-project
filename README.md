# Project on Long-Lived Particle Reconstruction in CLIC

## Content

+ `documents` folder for related documentation, presentations, articles, ...
+ `analysis_cecilia` folder for the summer student project material
+ `sample_production` folder for the summary and details of the samples used
+ `tracking_validation` for single pion and charginos track validation and overlay
+ `analysis` folder for the updated analysis scripts

## Git basics

+ How to obtain the repo:

```git clone https://:@gitlab.cern.ch:8443/uschnoor/stub-tracks-project.git```

+ Always check for changes:

```git pull```

+ Making changes:
```
cd stub-tracks-project
touch some.file
git add some.file
git commit -m "changed some.file"
git push -u origin master
```


+ Workflow help:
[https://rogerdudler.github.io/git-guide/](https://rogerdudler.github.io/git-guide/)

## Latest build of the note
* [Latest build of the note](https://gitlab.cern.ch/uschnoor/stub-tracks-project/builds/artifacts/master/raw/documents/clicdpNote/clicdpNote.pdf?job=fedora-latex)
