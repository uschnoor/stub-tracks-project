## Tracking validation of LLP sample

The [gitlab repo](https://gitlab.cern.ch/ericabro/trackingvalidation) for the tracking validation was used to produce efficiency, fake rate and duplicates for the LLP sample without and with the overlay.
Please use the customised branch `LLP`.

A customised version of the validator was run to remove the cut on the genStatus.
The files containing the ntuples can be found in `/eos/experiment/clicdp/data/user/e/ericabro/stubs/`.

Produce histo root with
```
cd run_analysis
mkdir results
python script_analysis_LLP.py
```
Currently only the efficiency is calculated for the sample with overlay.

Produce final plots with
```
cd produce_plots
python script_plotting.py ../cfg/run_script_plotting_LLP.json
python script_plotting_LLP_recosim.py
```
