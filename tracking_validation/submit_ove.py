#!/bin/python
import sys, getopt

#####################################################################

#parameters
nJobs = int(sys.argv[5])
nEvts = int(sys.argv[6])
nameTag = sys.argv[1]+'_'+sys.argv[2]+'_'+sys.argv[3]+'_'+sys.argv[4]
nameJobGroup = 'efficiencies_ILDLike_modified'
clicConfig = 'ILCSoft-2020-02-07'
ddsimVersion = 'ILCSoft-2020-02-07_gcc62' 
marlinVersion = 'ILCSoft-2020-02-07_gcc62' 
detectorModel =  'CLIC_o3_v14'
baseSteeringMarlin = '/home/ericabro/CLICstudies/2020/LLP/iLCSoft-2020-02-07/CLICPerformance/clicConfig/clicReconstruction_monitoring_stubs.xml'
nameSteeringMarlin = '/home/ericabro/CLICstudies/2020/LLP/stub-tracks-project/validation/clicReconstruction_monitoring_stubs_final.xml'
templateOutRoot = "histograms"
#rootFile = sys.argv[1]
nameDir = 'CLIC/2020/CLICo3v14/'+clicConfig+'/'+nameJobGroup+'/files_'+nameTag
prodID = sys.argv[4]

#####################################################################

#with open(baseSteeringMarlin) as f:
#    open(nameSteeringMarlin,"w").write(f.read().replace(templateOutRoot,rootFile))
 
#####################################################################     
#set environment          
import os
import sys

from DIRAC.Core.Base import Script #dirac environment                                              
Script.parseCommandLine() #dirac environment     

from ILCDIRAC.Interfaces.API.DiracILC import DiracILC #job receiver class   
dirac = DiracILC(False)      
from ILCDIRAC.Interfaces.API.NewInterface.UserJob import UserJob
from DIRAC.Resources.Catalog.FileCatalogClient import FileCatalogClient 
from ILCDIRAC.Interfaces.API.NewInterface.Applications import DDSim
from ILCDIRAC.Interfaces.API.NewInterface.Applications import Marlin

#####################################################################      
fcc = FileCatalogClient()
files = fcc.findFilesByMetadata( { 'ProdID': prodID , 'Datatype' : 'DST'} )

if not files['OK']:
  print "Error finding files", files['Message']
  exit(1)

files = files['Value']

for i, inputLFN in enumerate(files): 
    if i > nJobs:
        break

    print "file number", i, inputLFN
    rootFile = sys.argv[1]
    rootFile = rootFile + '_j%d' % (i)
    with open(baseSteeringMarlin) as f:
      open(nameSteeringMarlin,"w").write(f.read().replace(templateOutRoot,rootFile))

#job definition   

    job = UserJob() #use UserJob unless recommended differently  
    job.setName(nameTag)
    job.setJobGroup(nameJobGroup)
    job.setCPUTime(86400)
    job.setBannedSites(['LCG.UKI-LT2-IC-HEP.uk','LCG.KEK.jp','LCG.IN2P3-CC.fr','LCG.Tau.il','Weizmann.il','LCG.Weizmann.il','OSG.MIT.us','OSG.FNAL_FERMIGRID.us','OSG.GridUNESP_CENTRAL.br','OSG.SPRACE.br'])
    job.setInputSandbox([nameSteeringMarlin,'LFN:/ilc/user/e/ericabro/libCLICPerformance-iLCSoft-2020-02-07_ILDLike_modified.tar.gz'])
    job.setOutputSandbox(["*.log"]) 
    job.setInputData([inputLFN])
    job.setOutputData([rootFile+".root"],nameDir,"CERN-DST-EOS")   
    #job.setSplitEvents(nEvts,nJobs)

#####################################################################

#marlin
    ma = Marlin()
    ma.setVersion(marlinVersion)
    ma.setNbEvts(nEvts)
    ma.setDetectorModel(detectorModel)
    ma.setProcessorsToUse([])
    ma.setSteeringFile(nameSteeringMarlin)
    res = job.append(ma) 

    if not res['OK']:
        print res['Message']
        sys.exit(2)

#####################################################################      
#submit          

    job.dontPromptMe()
    print job.submit(dirac)

