#!/bin/bash

for i in $(find . -name "*.tex" ! -name "authors*.tex"); do
  echo "WARNING Possible spelling mistakes in $i WARNING" >> errors.txt
  cat $i | aspell -a --mode=tex --lang=en_GB-ise | cut -d ' ' -f 2 | grep -v '*' | sort | uniq > spell.txt
  hunspell -t -l $i >> spell.txt
  cat spell.txt | sort | uniq >> errors.txt
done

IFS=$'\n'    # make newlines the only separator
set -f       # disable globbing
sed -i '/^\s*$/d' errors.txt
for x in $(cat < ./ignore_spelling.txt); do
  sed -i '/^'"${x}"'/d' errors.txt
done
