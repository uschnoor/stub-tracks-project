\section{Event simulation}
\label{sec:simulation}

\subsection{Generation of signal samples}

As the disappearing tracks signature is a somewhat unusual signature,
at the time of writing, most tools do not support finite-lifetime,
long-lived particles out of the box. Therefore, several modifications of the
event generation and simulation as well as the output format have been
used in order to obtain access to the necessary information.
These modifications are documented in the following.

\subsubsection{Process and model definition}
The signal events are generated with \whizard~2~\cite{Kilian:2007gr}.
The signal process is defined as $\epem \rightarrow \tilde{\chi}^+_1
\tilde{\chi}^-_1 \rightarrow \tilde{\chi}^0_1 \pi^+ \tilde{\chi}^0_1
\pi^-$, where $\tilde{\chi}^\pm_1$ is the chargino and
$\tilde{\chi}^0$ is the neutralino of the first
generation in the minimal supersymmetric
extension of the SM (MSSM).

In order to obtain the properties of the degenerate Higgsino dark
matter model introduced in Sec.~\ref{sec:introduction}, the following
parameters are set:

\begin{itemize}
\item Mass of the chargino $m_{\tilde{\chi}_1^\pm} = 1050$\,GeV
  (PDGID\footnote{The PDGID is the identification number for particles
  according to the particle data group~\cite{PDG}.} $\pm$1000024)
\item Mass of the neutralino $m_{\tilde{\chi}_1^0} = 1049.8$\,GeV
  (PDGID 1000022)
\item The process is defined in a decay chain in Whizard:
  \begin{lstlisting}
process charginopair = e1, E1 => "ch1+", "ch1-"
process dec_chargino1m  = "ch1-" => ubar, d, neu1
process dec_chargino1p  = "ch1+" => dbar, u, neu1
unstable "ch1-" (dec_chargino1m)
unstable "ch1+" (dec_chargino1p)
\end{lstlisting}
which is then integrated and passed to the event simulation in the
  following way:
\begin{lstlisting}
integrate (dec_chargino1m, dec_chargino1p) { iterations = 1:1000 }
integrate (charginopair) { iterations = 5:10000, 2:10000 }
simulate (charginopair) {}
\end{lstlisting}

\item In addition, the mixing matrices for the pure higgsino and pure
  wino cases are set according to the description in App.~\ref{app:model}.
  
\end{itemize}
The full Sindarin description for Whizard2 used for the event
generation is given in App.~\ref{app:sindarin}.

\subsubsection{Simulation of finite-lifetime particles}
The event generation consists of the integration of this process as
well as the sampling of unweighted events.
The hadronic particles are subject to parton shower and
hadronisation as usual.


In this case, an additional crucial ingredient is the finite lifetime of the
charginos.
Appendix~\ref{app:pythia-lifetimes} gives instructions
  on alternative ways to implement this. The chosen solution for the event generation with
finite-lifetime chargino is the following procedure:
\begin{enumerate}
\item Generation of the signal process with partonic final state $\epem \rightarrow \tilde{\chi}^+_1
\tilde{\chi}^-_1 \rightarrow \tilde{\chi}^0_1 u \bar{d} \tilde{\chi}^0_1
\bar{u} d$
within \whizard using the settings of the MSSM listed above.
\item Modification of the Geant4 particle table to use the given
  lifetime of the chargino in the detector simulation.
\end{enumerate}

The Geant4 table is modified by adding or changing the respective
lines for SUSY charginos and neutralino
in the particle table file (\texttt{particle.tbl}):



\begin{lstlisting}
1000022 susy-chi_1^0   0   1049.645     0.00000   0.00000E+00
1000024 susy-chi_1^+   3   1050.00000   1.00000   180
-1000024 susy-chi_1^- -3   1050.00000   1.00000   180
\end{lstlisting}

This file \texttt{particle.tbl} is then passed to \texttt{ddsim}
through the option 
\texttt{---physics.pdgfile particle.tbl}. Within the CLIC MC production
framework, the \texttt{[DDSIM]} section is modified with the following
configuration:

\begin{lstlisting}
  ExtraParticles = 1000022 susy-chi_1^0 0 1049.645 0.00000 0.00000E+00
  1000024 susy-chi_1^+ 3 1050.00000 1.00000 6.9 -1000024 susy-chi_1^- -3 1050.00000 1.00000 6.9
\end{lstlisting}
\subsubsection{Available signal samples}
The signal process $\epem \rightarrow \tilde{\chi}^+_1 \tilde{\chi}^-_1 \rightarrow \tilde{\chi}^0_1 \pi^+ \tilde{\chi}^0_1 \pi^-$ 
is generated with \whizard{}~\cite{Kilian:2007gr,Moretti:2001zz}
interfaced to \pythia{}~\cite{Sjostrand:2006za} for parton shower and hadronisation.
Chargino and neutralino masses are set to \SI{1050}{\MeV} and \SI{1049.8}{\GeV}, respectively.
The performance of the disappearing signal is assessed with three
different samples listed in Table~\ref{tab:signal-samples}.


\begin{table}[ht]\centering
  \caption{Available signal samples}\label{tab:signal-samples}  
  \begin{tabular}[ht]{llcc}
    \toprule
    & mean $c\tau$ [mm]& P(e-) & \\\midrule
    & \SI{6.9}{\mm} & $\pm80\%$ \\
  &  \SI{180}{\mm} & 0\\
    & \SI{600}{\mm} & $\pm80\%$ \\
\bottomrule
  \end{tabular}
  

\end{table}

\subsection{Lifetime reweighting}

In order to ensure an efficient Monte Carlo generation, the sample
is produced with longer lifetime. This leads to proportionally longer
chargino tracks and a larger fraction of tracks which reach the detector.
These events can be reweighted according to the kinematics and the
length of the charginos' trajectories to a different mean proper
lifetime by applying a weight for each chargino according to its
survival probability in the following way:


The survival probability \(P_s\) is the probability for the chargino of
mass \(m_{\tilde{\chi}}\) and momentum \(p_{\tilde{\chi}}\) with a width
of \(\Gamma_{\tilde{\chi}}\) to travel at least the distance
\(d_{min}\):
\begin{equation}
 P_s (d_{\text{min}}) = \frac{1}{k} \exp(-m_{\tilde{\chi}} d_{min} \Gamma_{\tilde{\chi}}/p_{\tilde{\chi}})\label{eq:survivalprob}
\end{equation}

where \(k = \frac{pc\tau}{m}\) is the normalisation constant.

This corresponds to the cumulative distribution
\begin{equation}
 P_s (d_{\text{min}}) = \frac{1}{k}
 \int_{d_{\text{min}}}^\infty{\exp(-m_{\tilde{\chi}} \ell
   \,\Gamma_{\tilde{\chi}}/p_{\tilde{\chi}})}
 \text{d}\ell\label{eq:cumul_survivalprob}
\end{equation}

where \(\ell\) is the distance of the decay vertex from (0,0,0) in the
lab frame, which is retrieved from the event record as
\texttt{p.getEndpointVec().Mag()}. The factor \(mass/momentum\) boosts
the trajectory in the rest frame of the chargino. We therefore call
\(m_{\tilde{\chi}} \ell /p_{\tilde{\chi}}\) in the following the
\textit{boosted displacement}. We use an exponential function in the fit
to determine the mean proper lifetime in the sample.

   In this way, the exponent coefficient of the fit function below is
just 
\[\frac{m_{\tilde{\chi}} \ell }{p_{\tilde{\chi}}c \tau}\] with the
lifetime \(c\tau = 1/\Gamma_{\tilde{\chi}}\). This allows us to check the
results of the reweighting by fitting an exponential function as demonstrated
in Fig~\ref{fig:rw_displacement_boost}.


\begin{figure}[ht]
  \centering
  \includegraphics[width=0.5\textwidth]{figures/reweighting/boosted_displacement_reweighted.png}
  
  \caption{Comparison of the boosted displacement of the reweighted sample with an original mean
    lifetime of \SI{600}{mm} to the target of \SI{6.9}{mm}. Note that
    both are normalized to 1. The range $ 30 < (m/p)\ell < \SI{60}{mm}$ is
    considered as it corresponds roughly to the most relevant range inside the
    detector.}
  \label{fig:rw_displacement_boost}
\end{figure}
The weights \(w\) are defined  in dependence on the
length of the trajectory in the lab frame \(\ell\) as

\[ w(\ell) = \frac{P_{target}(\ell)}{P_{MC}(\ell)} \] where the target
lifetime is 6.9mm (or the one used for the limits later) and the \(MC\)
lifetime is 600mm for the production used.
As the events contain two charginos, each one contributes a weight
factor according to its truth trajectory length $\ell$ obtained from
the MC information. The weights are multiplied to the cross-section
and luminosity weight.

This evaluates to
\[ w(\ell) = \frac{P_{target}(\ell)}{P_{MC}(\ell)} = \frac{c
    \tau_{MC}}{c \tau_{target}} \exp\left(- m_{\tilde{\chi}}
    \frac{\ell}{p_{\tilde{\chi}}} \left[\frac{1}{c \tau_{MC}} -
      \frac{1}{c \tau_{target}} \right]\right). \]


Figure~\ref{fig:rw_displacement_boost} shows the good agreement between the distributions of the
boosted displacement for the sample with 6.9\,mm lifetime and the
reweighted sample, validating the procedure. 
In the low $c\tau$ range, the reweighting actually does not decrease the
statistical uncertainties. However, the actually relevant range
is beyond the vertex detector inner radius ($\approx$30\,mm at $\theta=90^\circ$). There, the nominal sample (green
line) has larger statistical uncertainties compared to the reweighted sample (blue line).
The crossover point from \(w>1\) to \(w<1\) is:

\[ 1 = w(\ell) =  \frac{c \tau_{MC}}{c \tau_{target}} \exp\left(- m_{\tilde{\chi}} \frac{\ell}{p_{\tilde{\chi}}} \left[\frac{1}{c \tau_{MC}} - \frac{1}{c \tau_{target}} \right]\right)\]

\[\Leftrightarrow \]

\[ \ell = \frac{p}{m} \log\left(\frac{c\tau_{MC}}{c\tau_{target}}\right) \frac{1}{ \left[ \frac{1}{c\tau_{target}} - \frac{1}{c\tau_{MC}} \right] }\]

For \(p\approx 1000\,\)GeV, this becomes \(\ell \approx 29.7\,\)mm. That
is  close to the inner radius of the CLICdet vertex detector. % Since the
% weights are then still close to 1, it might be even better
% to take a somewhat lower value of \(c\tau_{MC}\) such as 300mm.
    This shows that for values above ca. 40\,mm, the statistical
errors decrease when using the reweighted sample rather than the
original sample.



The reweighting is further validated by comparing generator-level kinematic variables
of the reweighted sample to the sample with $c\tau=6.9$\,mm.
Figure \ref{fig:rw_kinematics} illustrates the kinematic properties of
the charginos using the reweighting method. The good agreement between
the two cases indicates that the  samples
can be reweighted according to the lifetime, neglecting the effects from beamstrahlung and ISR.
\begin{figure}[ht]
  \centering
  \includegraphics[width=0.4\textwidth]{figures/reweighting/charginos_theta_reweighted_acc.pdf}
  \includegraphics[width=0.4\textwidth]{figures/reweighting/charginos_p_reweighted_acc.pdf}\\
(a) Polar angle $\theta$ distribution of the
charginos. ~~~~~~(b) Momentum distribution of the charginos.  \\
    \includegraphics[width=0.4\textwidth]{figures/reweighting/charginos_pT_reweighted_acc.pdf}
\\(c) Transverse momentum distribution of the charginos.
  \caption{Comparison of the generator-level kinematic distributions of the charginos
    between the sample with the mean proper lifetime of 6.9\,mm and the reweighted sample with an original mean
    lifetime of \SI{600}{mm} to the target of \SI{6.9}{mm}. Note that
    both are normalized to 1. Charginos with a trajectory length in
    the laboratory frame of at
  least 30\,mm are considered, which mimics the effect of the detector
  acceptance.}
  \label{fig:rw_kinematics}
\end{figure}




Figure \ref{fig:rw_kinematics_pions} illustrates the effect of the
reweighting applied to the event on the kinematics of the pions. In
this case, no acceptance cut is applied. Therefore, the non-reweighted
sample has larger statistics and thus smaller statistical error bars.
Good agreement between the reweighted and the nominal samples can be
seen. The reweighted sample is slightly shifted to higher momentum and
more central pions, but this shift is still covered by the statistical uncertainty.

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.4\textwidth]{figures/reweighting/pions_theta_reweighted.pdf}
  \includegraphics[width=0.4\textwidth]{figures/reweighting/pions_p_reweighted.pdf}\\
(a) Polar angle $\theta$ distribution of the
pions. ~~~~~~(b) Momentum distribution of the pions.  \\
    \includegraphics[width=0.4\textwidth]{figures/reweighting/pions_pT_reweighted.pdf}
\\(c) Transverse momentum distribution of the pions.
  \caption{Comparison of the generator-level kinematic distributions of the pions
    between the sample with the mean proper chargino lifetime of 6.9\,mm and the reweighted sample with an original mean
    lifetime of \SI{600}{mm} to the target of \SI{6.9}{mm}. Note that
    both are normalized to 1. No acceptance cut on the chargino length
  is applied. Both pions in one event are used.}
  \label{fig:rw_kinematics_pions}
\end{figure}




\subsection{Generation of background samples}
\label{sec:simulation_bg}

The most dominant background for this analysis will be from \gghad
production.
While the produced particles in \gghad are soft,
the signal reconstruction does not allow hard cuts to be placed on the
transverse momentum of the stub tracks.
Algorithmic
effects, split tracks, and conversions can lead to signatures similar
to a prompt, disappearing track.
As the \gghad process has a very high cross section, this background
cannot be neglected.


For technical reasons, the beam-induced \gghad background is simulated
using the process $\epem \to \nunubar$ while overlaying 30 bunch
crossings of \gghad to the event. The neutrino pair final state was
chosen such that no other particle than
those originating from \gghad is detectable.
Using this process is a technical workaround due to the different
format of the \gghad events available.
In principle, another approach could be chosen as long as they result
in the correct distribution of \gghad kinematics and distribution of
these events in the bunch crossings.


On average, 3.2 \gghad interactions take place in every bunch
crossing~\cite{cdrvol2} above a threshold of $W_{\gamma\gamma} >
\SI{2}{GeV}$.
This leads to a total of $4.2\times 10^{12}$ of \gghad interactions in
5\,ab$^{-1}$ collected at the 3\,TeV stage of CLIC.
Even if it is unlikely that hits created by these events are
mis-reconstructed as high-energy disappearing tracks, the large total
number means that this contribution should be considered.

The available sample of \gghad events contains 67400 events.
They are organised in files of 100 events read consecutively, while
the order of the filled bunch crossings and their numbers are randomised.
The number of \gghad events
overlayed to a bunch crossing is drawn randomly from  a Poissonian
distribution with mean 3.2 for each bunch crossing.
A total of 30 bunch crossings is overlayed to one event.


