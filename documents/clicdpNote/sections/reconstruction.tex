\section{Particle reconstruction and performance}
\label{sec:reconstruction}
\subsection{Reconstruction}



For all samples, 30 bunch crossings (BX) of beam-induced \gghad{} background are overlaid to each event~\cite{LCD:overlay}.

The linear collider \marlin{}-framework~\cite{MarlinLCCD} which is part of \ilcsoft{}~\cite{ilcsoft}
is used for the CLIC event reconstruction software. 
Large simulation and reconstruction
samples were produced with the \ilcdirac{} grid production tool\todo[color=green]{Add Refs!}.

In a first step, the expected number of \gghad{} interactions for 30 bunch crossings is overlaid to
the signal event, which is placed in bunch crossing 11 at $t=0~\si{\ns}$.
In order to simulate the expected detector timing resolutions and integration times,
only the energy deposits inside the timing window of \SI{5}{\ns} before and \SI{10}{\ns} after 
the signal event are selected. 
In the following step, the positions of the energy deposits in the tracking detectors are smeared
according to the sensor expected resolution and the energy of the calorimeter hits are scaled with the calibration constants.

The event reconstruction starts with running the track reconstruction algorithm.
The conformal tracking is the pattern recognition technique used that
combines the conformal mapping approach with a cellular automaton-based track following~\cite{ConformalTracking}.
At the end of the pattern recognition, a standard Kalman smoother filter is run to obtain the final track parameters.
Moreover, a dedicated algorithm is also performed to merge all tracks
which are sharing more than one hit and are stemmed from the same initial hit, also called \emph{duplicates}.
%Thanks to the double layers in the vertex detector, the minimum requirement set on the number of hits 
%can be lowered to three for this specific analysis.

Reconstructed tracks are then used as input together with the calorimeter clusters
to the particle flow algorithm by \pandora~\cite{Thomson:2009rp,Marshall:2012ryPandoraPFA,Marshall:2015rfaPandoraSDK}.
All visible particles reconstructed by \pandora are called \textit{particle flow objects} (PFOs).
A more detailed description of the reconstruction of single particles and more complex events is given in~\cite{CLICperf}.


Due to the non-standard nature of this signature, available samples in
the \texttt{DST} format do not include sufficient hit-level
information. We therefore add the necessary collections to the LCIO
output file. The corresponding settings are detailed in App.~\ref{app:lcio_collections}.


\subsection{Tracking performance}
\label{sec:tracking}

The tracking performance in this analysis is particularly important for the final identification of the signal.
In the case of the track left by charginos, the main challenge is to reconstruct very short and straight tracks.
From purely geometrical considerations, they typically traverse only few layers of the Vertex Detector,  
leaving in most of the cases a minimum of four hits and a maximum of six hits according to the different life times considered.
\todo{We can add more info here from the purely geometrical constraints but I dont think we need actually}
In the case of the pions, these tracks are soft and therefore they spiral in the detector without reaching the calorimenters. 
Moreover, their displacement will depend on the lifetime of the decaying changino.

The track reconstruction performance were evaluated in terms of tracking efficiency,
fake rate and duplicates for all three samples, including the overlay. 
Any selection on the simulated MC particles is removed compared to the standard analysis.
Here only the most important results are reported.

\subsubsection{Chargino tracks} 

In~\cref{fig:ch_tracking_eff}(a) the tracking efficiency for charginos track for signal-only samples for different life times is
displayed as a function of the polar angle. For the two longer life time, the efficiency is well above 90\% for the entire
range covered by the tracking system. The shorter life time brings the tracking efficiency down to 50\%. 
It is therefore needed for this particular sample the reconstruction and the matching of the pion track as well.

In~\cref{fig:ch_tracking_eff}(b) the efficiency of the signal-only \SI{600}{\mm} sample
is presented as a function of the transverse momentum without and with the \gghad{} background overlaid.
The impact of the background tracks on the chargino track reconstruction can be considered negligible.
In all samples considered and with the inclusion of the background, the number of tracks that
fake the signal signature can be considered negligible.

\begin{figure}[tb]
  \centering
  \begin{subfigure}[!b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{figures/tracking/charginos/eff_vs_theta.eps}
  \end{subfigure}
  \hfill
  \begin{subfigure}[!b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{figures/tracking/charginos_600mm/eff_vs_pt.eps}
  \end{subfigure}
  \caption{Chargino track reconstruction efficiency as a function of $\theta$
  for the three life times samples (a) and as a function of the \pT for \SI{600}{\mm} sample 
  reconstructed with and without the overlay of 30 BXs of \gghad{} 
  background expected at \SI{3}{\TeV} CLIC energy stage (b).}
  \label{fig:ch_tracking_eff}
\end{figure}

The comparison between the simulated particle and the reconstructed track associated 
is shown in~\cref{fig:ch_tracking_comparison}. While the polar and aximutal angles are correctly reconstructed, 
the limited arm length of the track does not allow a precise reconstruction of the
transverse momentum, which is dominated by the maximum reconstructable sagitta
and it is associated mostly to lower \pT.

\begin{figure}[tb]
  \centering
  \begin{subfigure}[!b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{figures/tracking/charginos_600mm/sim_vs_reco_theta.eps}
  \end{subfigure}
  \hfill
  \begin{subfigure}[!b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{figures/tracking/charginos_600mm/sim_vs_reco_pt.eps}
  \end{subfigure}
  \caption{Polar angle (a) and transverse momentum (b) comparison for MC particle and the associated reconstructed track
  for \SI{600}{\mm} lifetime sample.}
  \label{fig:ch_tracking_comparison}
\end{figure}

\subsubsection{Pion tracks} 

In~\cref{fig:pi_tracking_eff}(a) the tracking efficiency for pions track for signal-only samples for different life times is
displayed as a function of the polar angle. The track reconstruction becomes the more difficult the more displaced
is produced the pion. For the shortest life time sample, the efficiency is slightly above 80\% in the barrel region.

In~\cref{fig:pi_tracking_eff}(b) the efficiency of the signal-only \SI{6.9}{\mm} sample
is presented as a function of the transverse momentum without and with the \gghad{} background overlaid.
As for the chargino track, the impact of the background can be considered negligible.

\begin{figure}[tb]
  \centering
  \begin{subfigure}[!b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{figures/tracking/pions/eff_vs_theta.eps}
  \end{subfigure}
  \hfill
  \begin{subfigure}[!b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{figures/tracking/pions_007mm/eff_vs_pt.eps}
  \end{subfigure}
  \caption{Pion track reconstruction efficiency as a function of $\theta$
  for the three chargino life times samples (a) and as a function of the \pT for \SI{600}{\mm} sample 
  reconstructed with and without the overlay of 30 BXs of \gghad{} 
  background expected at \SI{3}{\TeV} CLIC energy stage (b).}
  \label{fig:pi_tracking_eff}
\end{figure}

The comparison between the simulated particle and the reconstructed track associated in the case of the pion
produced in the chargino decay is shown in~\cref{fig:pi_tracking_comparison}. 
The high peak in the central part of the detector is due to the very low trasnverse momenta of the pion track
which `loops` in the central detector many times, leaving multiple track segments.
The duplicate rate for signal-only samples for different life times is presented in~\cref{fig:pi_tracking_dupl}
along with a single event diplay of a `looper` pion.

\begin{figure}[tb]
  \centering
  \begin{subfigure}[!b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{figures/tracking/pions_007mm/sim_vs_reco_theta.eps}
  \end{subfigure}
  \hfill
  \begin{subfigure}[!b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{figures/tracking/pions_007mm/sim_vs_reco_pt.eps}
  \end{subfigure}
  \caption{Polar angle (a) and transverse momentum (b) comparison for MC particle and the associated reconstructed track
  for \SI{600}{\mm} lifetime sample.}
  \label{fig:pi_tracking_comparison}
\end{figure}


\begin{figure}[tb]
  \centering
  \begin{subfigure}[!b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{figures/tracking/pions/dupl_vs_theta.eps}
  \end{subfigure}
  \hfill
  \begin{subfigure}[!b]{0.48\textwidth}
    \includegraphics[width=\textwidth]{figures/tracking/pion_dupl.png}
  \end{subfigure}
  \caption{%Pion track reconstruction efficiency as a function of $\theta$
  %for the three chargino life times samples (a) and as a function of the \pT for \SI{600}{\mm} sample
  %reconstructed with and without the overlay of 30 BXs of \gghad{}
  %background expected at \SI{3}{\TeV} CLIC energy stage (b).}
  }
  \label{fig:pi_tracking_dupl}
\end{figure}

