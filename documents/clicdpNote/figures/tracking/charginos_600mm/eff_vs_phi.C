void eff_vs_phi()
{
//=========Macro generated from canvas: c/c
//=========  (Fri Aug 28 15:34:55 2020) by ROOT version6.08/00
   TCanvas *c = new TCanvas("c", "c",0,0,800,700);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c->SetHighLightColor(2);
   c->Range(-261.6,-0.2675676,218.4,1.218919);
   c->SetFillColor(0);
   c->SetBorderMode(0);
   c->SetBorderSize(2);
   c->SetGridx();
   c->SetGridy();
   c->SetTickx(1);
   c->SetTicky(1);
   c->SetLeftMargin(0.17);
   c->SetRightMargin(0.08);
   c->SetTopMargin(0.08);
   c->SetBottomMargin(0.18);
   c->SetFrameLineWidth(2);
   c->SetFrameBorderMode(0);
   c->SetFrameLineWidth(2);
   c->SetFrameBorderMode(0);
   
   TEfficiency * eff_vs_phi5 = new TEfficiency("eff_vs_phi","",180,-180,180);
   
   eff_vs_phi5->SetConfidenceLevel(0.6826895);
   eff_vs_phi5->SetBetaAlpha(1);
   eff_vs_phi5->SetBetaBeta(1);
   eff_vs_phi5->SetWeight(1);
   eff_vs_phi5->SetStatisticOption(0);
   eff_vs_phi5->SetPosteriorMode(0);
   eff_vs_phi5->SetShortestInterval(0);
   eff_vs_phi5->SetTotalEvents(0,0);
   eff_vs_phi5->SetPassedEvents(0,0);
   eff_vs_phi5->SetTotalEvents(1,444);
   eff_vs_phi5->SetPassedEvents(1,436);
   eff_vs_phi5->SetTotalEvents(2,428);
   eff_vs_phi5->SetPassedEvents(2,426);
   eff_vs_phi5->SetTotalEvents(3,456);
   eff_vs_phi5->SetPassedEvents(3,447);
   eff_vs_phi5->SetTotalEvents(4,437);
   eff_vs_phi5->SetPassedEvents(4,435);
   eff_vs_phi5->SetTotalEvents(5,479);
   eff_vs_phi5->SetPassedEvents(5,473);
   eff_vs_phi5->SetTotalEvents(6,442);
   eff_vs_phi5->SetPassedEvents(6,437);
   eff_vs_phi5->SetTotalEvents(7,449);
   eff_vs_phi5->SetPassedEvents(7,446);
   eff_vs_phi5->SetTotalEvents(8,449);
   eff_vs_phi5->SetPassedEvents(8,438);
   eff_vs_phi5->SetTotalEvents(9,442);
   eff_vs_phi5->SetPassedEvents(9,435);
   eff_vs_phi5->SetTotalEvents(10,425);
   eff_vs_phi5->SetPassedEvents(10,421);
   eff_vs_phi5->SetTotalEvents(11,433);
   eff_vs_phi5->SetPassedEvents(11,429);
   eff_vs_phi5->SetTotalEvents(12,424);
   eff_vs_phi5->SetPassedEvents(12,423);
   eff_vs_phi5->SetTotalEvents(13,480);
   eff_vs_phi5->SetPassedEvents(13,477);
   eff_vs_phi5->SetTotalEvents(14,481);
   eff_vs_phi5->SetPassedEvents(14,481);
   eff_vs_phi5->SetTotalEvents(15,438);
   eff_vs_phi5->SetPassedEvents(15,435);
   eff_vs_phi5->SetTotalEvents(16,471);
   eff_vs_phi5->SetPassedEvents(16,468);
   eff_vs_phi5->SetTotalEvents(17,476);
   eff_vs_phi5->SetPassedEvents(17,471);
   eff_vs_phi5->SetTotalEvents(18,416);
   eff_vs_phi5->SetPassedEvents(18,415);
   eff_vs_phi5->SetTotalEvents(19,475);
   eff_vs_phi5->SetPassedEvents(19,462);
   eff_vs_phi5->SetTotalEvents(20,433);
   eff_vs_phi5->SetPassedEvents(20,427);
   eff_vs_phi5->SetTotalEvents(21,444);
   eff_vs_phi5->SetPassedEvents(21,443);
   eff_vs_phi5->SetTotalEvents(22,443);
   eff_vs_phi5->SetPassedEvents(22,439);
   eff_vs_phi5->SetTotalEvents(23,452);
   eff_vs_phi5->SetPassedEvents(23,449);
   eff_vs_phi5->SetTotalEvents(24,465);
   eff_vs_phi5->SetPassedEvents(24,457);
   eff_vs_phi5->SetTotalEvents(25,459);
   eff_vs_phi5->SetPassedEvents(25,456);
   eff_vs_phi5->SetTotalEvents(26,484);
   eff_vs_phi5->SetPassedEvents(26,483);
   eff_vs_phi5->SetTotalEvents(27,461);
   eff_vs_phi5->SetPassedEvents(27,459);
   eff_vs_phi5->SetTotalEvents(28,438);
   eff_vs_phi5->SetPassedEvents(28,436);
   eff_vs_phi5->SetTotalEvents(29,490);
   eff_vs_phi5->SetPassedEvents(29,487);
   eff_vs_phi5->SetTotalEvents(30,426);
   eff_vs_phi5->SetPassedEvents(30,418);
   eff_vs_phi5->SetTotalEvents(31,487);
   eff_vs_phi5->SetPassedEvents(31,484);
   eff_vs_phi5->SetTotalEvents(32,466);
   eff_vs_phi5->SetPassedEvents(32,463);
   eff_vs_phi5->SetTotalEvents(33,444);
   eff_vs_phi5->SetPassedEvents(33,439);
   eff_vs_phi5->SetTotalEvents(34,483);
   eff_vs_phi5->SetPassedEvents(34,480);
   eff_vs_phi5->SetTotalEvents(35,447);
   eff_vs_phi5->SetPassedEvents(35,445);
   eff_vs_phi5->SetTotalEvents(36,440);
   eff_vs_phi5->SetPassedEvents(36,436);
   eff_vs_phi5->SetTotalEvents(37,455);
   eff_vs_phi5->SetPassedEvents(37,454);
   eff_vs_phi5->SetTotalEvents(38,442);
   eff_vs_phi5->SetPassedEvents(38,433);
   eff_vs_phi5->SetTotalEvents(39,472);
   eff_vs_phi5->SetPassedEvents(39,468);
   eff_vs_phi5->SetTotalEvents(40,405);
   eff_vs_phi5->SetPassedEvents(40,402);
   eff_vs_phi5->SetTotalEvents(41,499);
   eff_vs_phi5->SetPassedEvents(41,491);
   eff_vs_phi5->SetTotalEvents(42,445);
   eff_vs_phi5->SetPassedEvents(42,439);
   eff_vs_phi5->SetTotalEvents(43,486);
   eff_vs_phi5->SetPassedEvents(43,482);
   eff_vs_phi5->SetTotalEvents(44,443);
   eff_vs_phi5->SetPassedEvents(44,441);
   eff_vs_phi5->SetTotalEvents(45,470);
   eff_vs_phi5->SetPassedEvents(45,468);
   eff_vs_phi5->SetTotalEvents(46,489);
   eff_vs_phi5->SetPassedEvents(46,485);
   eff_vs_phi5->SetTotalEvents(47,448);
   eff_vs_phi5->SetPassedEvents(47,446);
   eff_vs_phi5->SetTotalEvents(48,472);
   eff_vs_phi5->SetPassedEvents(48,467);
   eff_vs_phi5->SetTotalEvents(49,443);
   eff_vs_phi5->SetPassedEvents(49,439);
   eff_vs_phi5->SetTotalEvents(50,469);
   eff_vs_phi5->SetPassedEvents(50,465);
   eff_vs_phi5->SetTotalEvents(51,478);
   eff_vs_phi5->SetPassedEvents(51,475);
   eff_vs_phi5->SetTotalEvents(52,478);
   eff_vs_phi5->SetPassedEvents(52,470);
   eff_vs_phi5->SetTotalEvents(53,476);
   eff_vs_phi5->SetPassedEvents(53,457);
   eff_vs_phi5->SetTotalEvents(54,464);
   eff_vs_phi5->SetPassedEvents(54,458);
   eff_vs_phi5->SetTotalEvents(55,499);
   eff_vs_phi5->SetPassedEvents(55,496);
   eff_vs_phi5->SetTotalEvents(56,416);
   eff_vs_phi5->SetPassedEvents(56,413);
   eff_vs_phi5->SetTotalEvents(57,440);
   eff_vs_phi5->SetPassedEvents(57,432);
   eff_vs_phi5->SetTotalEvents(58,447);
   eff_vs_phi5->SetPassedEvents(58,444);
   eff_vs_phi5->SetTotalEvents(59,468);
   eff_vs_phi5->SetPassedEvents(59,465);
   eff_vs_phi5->SetTotalEvents(60,468);
   eff_vs_phi5->SetPassedEvents(60,468);
   eff_vs_phi5->SetTotalEvents(61,426);
   eff_vs_phi5->SetPassedEvents(61,420);
   eff_vs_phi5->SetTotalEvents(62,453);
   eff_vs_phi5->SetPassedEvents(62,453);
   eff_vs_phi5->SetTotalEvents(63,451);
   eff_vs_phi5->SetPassedEvents(63,449);
   eff_vs_phi5->SetTotalEvents(64,469);
   eff_vs_phi5->SetPassedEvents(64,460);
   eff_vs_phi5->SetTotalEvents(65,502);
   eff_vs_phi5->SetPassedEvents(65,499);
   eff_vs_phi5->SetTotalEvents(66,455);
   eff_vs_phi5->SetPassedEvents(66,453);
   eff_vs_phi5->SetTotalEvents(67,449);
   eff_vs_phi5->SetPassedEvents(67,445);
   eff_vs_phi5->SetTotalEvents(68,494);
   eff_vs_phi5->SetPassedEvents(68,487);
   eff_vs_phi5->SetTotalEvents(69,456);
   eff_vs_phi5->SetPassedEvents(69,448);
   eff_vs_phi5->SetTotalEvents(70,476);
   eff_vs_phi5->SetPassedEvents(70,475);
   eff_vs_phi5->SetTotalEvents(71,465);
   eff_vs_phi5->SetPassedEvents(71,463);
   eff_vs_phi5->SetTotalEvents(72,479);
   eff_vs_phi5->SetPassedEvents(72,474);
   eff_vs_phi5->SetTotalEvents(73,457);
   eff_vs_phi5->SetPassedEvents(73,453);
   eff_vs_phi5->SetTotalEvents(74,473);
   eff_vs_phi5->SetPassedEvents(74,468);
   eff_vs_phi5->SetTotalEvents(75,468);
   eff_vs_phi5->SetPassedEvents(75,459);
   eff_vs_phi5->SetTotalEvents(76,483);
   eff_vs_phi5->SetPassedEvents(76,478);
   eff_vs_phi5->SetTotalEvents(77,489);
   eff_vs_phi5->SetPassedEvents(77,485);
   eff_vs_phi5->SetTotalEvents(78,480);
   eff_vs_phi5->SetPassedEvents(78,476);
   eff_vs_phi5->SetTotalEvents(79,482);
   eff_vs_phi5->SetPassedEvents(79,479);
   eff_vs_phi5->SetTotalEvents(80,485);
   eff_vs_phi5->SetPassedEvents(80,480);
   eff_vs_phi5->SetTotalEvents(81,474);
   eff_vs_phi5->SetPassedEvents(81,470);
   eff_vs_phi5->SetTotalEvents(82,466);
   eff_vs_phi5->SetPassedEvents(82,460);
   eff_vs_phi5->SetTotalEvents(83,438);
   eff_vs_phi5->SetPassedEvents(83,435);
   eff_vs_phi5->SetTotalEvents(84,441);
   eff_vs_phi5->SetPassedEvents(84,437);
   eff_vs_phi5->SetTotalEvents(85,419);
   eff_vs_phi5->SetPassedEvents(85,416);
   eff_vs_phi5->SetTotalEvents(86,452);
   eff_vs_phi5->SetPassedEvents(86,446);
   eff_vs_phi5->SetTotalEvents(87,512);
   eff_vs_phi5->SetPassedEvents(87,501);
   eff_vs_phi5->SetTotalEvents(88,466);
   eff_vs_phi5->SetPassedEvents(88,463);
   eff_vs_phi5->SetTotalEvents(89,449);
   eff_vs_phi5->SetPassedEvents(89,446);
   eff_vs_phi5->SetTotalEvents(90,543);
   eff_vs_phi5->SetPassedEvents(90,538);
   eff_vs_phi5->SetTotalEvents(91,455);
   eff_vs_phi5->SetPassedEvents(91,454);
   eff_vs_phi5->SetTotalEvents(92,448);
   eff_vs_phi5->SetPassedEvents(92,445);
   eff_vs_phi5->SetTotalEvents(93,499);
   eff_vs_phi5->SetPassedEvents(93,494);
   eff_vs_phi5->SetTotalEvents(94,459);
   eff_vs_phi5->SetPassedEvents(94,457);
   eff_vs_phi5->SetTotalEvents(95,504);
   eff_vs_phi5->SetPassedEvents(95,499);
   eff_vs_phi5->SetTotalEvents(96,479);
   eff_vs_phi5->SetPassedEvents(96,477);
   eff_vs_phi5->SetTotalEvents(97,451);
   eff_vs_phi5->SetPassedEvents(97,449);
   eff_vs_phi5->SetTotalEvents(98,471);
   eff_vs_phi5->SetPassedEvents(98,460);
   eff_vs_phi5->SetTotalEvents(99,448);
   eff_vs_phi5->SetPassedEvents(99,444);
   eff_vs_phi5->SetTotalEvents(100,463);
   eff_vs_phi5->SetPassedEvents(100,457);
   eff_vs_phi5->SetTotalEvents(101,446);
   eff_vs_phi5->SetPassedEvents(101,439);
   eff_vs_phi5->SetTotalEvents(102,463);
   eff_vs_phi5->SetPassedEvents(102,458);
   eff_vs_phi5->SetTotalEvents(103,491);
   eff_vs_phi5->SetPassedEvents(103,486);
   eff_vs_phi5->SetTotalEvents(104,492);
   eff_vs_phi5->SetPassedEvents(104,489);
   eff_vs_phi5->SetTotalEvents(105,480);
   eff_vs_phi5->SetPassedEvents(105,474);
   eff_vs_phi5->SetTotalEvents(106,490);
   eff_vs_phi5->SetPassedEvents(106,485);
   eff_vs_phi5->SetTotalEvents(107,468);
   eff_vs_phi5->SetPassedEvents(107,463);
   eff_vs_phi5->SetTotalEvents(108,499);
   eff_vs_phi5->SetPassedEvents(108,496);
   eff_vs_phi5->SetTotalEvents(109,452);
   eff_vs_phi5->SetPassedEvents(109,444);
   eff_vs_phi5->SetTotalEvents(110,486);
   eff_vs_phi5->SetPassedEvents(110,482);
   eff_vs_phi5->SetTotalEvents(111,460);
   eff_vs_phi5->SetPassedEvents(111,453);
   eff_vs_phi5->SetTotalEvents(112,479);
   eff_vs_phi5->SetPassedEvents(112,477);
   eff_vs_phi5->SetTotalEvents(113,477);
   eff_vs_phi5->SetPassedEvents(113,469);
   eff_vs_phi5->SetTotalEvents(114,473);
   eff_vs_phi5->SetPassedEvents(114,464);
   eff_vs_phi5->SetTotalEvents(115,493);
   eff_vs_phi5->SetPassedEvents(115,489);
   eff_vs_phi5->SetTotalEvents(116,497);
   eff_vs_phi5->SetPassedEvents(116,490);
   eff_vs_phi5->SetTotalEvents(117,453);
   eff_vs_phi5->SetPassedEvents(117,449);
   eff_vs_phi5->SetTotalEvents(118,499);
   eff_vs_phi5->SetPassedEvents(118,498);
   eff_vs_phi5->SetTotalEvents(119,469);
   eff_vs_phi5->SetPassedEvents(119,463);
   eff_vs_phi5->SetTotalEvents(120,467);
   eff_vs_phi5->SetPassedEvents(120,459);
   eff_vs_phi5->SetTotalEvents(121,485);
   eff_vs_phi5->SetPassedEvents(121,484);
   eff_vs_phi5->SetTotalEvents(122,439);
   eff_vs_phi5->SetPassedEvents(122,432);
   eff_vs_phi5->SetTotalEvents(123,507);
   eff_vs_phi5->SetPassedEvents(123,505);
   eff_vs_phi5->SetTotalEvents(124,457);
   eff_vs_phi5->SetPassedEvents(124,453);
   eff_vs_phi5->SetTotalEvents(125,430);
   eff_vs_phi5->SetPassedEvents(125,425);
   eff_vs_phi5->SetTotalEvents(126,476);
   eff_vs_phi5->SetPassedEvents(126,469);
   eff_vs_phi5->SetTotalEvents(127,481);
   eff_vs_phi5->SetPassedEvents(127,478);
   eff_vs_phi5->SetTotalEvents(128,478);
   eff_vs_phi5->SetPassedEvents(128,473);
   eff_vs_phi5->SetTotalEvents(129,406);
   eff_vs_phi5->SetPassedEvents(129,401);
   eff_vs_phi5->SetTotalEvents(130,494);
   eff_vs_phi5->SetPassedEvents(130,493);
   eff_vs_phi5->SetTotalEvents(131,461);
   eff_vs_phi5->SetPassedEvents(131,458);
   eff_vs_phi5->SetTotalEvents(132,493);
   eff_vs_phi5->SetPassedEvents(132,483);
   eff_vs_phi5->SetTotalEvents(133,471);
   eff_vs_phi5->SetPassedEvents(133,469);
   eff_vs_phi5->SetTotalEvents(134,446);
   eff_vs_phi5->SetPassedEvents(134,444);
   eff_vs_phi5->SetTotalEvents(135,482);
   eff_vs_phi5->SetPassedEvents(135,478);
   eff_vs_phi5->SetTotalEvents(136,463);
   eff_vs_phi5->SetPassedEvents(136,460);
   eff_vs_phi5->SetTotalEvents(137,443);
   eff_vs_phi5->SetPassedEvents(137,438);
   eff_vs_phi5->SetTotalEvents(138,445);
   eff_vs_phi5->SetPassedEvents(138,443);
   eff_vs_phi5->SetTotalEvents(139,454);
   eff_vs_phi5->SetPassedEvents(139,450);
   eff_vs_phi5->SetTotalEvents(140,478);
   eff_vs_phi5->SetPassedEvents(140,474);
   eff_vs_phi5->SetTotalEvents(141,470);
   eff_vs_phi5->SetPassedEvents(141,466);
   eff_vs_phi5->SetTotalEvents(142,473);
   eff_vs_phi5->SetPassedEvents(142,469);
   eff_vs_phi5->SetTotalEvents(143,471);
   eff_vs_phi5->SetPassedEvents(143,461);
   eff_vs_phi5->SetTotalEvents(144,465);
   eff_vs_phi5->SetPassedEvents(144,461);
   eff_vs_phi5->SetTotalEvents(145,415);
   eff_vs_phi5->SetPassedEvents(145,414);
   eff_vs_phi5->SetTotalEvents(146,425);
   eff_vs_phi5->SetPassedEvents(146,424);
   eff_vs_phi5->SetTotalEvents(147,455);
   eff_vs_phi5->SetPassedEvents(147,453);
   eff_vs_phi5->SetTotalEvents(148,455);
   eff_vs_phi5->SetPassedEvents(148,452);
   eff_vs_phi5->SetTotalEvents(149,453);
   eff_vs_phi5->SetPassedEvents(149,450);
   eff_vs_phi5->SetTotalEvents(150,426);
   eff_vs_phi5->SetPassedEvents(150,425);
   eff_vs_phi5->SetTotalEvents(151,434);
   eff_vs_phi5->SetPassedEvents(151,429);
   eff_vs_phi5->SetTotalEvents(152,436);
   eff_vs_phi5->SetPassedEvents(152,434);
   eff_vs_phi5->SetTotalEvents(153,473);
   eff_vs_phi5->SetPassedEvents(153,468);
   eff_vs_phi5->SetTotalEvents(154,459);
   eff_vs_phi5->SetPassedEvents(154,447);
   eff_vs_phi5->SetTotalEvents(155,458);
   eff_vs_phi5->SetPassedEvents(155,455);
   eff_vs_phi5->SetTotalEvents(156,447);
   eff_vs_phi5->SetPassedEvents(156,442);
   eff_vs_phi5->SetTotalEvents(157,474);
   eff_vs_phi5->SetPassedEvents(157,471);
   eff_vs_phi5->SetTotalEvents(158,437);
   eff_vs_phi5->SetPassedEvents(158,431);
   eff_vs_phi5->SetTotalEvents(159,460);
   eff_vs_phi5->SetPassedEvents(159,456);
   eff_vs_phi5->SetTotalEvents(160,440);
   eff_vs_phi5->SetPassedEvents(160,437);
   eff_vs_phi5->SetTotalEvents(161,461);
   eff_vs_phi5->SetPassedEvents(161,457);
   eff_vs_phi5->SetTotalEvents(162,443);
   eff_vs_phi5->SetPassedEvents(162,436);
   eff_vs_phi5->SetTotalEvents(163,449);
   eff_vs_phi5->SetPassedEvents(163,447);
   eff_vs_phi5->SetTotalEvents(164,432);
   eff_vs_phi5->SetPassedEvents(164,430);
   eff_vs_phi5->SetTotalEvents(165,499);
   eff_vs_phi5->SetPassedEvents(165,483);
   eff_vs_phi5->SetTotalEvents(166,456);
   eff_vs_phi5->SetPassedEvents(166,451);
   eff_vs_phi5->SetTotalEvents(167,439);
   eff_vs_phi5->SetPassedEvents(167,437);
   eff_vs_phi5->SetTotalEvents(168,468);
   eff_vs_phi5->SetPassedEvents(168,461);
   eff_vs_phi5->SetTotalEvents(169,461);
   eff_vs_phi5->SetPassedEvents(169,457);
   eff_vs_phi5->SetTotalEvents(170,452);
   eff_vs_phi5->SetPassedEvents(170,450);
   eff_vs_phi5->SetTotalEvents(171,423);
   eff_vs_phi5->SetPassedEvents(171,422);
   eff_vs_phi5->SetTotalEvents(172,458);
   eff_vs_phi5->SetPassedEvents(172,452);
   eff_vs_phi5->SetTotalEvents(173,397);
   eff_vs_phi5->SetPassedEvents(173,394);
   eff_vs_phi5->SetTotalEvents(174,432);
   eff_vs_phi5->SetPassedEvents(174,429);
   eff_vs_phi5->SetTotalEvents(175,413);
   eff_vs_phi5->SetPassedEvents(175,405);
   eff_vs_phi5->SetTotalEvents(176,422);
   eff_vs_phi5->SetPassedEvents(176,419);
   eff_vs_phi5->SetTotalEvents(177,462);
   eff_vs_phi5->SetPassedEvents(177,455);
   eff_vs_phi5->SetTotalEvents(178,453);
   eff_vs_phi5->SetPassedEvents(178,449);
   eff_vs_phi5->SetTotalEvents(179,424);
   eff_vs_phi5->SetPassedEvents(179,419);
   eff_vs_phi5->SetTotalEvents(180,502);
   eff_vs_phi5->SetPassedEvents(180,499);
   eff_vs_phi5->SetTotalEvents(181,0);
   eff_vs_phi5->SetPassedEvents(181,0);
   eff_vs_phi5->SetFillColor(19);
   eff_vs_phi5->SetLineColor(2);
   eff_vs_phi5->SetMarkerColor(2);
   eff_vs_phi5->SetMarkerStyle(20);
   eff_vs_phi5->SetMarkerSize(1.3);
   eff_vs_phi5->Draw("ap");
   
   TLegend *leg = new TLegend(0.53,0.32,0.85,0.47,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.03);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(2);
   leg->SetFillColor(0);
   leg->SetFillStyle(1001);
   TLegendEntry *entry=leg->AddEntry("NULL","#chi^{#pm}, c#tau_{#chi^{#pm}} = 600mm","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("eff_vs_phi","Signal","ep");
   entry->SetLineColor(2);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1.3);
   entry->SetTextFont(62);
   entry=leg->AddEntry("eff_vs_phi","Signal + 3 TeV #gamma#gamma#rightarrow had","ep");
   entry->SetLineColor(13);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(13);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1.3);
   entry->SetTextFont(62);
   leg->Draw();
   
   TEfficiency * eff_vs_phi6 = new TEfficiency("eff_vs_phi","",180,-180,180);
   
   eff_vs_phi6->SetConfidenceLevel(0.6826895);
   eff_vs_phi6->SetBetaAlpha(1);
   eff_vs_phi6->SetBetaBeta(1);
   eff_vs_phi6->SetWeight(1);
   eff_vs_phi6->SetStatisticOption(0);
   eff_vs_phi6->SetPosteriorMode(0);
   eff_vs_phi6->SetShortestInterval(0);
   eff_vs_phi6->SetTotalEvents(0,0);
   eff_vs_phi6->SetPassedEvents(0,0);
   eff_vs_phi6->SetTotalEvents(1,444);
   eff_vs_phi6->SetPassedEvents(1,435);
   eff_vs_phi6->SetTotalEvents(2,429);
   eff_vs_phi6->SetPassedEvents(2,426);
   eff_vs_phi6->SetTotalEvents(3,457);
   eff_vs_phi6->SetPassedEvents(3,448);
   eff_vs_phi6->SetTotalEvents(4,438);
   eff_vs_phi6->SetPassedEvents(4,436);
   eff_vs_phi6->SetTotalEvents(5,480);
   eff_vs_phi6->SetPassedEvents(5,472);
   eff_vs_phi6->SetTotalEvents(6,442);
   eff_vs_phi6->SetPassedEvents(6,436);
   eff_vs_phi6->SetTotalEvents(7,449);
   eff_vs_phi6->SetPassedEvents(7,446);
   eff_vs_phi6->SetTotalEvents(8,449);
   eff_vs_phi6->SetPassedEvents(8,437);
   eff_vs_phi6->SetTotalEvents(9,444);
   eff_vs_phi6->SetPassedEvents(9,434);
   eff_vs_phi6->SetTotalEvents(10,425);
   eff_vs_phi6->SetPassedEvents(10,421);
   eff_vs_phi6->SetTotalEvents(11,433);
   eff_vs_phi6->SetPassedEvents(11,425);
   eff_vs_phi6->SetTotalEvents(12,424);
   eff_vs_phi6->SetPassedEvents(12,423);
   eff_vs_phi6->SetTotalEvents(13,482);
   eff_vs_phi6->SetPassedEvents(13,478);
   eff_vs_phi6->SetTotalEvents(14,481);
   eff_vs_phi6->SetPassedEvents(14,481);
   eff_vs_phi6->SetTotalEvents(15,438);
   eff_vs_phi6->SetPassedEvents(15,433);
   eff_vs_phi6->SetTotalEvents(16,471);
   eff_vs_phi6->SetPassedEvents(16,467);
   eff_vs_phi6->SetTotalEvents(17,476);
   eff_vs_phi6->SetPassedEvents(17,470);
   eff_vs_phi6->SetTotalEvents(18,416);
   eff_vs_phi6->SetPassedEvents(18,414);
   eff_vs_phi6->SetTotalEvents(19,475);
   eff_vs_phi6->SetPassedEvents(19,461);
   eff_vs_phi6->SetTotalEvents(20,433);
   eff_vs_phi6->SetPassedEvents(20,424);
   eff_vs_phi6->SetTotalEvents(21,445);
   eff_vs_phi6->SetPassedEvents(21,442);
   eff_vs_phi6->SetTotalEvents(22,443);
   eff_vs_phi6->SetPassedEvents(22,435);
   eff_vs_phi6->SetTotalEvents(23,452);
   eff_vs_phi6->SetPassedEvents(23,446);
   eff_vs_phi6->SetTotalEvents(24,465);
   eff_vs_phi6->SetPassedEvents(24,452);
   eff_vs_phi6->SetTotalEvents(25,459);
   eff_vs_phi6->SetPassedEvents(25,456);
   eff_vs_phi6->SetTotalEvents(26,484);
   eff_vs_phi6->SetPassedEvents(26,480);
   eff_vs_phi6->SetTotalEvents(27,461);
   eff_vs_phi6->SetPassedEvents(27,459);
   eff_vs_phi6->SetTotalEvents(28,438);
   eff_vs_phi6->SetPassedEvents(28,434);
   eff_vs_phi6->SetTotalEvents(29,490);
   eff_vs_phi6->SetPassedEvents(29,485);
   eff_vs_phi6->SetTotalEvents(30,426);
   eff_vs_phi6->SetPassedEvents(30,419);
   eff_vs_phi6->SetTotalEvents(31,487);
   eff_vs_phi6->SetPassedEvents(31,483);
   eff_vs_phi6->SetTotalEvents(32,466);
   eff_vs_phi6->SetPassedEvents(32,461);
   eff_vs_phi6->SetTotalEvents(33,444);
   eff_vs_phi6->SetPassedEvents(33,439);
   eff_vs_phi6->SetTotalEvents(34,483);
   eff_vs_phi6->SetPassedEvents(34,479);
   eff_vs_phi6->SetTotalEvents(35,447);
   eff_vs_phi6->SetPassedEvents(35,445);
   eff_vs_phi6->SetTotalEvents(36,440);
   eff_vs_phi6->SetPassedEvents(36,434);
   eff_vs_phi6->SetTotalEvents(37,455);
   eff_vs_phi6->SetPassedEvents(37,452);
   eff_vs_phi6->SetTotalEvents(38,442);
   eff_vs_phi6->SetPassedEvents(38,433);
   eff_vs_phi6->SetTotalEvents(39,473);
   eff_vs_phi6->SetPassedEvents(39,469);
   eff_vs_phi6->SetTotalEvents(40,405);
   eff_vs_phi6->SetPassedEvents(40,403);
   eff_vs_phi6->SetTotalEvents(41,499);
   eff_vs_phi6->SetPassedEvents(41,486);
   eff_vs_phi6->SetTotalEvents(42,446);
   eff_vs_phi6->SetPassedEvents(42,435);
   eff_vs_phi6->SetTotalEvents(43,486);
   eff_vs_phi6->SetPassedEvents(43,480);
   eff_vs_phi6->SetTotalEvents(44,444);
   eff_vs_phi6->SetPassedEvents(44,438);
   eff_vs_phi6->SetTotalEvents(45,470);
   eff_vs_phi6->SetPassedEvents(45,467);
   eff_vs_phi6->SetTotalEvents(46,489);
   eff_vs_phi6->SetPassedEvents(46,483);
   eff_vs_phi6->SetTotalEvents(47,448);
   eff_vs_phi6->SetPassedEvents(47,444);
   eff_vs_phi6->SetTotalEvents(48,472);
   eff_vs_phi6->SetPassedEvents(48,468);
   eff_vs_phi6->SetTotalEvents(49,443);
   eff_vs_phi6->SetPassedEvents(49,437);
   eff_vs_phi6->SetTotalEvents(50,469);
   eff_vs_phi6->SetPassedEvents(50,464);
   eff_vs_phi6->SetTotalEvents(51,479);
   eff_vs_phi6->SetPassedEvents(51,475);
   eff_vs_phi6->SetTotalEvents(52,478);
   eff_vs_phi6->SetPassedEvents(52,472);
   eff_vs_phi6->SetTotalEvents(53,476);
   eff_vs_phi6->SetPassedEvents(53,458);
   eff_vs_phi6->SetTotalEvents(54,465);
   eff_vs_phi6->SetPassedEvents(54,457);
   eff_vs_phi6->SetTotalEvents(55,499);
   eff_vs_phi6->SetPassedEvents(55,493);
   eff_vs_phi6->SetTotalEvents(56,416);
   eff_vs_phi6->SetPassedEvents(56,413);
   eff_vs_phi6->SetTotalEvents(57,440);
   eff_vs_phi6->SetPassedEvents(57,429);
   eff_vs_phi6->SetTotalEvents(58,447);
   eff_vs_phi6->SetPassedEvents(58,444);
   eff_vs_phi6->SetTotalEvents(59,468);
   eff_vs_phi6->SetPassedEvents(59,465);
   eff_vs_phi6->SetTotalEvents(60,468);
   eff_vs_phi6->SetPassedEvents(60,465);
   eff_vs_phi6->SetTotalEvents(61,426);
   eff_vs_phi6->SetPassedEvents(61,420);
   eff_vs_phi6->SetTotalEvents(62,453);
   eff_vs_phi6->SetPassedEvents(62,452);
   eff_vs_phi6->SetTotalEvents(63,451);
   eff_vs_phi6->SetPassedEvents(63,447);
   eff_vs_phi6->SetTotalEvents(64,470);
   eff_vs_phi6->SetPassedEvents(64,456);
   eff_vs_phi6->SetTotalEvents(65,502);
   eff_vs_phi6->SetPassedEvents(65,496);
   eff_vs_phi6->SetTotalEvents(66,457);
   eff_vs_phi6->SetPassedEvents(66,452);
   eff_vs_phi6->SetTotalEvents(67,449);
   eff_vs_phi6->SetPassedEvents(67,443);
   eff_vs_phi6->SetTotalEvents(68,494);
   eff_vs_phi6->SetPassedEvents(68,486);
   eff_vs_phi6->SetTotalEvents(69,456);
   eff_vs_phi6->SetPassedEvents(69,446);
   eff_vs_phi6->SetTotalEvents(70,476);
   eff_vs_phi6->SetPassedEvents(70,472);
   eff_vs_phi6->SetTotalEvents(71,466);
   eff_vs_phi6->SetPassedEvents(71,464);
   eff_vs_phi6->SetTotalEvents(72,479);
   eff_vs_phi6->SetPassedEvents(72,473);
   eff_vs_phi6->SetTotalEvents(73,457);
   eff_vs_phi6->SetPassedEvents(73,451);
   eff_vs_phi6->SetTotalEvents(74,473);
   eff_vs_phi6->SetPassedEvents(74,468);
   eff_vs_phi6->SetTotalEvents(75,468);
   eff_vs_phi6->SetPassedEvents(75,457);
   eff_vs_phi6->SetTotalEvents(76,483);
   eff_vs_phi6->SetPassedEvents(76,477);
   eff_vs_phi6->SetTotalEvents(77,490);
   eff_vs_phi6->SetPassedEvents(77,484);
   eff_vs_phi6->SetTotalEvents(78,480);
   eff_vs_phi6->SetPassedEvents(78,476);
   eff_vs_phi6->SetTotalEvents(79,483);
   eff_vs_phi6->SetPassedEvents(79,478);
   eff_vs_phi6->SetTotalEvents(80,485);
   eff_vs_phi6->SetPassedEvents(80,480);
   eff_vs_phi6->SetTotalEvents(81,474);
   eff_vs_phi6->SetPassedEvents(81,469);
   eff_vs_phi6->SetTotalEvents(82,466);
   eff_vs_phi6->SetPassedEvents(82,460);
   eff_vs_phi6->SetTotalEvents(83,438);
   eff_vs_phi6->SetPassedEvents(83,432);
   eff_vs_phi6->SetTotalEvents(84,441);
   eff_vs_phi6->SetPassedEvents(84,437);
   eff_vs_phi6->SetTotalEvents(85,419);
   eff_vs_phi6->SetPassedEvents(85,414);
   eff_vs_phi6->SetTotalEvents(86,452);
   eff_vs_phi6->SetPassedEvents(86,445);
   eff_vs_phi6->SetTotalEvents(87,512);
   eff_vs_phi6->SetPassedEvents(87,497);
   eff_vs_phi6->SetTotalEvents(88,467);
   eff_vs_phi6->SetPassedEvents(88,461);
   eff_vs_phi6->SetTotalEvents(89,450);
   eff_vs_phi6->SetPassedEvents(89,447);
   eff_vs_phi6->SetTotalEvents(90,544);
   eff_vs_phi6->SetPassedEvents(90,536);
   eff_vs_phi6->SetTotalEvents(91,455);
   eff_vs_phi6->SetPassedEvents(91,452);
   eff_vs_phi6->SetTotalEvents(92,449);
   eff_vs_phi6->SetPassedEvents(92,446);
   eff_vs_phi6->SetTotalEvents(93,500);
   eff_vs_phi6->SetPassedEvents(93,493);
   eff_vs_phi6->SetTotalEvents(94,460);
   eff_vs_phi6->SetPassedEvents(94,456);
   eff_vs_phi6->SetTotalEvents(95,504);
   eff_vs_phi6->SetPassedEvents(95,498);
   eff_vs_phi6->SetTotalEvents(96,479);
   eff_vs_phi6->SetPassedEvents(96,474);
   eff_vs_phi6->SetTotalEvents(97,451);
   eff_vs_phi6->SetPassedEvents(97,450);
   eff_vs_phi6->SetTotalEvents(98,471);
   eff_vs_phi6->SetPassedEvents(98,459);
   eff_vs_phi6->SetTotalEvents(99,451);
   eff_vs_phi6->SetPassedEvents(99,447);
   eff_vs_phi6->SetTotalEvents(100,463);
   eff_vs_phi6->SetPassedEvents(100,455);
   eff_vs_phi6->SetTotalEvents(101,446);
   eff_vs_phi6->SetPassedEvents(101,437);
   eff_vs_phi6->SetTotalEvents(102,464);
   eff_vs_phi6->SetPassedEvents(102,456);
   eff_vs_phi6->SetTotalEvents(103,492);
   eff_vs_phi6->SetPassedEvents(103,485);
   eff_vs_phi6->SetTotalEvents(104,492);
   eff_vs_phi6->SetPassedEvents(104,488);
   eff_vs_phi6->SetTotalEvents(105,480);
   eff_vs_phi6->SetPassedEvents(105,473);
   eff_vs_phi6->SetTotalEvents(106,490);
   eff_vs_phi6->SetPassedEvents(106,483);
   eff_vs_phi6->SetTotalEvents(107,468);
   eff_vs_phi6->SetPassedEvents(107,463);
   eff_vs_phi6->SetTotalEvents(108,499);
   eff_vs_phi6->SetPassedEvents(108,492);
   eff_vs_phi6->SetTotalEvents(109,452);
   eff_vs_phi6->SetPassedEvents(109,438);
   eff_vs_phi6->SetTotalEvents(110,487);
   eff_vs_phi6->SetPassedEvents(110,483);
   eff_vs_phi6->SetTotalEvents(111,460);
   eff_vs_phi6->SetPassedEvents(111,451);
   eff_vs_phi6->SetTotalEvents(112,479);
   eff_vs_phi6->SetPassedEvents(112,475);
   eff_vs_phi6->SetTotalEvents(113,477);
   eff_vs_phi6->SetPassedEvents(113,468);
   eff_vs_phi6->SetTotalEvents(114,473);
   eff_vs_phi6->SetPassedEvents(114,461);
   eff_vs_phi6->SetTotalEvents(115,493);
   eff_vs_phi6->SetPassedEvents(115,486);
   eff_vs_phi6->SetTotalEvents(116,497);
   eff_vs_phi6->SetPassedEvents(116,490);
   eff_vs_phi6->SetTotalEvents(117,453);
   eff_vs_phi6->SetPassedEvents(117,448);
   eff_vs_phi6->SetTotalEvents(118,499);
   eff_vs_phi6->SetPassedEvents(118,497);
   eff_vs_phi6->SetTotalEvents(119,469);
   eff_vs_phi6->SetPassedEvents(119,457);
   eff_vs_phi6->SetTotalEvents(120,467);
   eff_vs_phi6->SetPassedEvents(120,456);
   eff_vs_phi6->SetTotalEvents(121,485);
   eff_vs_phi6->SetPassedEvents(121,483);
   eff_vs_phi6->SetTotalEvents(122,439);
   eff_vs_phi6->SetPassedEvents(122,431);
   eff_vs_phi6->SetTotalEvents(123,507);
   eff_vs_phi6->SetPassedEvents(123,503);
   eff_vs_phi6->SetTotalEvents(124,457);
   eff_vs_phi6->SetPassedEvents(124,451);
   eff_vs_phi6->SetTotalEvents(125,430);
   eff_vs_phi6->SetPassedEvents(125,422);
   eff_vs_phi6->SetTotalEvents(126,476);
   eff_vs_phi6->SetPassedEvents(126,467);
   eff_vs_phi6->SetTotalEvents(127,481);
   eff_vs_phi6->SetPassedEvents(127,477);
   eff_vs_phi6->SetTotalEvents(128,478);
   eff_vs_phi6->SetPassedEvents(128,474);
   eff_vs_phi6->SetTotalEvents(129,406);
   eff_vs_phi6->SetPassedEvents(129,401);
   eff_vs_phi6->SetTotalEvents(130,494);
   eff_vs_phi6->SetPassedEvents(130,491);
   eff_vs_phi6->SetTotalEvents(131,462);
   eff_vs_phi6->SetPassedEvents(131,456);
   eff_vs_phi6->SetTotalEvents(132,493);
   eff_vs_phi6->SetPassedEvents(132,482);
   eff_vs_phi6->SetTotalEvents(133,472);
   eff_vs_phi6->SetPassedEvents(133,468);
   eff_vs_phi6->SetTotalEvents(134,446);
   eff_vs_phi6->SetPassedEvents(134,443);
   eff_vs_phi6->SetTotalEvents(135,482);
   eff_vs_phi6->SetPassedEvents(135,478);
   eff_vs_phi6->SetTotalEvents(136,463);
   eff_vs_phi6->SetPassedEvents(136,459);
   eff_vs_phi6->SetTotalEvents(137,443);
   eff_vs_phi6->SetPassedEvents(137,436);
   eff_vs_phi6->SetTotalEvents(138,445);
   eff_vs_phi6->SetPassedEvents(138,441);
   eff_vs_phi6->SetTotalEvents(139,454);
   eff_vs_phi6->SetPassedEvents(139,446);
   eff_vs_phi6->SetTotalEvents(140,479);
   eff_vs_phi6->SetPassedEvents(140,475);
   eff_vs_phi6->SetTotalEvents(141,470);
   eff_vs_phi6->SetPassedEvents(141,462);
   eff_vs_phi6->SetTotalEvents(142,473);
   eff_vs_phi6->SetPassedEvents(142,465);
   eff_vs_phi6->SetTotalEvents(143,472);
   eff_vs_phi6->SetPassedEvents(143,459);
   eff_vs_phi6->SetTotalEvents(144,465);
   eff_vs_phi6->SetPassedEvents(144,460);
   eff_vs_phi6->SetTotalEvents(145,415);
   eff_vs_phi6->SetPassedEvents(145,412);
   eff_vs_phi6->SetTotalEvents(146,425);
   eff_vs_phi6->SetPassedEvents(146,421);
   eff_vs_phi6->SetTotalEvents(147,455);
   eff_vs_phi6->SetPassedEvents(147,451);
   eff_vs_phi6->SetTotalEvents(148,455);
   eff_vs_phi6->SetPassedEvents(148,450);
   eff_vs_phi6->SetTotalEvents(149,453);
   eff_vs_phi6->SetPassedEvents(149,447);
   eff_vs_phi6->SetTotalEvents(150,426);
   eff_vs_phi6->SetPassedEvents(150,423);
   eff_vs_phi6->SetTotalEvents(151,434);
   eff_vs_phi6->SetPassedEvents(151,429);
   eff_vs_phi6->SetTotalEvents(152,436);
   eff_vs_phi6->SetPassedEvents(152,431);
   eff_vs_phi6->SetTotalEvents(153,474);
   eff_vs_phi6->SetPassedEvents(153,467);
   eff_vs_phi6->SetTotalEvents(154,459);
   eff_vs_phi6->SetPassedEvents(154,445);
   eff_vs_phi6->SetTotalEvents(155,459);
   eff_vs_phi6->SetPassedEvents(155,455);
   eff_vs_phi6->SetTotalEvents(156,447);
   eff_vs_phi6->SetPassedEvents(156,442);
   eff_vs_phi6->SetTotalEvents(157,474);
   eff_vs_phi6->SetPassedEvents(157,469);
   eff_vs_phi6->SetTotalEvents(158,437);
   eff_vs_phi6->SetPassedEvents(158,428);
   eff_vs_phi6->SetTotalEvents(159,460);
   eff_vs_phi6->SetPassedEvents(159,457);
   eff_vs_phi6->SetTotalEvents(160,440);
   eff_vs_phi6->SetPassedEvents(160,436);
   eff_vs_phi6->SetTotalEvents(161,461);
   eff_vs_phi6->SetPassedEvents(161,455);
   eff_vs_phi6->SetTotalEvents(162,443);
   eff_vs_phi6->SetPassedEvents(162,434);
   eff_vs_phi6->SetTotalEvents(163,449);
   eff_vs_phi6->SetPassedEvents(163,445);
   eff_vs_phi6->SetTotalEvents(164,432);
   eff_vs_phi6->SetPassedEvents(164,426);
   eff_vs_phi6->SetTotalEvents(165,499);
   eff_vs_phi6->SetPassedEvents(165,484);
   eff_vs_phi6->SetTotalEvents(166,456);
   eff_vs_phi6->SetPassedEvents(166,450);
   eff_vs_phi6->SetTotalEvents(167,440);
   eff_vs_phi6->SetPassedEvents(167,435);
   eff_vs_phi6->SetTotalEvents(168,469);
   eff_vs_phi6->SetPassedEvents(168,461);
   eff_vs_phi6->SetTotalEvents(169,461);
   eff_vs_phi6->SetPassedEvents(169,454);
   eff_vs_phi6->SetTotalEvents(170,452);
   eff_vs_phi6->SetPassedEvents(170,448);
   eff_vs_phi6->SetTotalEvents(171,423);
   eff_vs_phi6->SetPassedEvents(171,421);
   eff_vs_phi6->SetTotalEvents(172,458);
   eff_vs_phi6->SetPassedEvents(172,452);
   eff_vs_phi6->SetTotalEvents(173,397);
   eff_vs_phi6->SetPassedEvents(173,392);
   eff_vs_phi6->SetTotalEvents(174,432);
   eff_vs_phi6->SetPassedEvents(174,427);
   eff_vs_phi6->SetTotalEvents(175,413);
   eff_vs_phi6->SetPassedEvents(175,405);
   eff_vs_phi6->SetTotalEvents(176,422);
   eff_vs_phi6->SetPassedEvents(176,418);
   eff_vs_phi6->SetTotalEvents(177,462);
   eff_vs_phi6->SetPassedEvents(177,455);
   eff_vs_phi6->SetTotalEvents(178,454);
   eff_vs_phi6->SetPassedEvents(178,447);
   eff_vs_phi6->SetTotalEvents(179,424);
   eff_vs_phi6->SetPassedEvents(179,418);
   eff_vs_phi6->SetTotalEvents(180,503);
   eff_vs_phi6->SetPassedEvents(180,495);
   eff_vs_phi6->SetTotalEvents(181,0);
   eff_vs_phi6->SetPassedEvents(181,0);
   eff_vs_phi6->SetFillColor(19);
   eff_vs_phi6->SetLineColor(13);
   eff_vs_phi6->SetMarkerColor(13);
   eff_vs_phi6->SetMarkerStyle(21);
   eff_vs_phi6->SetMarkerSize(1.3);
   eff_vs_phi6->Draw("samep");
   
   leg = new TLegend(0.53,0.32,0.85,0.47,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.03);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(2);
   leg->SetFillColor(0);
   leg->SetFillStyle(1001);
   entry=leg->AddEntry("NULL","#chi^{#pm}, c#tau_{#chi^{#pm}} = 600mm","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("eff_vs_phi","Signal","ep");
   entry->SetLineColor(2);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1.3);
   entry->SetTextFont(62);
   entry=leg->AddEntry("eff_vs_phi","Signal + 3 TeV #gamma#gamma#rightarrow had","ep");
   entry->SetLineColor(13);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(13);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1.3);
   entry->SetTextFont(62);
   leg->Draw();
   TText *text = new TText(0.175,0.939349,"CLICdp work in progress");
   text->SetNDC();
   text->SetTextFont(42);
   text->SetTextSize(0.035);
   text->Draw();
   c->Modified();
   c->cd();
   c->SetSelected(c);
}
