import ROOT
import glob
from math import exp

sample_dict = {
    "signal_v2_600mm" : 
        {"path" :"/eos/experiment/clicdp/grid/ilc/user/s/schnooru/LLP/LLP_2020-11-05/", 
             "prodIDs" : ["13901"], 
             "ctau": 600},
    "background" :
    {
        "path" : "/eos/experiment/clicdp/grid/ilc/user/s/schnooru/LLP/LLP_2020-11-05",
        "prodIDs" : ["13903", "13904", "13927", "13930", "13933", "13936", "13939", "13942", "13945", "13948"]
    }
}

def create_chain_from_path(input_path, target_chain,prodIDs=[]):
    for prodID in prodIDs:
        for i in glob.iglob(  input_path  +'/*'+prodID+'*.root'):
            print(i)
            target_chain.Add(i)
    return target_chain

chain_signal_v2_600mm = ROOT.TChain("Event")
path_signal_v2_600mm = sample_dict["signal_v2_600mm"]["path"]
create_chain_from_path(path_signal_v2_600mm,chain_signal_v2_600mm,prodIDs=sample_dict["signal_v2_600mm"]["prodIDs"])

chain_bg = ROOT.TChain("Event")
path_bg = sample_dict["background"]["path"]
create_chain_from_path(path_bg, chain_bg, prodIDs=sample_dict["background"]["prodIDs"])

signal = chain_signal_v2_600mm
background = chain_bg
print(chain_signal_v2_600mm.GetListOfBranches())

ROOT.TMVA.Tools.Instance()
fout = ROOT.TFile("test.root","RECREATE")
 
factory = ROOT.TMVA.Factory("TMVAClassification", fout,
                            ":".join([
                                "!V",
                                "!Silent",
                                "Color",
                                "DrawProgressBar",
                                "Transformations=I;D;P;G,D",
                                "AnalysisType=Classification"]
                                     ))

dataloader = ROOT.TMVA.DataLoader('dataset_pymva')

train_vars = ["Ch1_PT","Ch2_PT","Ch1_Theta","Ch1_PTFraction_cone01","Ch1_LastHitR","Ch1_dEdx_Avrg","Ch2_Theta","Ch2_PTFraction_cone01","Ch2_LastHitR","Ch2_dEdx_Avrg"]
for train_var in train_vars:
    #dataloader.AddVariable(signal.GetBranch(train_var))
    dataloader.AddVariable(train_var)    

truth_vars = ["TrueChm_PT", "TrueChm_P", "TrueChm_Theta", "TrueChm_Phi", "TrueChm_TrajLen", "TrueChm_PDGID", "TrueChm_VertexR","TrueChp_PT", "TrueChp_P", "TrueChp_Theta", "TrueChp_Phi", "TrueChp_TrajLen", "TrueChp_PDGID", "TrueChp_VertexR" ]
for truth_var in truth_vars:
    dataloader.AddSpectator(truth_var)

spectators = ["Ch1_truePDGID", "Ch1_Phi", "Ch1_N_AssocPFO", "Ch1_N_ConePions", "Ch1_D0", "Ch1_Z0", "Ch1_FirstHitR", "Ch1_VertexR", "Ch2_truePDGID", "Ch2_Phi", "Ch2_N_AssocPFO", "Ch2_N_ConePions", "Ch2_D0", "Ch2_Z0", "Ch2_FirstHitR", "Ch2_VertexR" ]
for spec in spectators:
    dataloader.AddSpectator(spec)


ctau_orig = 600
ctau_rw = 6.9
mass = 1050

dataloader.SetSignalWeightExpression( "600 / 6.9 * exp(-1050 / TrueChm_P * TrueChm_TrajLen / 6.9 + 1050 /  TrueChm_P * TrueChm_TrajLen / 600) * 600 / 6.9 * exp(-1050 / TrueChp_P * TrueChp_TrajLen / 6.9 + 1050 /  TrueChp_P * TrueChp_TrajLen / 600)   " )
# using w_chm*w_chp with w_chm =  "ctau_orig / ctau_rw * exp(-mass / TrueChm_P * TrueChm_TrajLen / ctau_rw + mass /  TrueChm_P * TrueChm_TrajLen / ctau_orig)"


dataloader.AddSignalTree(signal)
dataloader.AddBackgroundTree(background)
trainTestSplit = 0.8
dataloader.PrepareTrainingAndTestTree(ROOT.TCut(''),
        'TrainTestSplit_Signal={}:'.format(trainTestSplit)+\
        'TrainTestSplit_Background={}:'.format(trainTestSplit)+\
        'SplitMode=Random')
factory.BookMethod(dataloader, ROOT.TMVA.Types.kBDT, "BDT",
                   ":".join([
                       "!H",
                       "!V",
                       "NTrees=850",
                       "MinNodeSize=5%",
                       "MaxDepth=3",
                       "BoostType=AdaBoost",
                       "AdaBoostBeta=0.5",
                       "SeparationType=GiniIndex",
                       "nCuts=20",
                       "PruneMethod=NoPruning",
                       ]))


factory.TrainAllMethods()
factory.TestAllMethods()

factory.EvaluateAllMethods()
# Print ROC
canvas = factory.GetROCCurve(dataloader)
canvas.Draw()
raw_input()
canvas = factory.DrawOutputDistribution(dataloader, "BDT")

canvas.Draw()
raw_input()

